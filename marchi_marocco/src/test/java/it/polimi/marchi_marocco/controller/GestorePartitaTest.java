package it.polimi.marchi_marocco.controller;

import static org.junit.Assert.*;

import java.util.Random;

import it.polimi.marchi_marocco.model.Giocatore;
import it.polimi.marchi_marocco.model.StatoPartita;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class GestorePartitaTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	/**
	 * test del metodo privato testInizioPartita: controlla che il metodo utilizzi correttamente gli input
	 * ricevuti dai client nella fase di posizionamento dei pastori iniziali,
	 * in caso di 2 o più giocatori.
	 */
	@Test
	public void testInizioPartita(){
		
		StatoPartita stato = new StatoPartita(2);
		GestoreComunicazioneStubPosizionamentoPastoriInizialiTest comunicazione = new GestoreComunicazioneStubPosizionamentoPastoriInizialiTest();
		GestorePartita gestore = new GestorePartita(stato, comunicazione, 2);
		
		gestore.testGetInizioPartita();
		for(int i= 0; i<2; i++){
			assertTrue(stato.getGiocatore(i).getPosizioni() != null);
			assertTrue(stato.getGiocatore(i).getPosizioni().get(0).getFlagGiocatore());
			assertTrue(stato.getGiocatore(i).getPosizioni().get(1).getFlagGiocatore());
			
		}
	
		
		stato = new StatoPartita(3);
		comunicazione = new GestoreComunicazioneStubPosizionamentoPastoriInizialiTest();
		gestore = new GestorePartita(stato, comunicazione, 3);
		
		gestore.testGetInizioPartita();
		
		for(int i= 0; i<3; i++){
			assertTrue(stato.getGiocatore(i).getPosizioni() != null);
			assertTrue(stato.getGiocatore(i).getPosizioni().get(0).getFlagGiocatore());
			
		}
		
		
	}
	
	/**
	 * test del metodo runPartita: controlla che il metodo  si esegua completamente
	 * fino alla chiamata di inizioPartita()
	 */
	@Test
	public void testRunPartita(){
		

		StatoPartita stato = new StatoPartita(4);
		GestoreComunicazioneStubPosizionamentoPastoriInizialiTest comunicazione = new GestoreComunicazioneStubPosizionamentoPastoriInizialiTest();
		GestorePartita gestore = new GestorePartita(stato, comunicazione, 2);
		
		gestore.runPartita();
		
	}
	
	/**
	 * test del metodo runMossa: controlla che, nel caso in cui i metodi di controllo ed esecuzione effettiva della mossa
	 * all'interno del model procedano senza lanciare eccezioni (si presuppone che i parametri passati siano corretti),
	 * il codice di runMossa si esegua completamente senza generare errori e passando i parametri corretti alle funzioni chiamate.
	 * @throws MossaNonValidaException 
	 */
	@Test
	public void testRunMossa() throws MossaNonValidaException{
		StatoPartita stato = new StatoPartita(4);
		GestoreComunicazioneStubPosizionamentoPastoriInizialiTest comunicazione = new GestoreComunicazioneStubPosizionamentoPastoriInizialiTest();
		GestorePartita gestore = new GestorePartita(stato, comunicazione, 2);
		
		//Muovo un pastore
		stato.setPastoreInUso(stato.getMappa().getStrada(32));
		stato.setGiocatoreGioca(stato.getGiocatore(0));
		String messaggio = "0"+":"+"36";
		gestore.testGetRunMossa(messaggio);
		//Controllo che il pastore sia stato mosso nella strada desiderata
		assertTrue(stato.getMappa().getStrada(36).getFlagGiocatore());
		
		//Muovo una pecora
		messaggio = "1"+":"+"14"+":"+"false";
		gestore.testGetRunMossa(messaggio);
		//Controllo che la mossa sia stata effettuata
		assertEquals(2, stato.getMappa().getRegione(16).getContatorePecore());
		
		//Compro una carta
		messaggio = "2"+":"+"foresta";
		int numCarteTipo = stato.getGiocatore(0).getNumeroCarteTipoN("foresta");
		gestore.testGetRunMossa(messaggio);
		//Controllo che la carta sia stata comprata
		assertEquals(numCarteTipo +1, stato.getGiocatore(0).getNumeroCarteTipoN("foresta"));
		
	}
	
	
	/**
	 * test del metodo runTurno: controlla che, passati dei parametri corretti ed essendo la mossa scelta valida,
	 * la funzione effettui un ciclo(= una mossa) nell'ambito di un turno
	 * @throws MossaNonValidaException 
	 */
	@Test
	public void testRunTurno() throws MossaNonValidaException{
		
		StatoPartita stato = new StatoPartita(4);
		GestoreComunicazioneStubParametriMossaTest comunicazione = new GestoreComunicazioneStubParametriMossaTest();
		GestorePartita gestore = new GestorePartita(stato, comunicazione, 4);
		
		stato.setGiocatoreGioca(stato.getGiocatore(0));
		stato.setPastoreInUso(stato.getMappa().getStrada(25));
		stato.setNumeroMosse(2);
		stato.setUltimaMossa(0);
		stato.setPastoreMosso(true);
		stato.getMosseValide()[2]=true;
		stato.getMosseValide()[0]=true;
		stato.getMosseValide()[1]=true;
		
		gestore.testGetRunTurno();
			
		
	}
	
	/**
	 * test del metodo controllaMuoviPecora nel caso in cui non sia lanciata 
	 * MossaNonValidaException
	 * @throws MossaNonValidaException
	 */
	@Test
	public void testControllaMuoviPecora() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		stato.setPastoreInUso(stato.getMappa().getStrada(5));
		stato.setIdRegionePecoraNera(3);
		stato.getMappa().getRegione(3).setPecoraNera(true);
		assertEquals(1, stato.getMappa().getRegione(3).getContatorePecore());
		assertEquals(3, stato.getIdRegionePecoraNera());
		
		//Muovo la pecora Bianca
		gestore.testGetControllaMuoviPecora(3, false);
		//Pecore incrementate nella regione di arrivo
		assertEquals(2, stato.getMappa().getRegione(7).getContatorePecore());
		//Pecore decrementate nella regione di partenza
		assertEquals(0, stato.getMappa().getRegione(3).getContatorePecore());
		//La pecora nera NON si è mossa
		assertFalse(stato.getMappa().getRegione(7).getPecoraNera());
		assertTrue(stato.getMappa().getRegione(3).getPecoraNera());
		
		//Muovo la pecora Nera
		gestore.testGetControllaMuoviPecora(3, true);
		//La pecora nera non è più nella regione di partenza, ma in quella di arrivo
		assertFalse(stato.getMappa().getRegione(3).getPecoraNera());
		assertTrue(stato.getMappa().getRegione(7).getPecoraNera());
		assertEquals(7, stato.getIdRegionePecoraNera());
		
		
			
			
			
		
	}
	
	/**
	 * testa che, qualora non ci siano pecore bianche nella regione di partenza
	 * e non si voglia muovere la pecora nera, sia lanciata MossaNonValidaException.
	 * @throws MossaNonValidaException 
	 */
	@Test(expected = MossaNonValidaException.class)
	public void testControllaMuoviPecoraThrownException1() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
			
		stato.getMappa().getRegione(9).diminuisciPecore();
		assertEquals(0, stato.getMappa().getRegione(9).getContatorePecore());
			
		gestore.testGetControllaMuoviPecora(9, false);
		
	}
	
	/**
	 * testa che sia lanciata MossaNonValidaException se la pecora che si vuole muovere non è 
	 * in una regione adiacente al pastore del giocatore che sta eseguendo il turno.
	 * @throws MossaNonValidaException 
	 */
	@Test(expected = MossaNonValidaException.class)
	public void testControllaMuoviPecoraThrownException2() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		stato.setPastoreInUso(stato.getMappa().getStrada(20));
		
		gestore.testGetControllaMuoviPecora(1, false);
		
		
		
		
	}
	
	
	/**
	 * testa che sia lanciata MossaNonValidaException nel caso in cui il giocatore tenti di far spostare 
	 * la pecora nera da una regione in cui la pecora nera non è presente.
	 * @throws MossaNonValidaException 
	 */
	@Test(expected = MossaNonValidaException.class)
	public void testControllaMuoviPecoraThrownException3() throws MossaNonValidaException {
		
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		stato.setPastoreInUso(stato.getMappa().getStrada(40));
		
		gestore.testGetControllaMuoviPecora(17, true);
		
	}
	
	
	
	
	/**
	 * testa che, in caso di controlli andati a buon fine,
	 * la carta sia comprata in modo corretto
	 * @throws MossaNonValidaException
	 */
	@Test
	public void testControllaCompraCarta() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		stato.setPastoreInUso(stato.getMappa().getStrada(22));
		stato.setGiocatoreGioca(stato.getGiocatore(0));
		
		//Giocatore0 compra la prima carta di tipo foresta
		//deve incrementarsi il costo delle carte e il numero delle carte in possesso
		//del giocatore, decrementarsi il denaro del giocatore
		int denaroIniziale = stato.getGiocatore(0).getDenaro();
		int carteTipo = stato.getGiocatore(0).getNumeroCarteTipoN("foresta");
		int costo = stato.getCostoCarteTipoNDisponibili("foresta");
		gestore.testGetControllaCompraCarta("foresta");

		assertEquals(costo+1, stato.getCostoCarteTipoNDisponibili("foresta"));
		assertEquals(carteTipo +1 ,stato.getGiocatore(0).getNumeroCarteTipoN("foresta"));
		assertEquals(denaroIniziale - costo, stato.getGiocatore(0).getDenaro());
		
		//Giocatore0 compra la seconda carta di tipo foresta
		//deve incrementarsi il costo delle carte e il numero delle carte in possesso
		//del giocatore, decrementarsi il denaro del giocatore
		denaroIniziale = stato.getGiocatore(0).getDenaro();
		carteTipo = stato.getGiocatore(0).getNumeroCarteTipoN("foresta");
		costo = stato.getCostoCarteTipoNDisponibili("foresta");
		gestore.testGetControllaCompraCarta("foresta");
		assertEquals(costo + 1, stato.getCostoCarteTipoNDisponibili("foresta"));
		assertEquals(carteTipo + 1 ,stato.getGiocatore(0).getNumeroCarteTipoN("foresta"));
		assertEquals(denaroIniziale - costo, stato.getGiocatore(0).getDenaro());
		
	}
	
	/**
	 * testa che sia lanciata MossaNonValidaException nel caso in cui il giocatore tenti di comprare una carta
	 * il cui tipo non appartenga a quelli delle regioni adiacenti a quella del pastore.
	 * @throws MossaNonValidaException
	 */
	@Test(expected = MossaNonValidaException.class)
	public void testControllaCompraCartaThrownException1() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		//Il pastore si trova sulla strada 36: le regioni adiacenti sono di tipo "foresta" e "palude"
		stato.setPastoreInUso(stato.getMappa().getStrada(36));
		gestore.testGetControllaCompraCarta("deserto");
		
	}
	
	/**
	 * testa che sia lanciata MossaNonValidaException se un giocatore tenta di comprare le carte di un certo tipo, ma queste sono già terminate.
	 * @throws MossaNonValidaException
	 */
	@Test(expected = MossaNonValidaException.class)
	public void testControllaCompraCartaThrownException2() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		//pongo il costo delle carte >4
		stato.setPastoreInUso(stato.getMappa().getStrada(25));
		for(int i=0; i<=6; i++){
			stato.setCarteDisponibili("pianura");
		}
		
		gestore.testGetControllaCompraCarta("pianura");	
		
	}
	
	/**
	 * testa che sia lanciata MossaNonValidaException se un giocatore tenta di comprare una carta, 
	 * ma non ha denaro sufficiente.
	 * @throws MossaNonValidaException
	 */
	@Test(expected = MossaNonValidaException.class)
	public void testControllaCompraCartaThrownException3() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		//Il giocatore in gioco non ha denaro disponibile
		stato.setPastoreInUso(stato.getMappa().getStrada(4));
		stato.setGiocatoreGioca(stato.getGiocatore(0));
		stato.getGiocatore(0).setDenaro(0);
		
		for(int i=0; i<=3; i++){
			stato.setCarteDisponibili("montagna");
		}
		
		gestore.testGetControllaCompraCarta("montagna");	
	}
	
	
	/**
	 * test del metodo controllaMuoviPastore nel caso in cui non sia lanciata 
	 * MossaNonValidaException
	 * @throws MossaNonValidaException
	 */
	@Test
	public void testControllaMuoviPastore() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		stato.setPastoreInUso(stato.getMappa().getStrada(23));
		stato.setGiocatoreGioca(stato.getGiocatore(0));
		
		int denaroIniziale = stato.getGiocatore(0).getDenaro();
		
		gestore.testGetControllaMuoviPastore(24);
		assertEquals(denaroIniziale, stato.getGiocatore(0).getDenaro());
		assertFalse(stato.getMappa().getStrada(23).getFlagGiocatore());
		assertTrue(stato.getMappa().getStrada(23).getFlagRecinto());
		assertTrue(stato.getMappa().getStrada(24).getFlagGiocatore());
		
		gestore.testGetControllaMuoviPastore(10);
		
		assertEquals(denaroIniziale -1, stato.getGiocatore(0).getDenaro());
		assertFalse(stato.getMappa().getStrada(24).getFlagGiocatore());
		assertTrue(stato.getMappa().getStrada(24).getFlagRecinto());
		assertTrue(stato.getMappa().getStrada(10).getFlagGiocatore());
	}
	
	/**
	 * testa che sia lanciata MossaNonValidaException se un giocatore tenta di muovere il suo pastore su una strada
	 * già occupata da un altro pastore.
	 * @throws MossaNonValidaException
	 */
	@Test(expected = MossaNonValidaException.class)
	public void testControllaMuoviPastoreThrownException1() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		stato.setPastoreInUso(stato.getMappa().getStrada(27));
		stato.getMappa().getStrada(26).setFlagGiocatore(true);
		
		gestore.testGetControllaMuoviPastore(26);
	}
	
	/**
	 * testa che sia lanciata MossaNonValidaException se un giocatore tenta di muovere il suo pastore su una strada
	 * già occupata da un recinto.
	 * @throws MossaNonValidaException
	 */
	@Test(expected = MossaNonValidaException.class)
	public void testControllaMuoviPastoreThrownException2() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		stato.setPastoreInUso(stato.getMappa().getStrada(18));
		stato.getMappa().getStrada(15).setFlagRecinto(true);
		
		gestore.testGetControllaMuoviPastore(15);
	}
	
	
	/**
	 * testa che sia lanciata MossaNonValidaException se un giocatore tenta di muovere il suo pastore su una strada
	 * non adiacente e non ha denaro sufficiente per eseguire la mossa.
	 * @throws MossaNonValidaException
	 */
	@Test(expected = MossaNonValidaException.class)
	public void testControllaMuoviPastoreThrownException3() throws MossaNonValidaException {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		stato.setPastoreInUso(stato.getMappa().getStrada(3));
		stato.setGiocatoreGioca(stato.getGiocatore(1));
		stato.getGiocatore(1).setDenaro(0);
		
		gestore.testGetControllaMuoviPastore(38);
		
	}
	
	
	
	/**
	 * Test del metodo controllaMosseDisponibili
	 */
	
	@Test
	public void testControllaMosseDisponibili() {
		
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		//Controllo che inizialmente tutte e tre le mosse siano valide
		for(int i=0; i<=2; i++){
			assertTrue(gestore.testGetControllaMosseDisponibili()[i]);
		}
		//Percorso 1: le prime due mosse hanno codice relativamente 1 e 2
		//Case 1, mossoPastore == false, ultimaMossa == 1
		stato.setNumeroMosse(1);
		gestore.testOttieniStato().setPastoreMosso(false);
		gestore.testOttieniStato().setUltimaMossa(1);
		assertTrue(gestore.testGetControllaMosseDisponibili()[0]);
		assertFalse(gestore.testGetControllaMosseDisponibili()[1]);
		assertTrue(gestore.testGetControllaMosseDisponibili()[2]);
		//Case 2, mossopastore == false, ultimaMossa == 2
		stato.setNumeroMosse(2);
		gestore.testOttieniStato().setUltimaMossa(2);
		assertTrue(gestore.testGetControllaMosseDisponibili()[0]&&
				!gestore.testGetControllaMosseDisponibili()[1]&&
				!gestore.testGetControllaMosseDisponibili()[2]);
		
		stato = new StatoPartita(2);
		gestore = new GestorePartita(stato, null, 2);
		for(int i=0; i<=2; i++){
			assertTrue(gestore.testGetControllaMosseDisponibili()[i]);
		}
		
		
		//Percorso 2: le prime due mosse hanno entrambe codice 0
		//Case 1, mossoPastore == true, ultimaMossa == 0
		stato.setNumeroMosse(1);
		gestore.testOttieniStato().setPastoreMosso(true);
		gestore.testOttieniStato().setUltimaMossa(0);
		assertTrue(gestore.testGetControllaMosseDisponibili()[0]&&
					gestore.testGetControllaMosseDisponibili()[1]&&
					gestore.testGetControllaMosseDisponibili()[2]);
		//Case 2, mossopastore == true, ultimaMossa == 0
		stato.setNumeroMosse(2);
		gestore.testOttieniStato().setUltimaMossa(0);
		assertTrue(gestore.testGetControllaMosseDisponibili()[0]&&
					gestore.testGetControllaMosseDisponibili()[1]&&
					gestore.testGetControllaMosseDisponibili()[2]);
		
		stato = new StatoPartita(2);
		gestore = new GestorePartita(stato, null, 2);
		
				for(int i=0; i<=2; i++){
					assertTrue(gestore.testGetControllaMosseDisponibili()[i]);
				}
		
		//Percorso 3: le prime due mosse hanno codice 0 e 1
		//Case 1, mossoPastore == true, ultimaMossa == 0
		stato.setNumeroMosse(1);
		gestore.testOttieniStato().setPastoreMosso(true);
		gestore.testOttieniStato().setUltimaMossa(0);
		assertTrue(gestore.testGetControllaMosseDisponibili()[0]&&
					gestore.testGetControllaMosseDisponibili()[1]&&
					gestore.testGetControllaMosseDisponibili()[2]);	
		//Case 2, mossopastore == true, ultimaMossa == 1
		stato.setNumeroMosse(2);
		gestore.testOttieniStato().setUltimaMossa(1);
		assertTrue(gestore.testGetControllaMosseDisponibili()[0]&&
					!gestore.testGetControllaMosseDisponibili()[1]&&
					gestore.testGetControllaMosseDisponibili()[2]);
		
		
		
		
	}

	

	
	/**
	 * Test del metodo controllaMossaValida: controlla che il metodo restituisca un risultato coerente
	 * con il contenuto dell'array valide[]
	 */
	@Test
	public void testControllaMossaValida() {
		
		StatoPartita stato = new StatoPartita(0);
		GestorePartita gestore = new GestorePartita(stato, null, 0);
		
		gestore.testOttieniStato().testGetMosseValide()[0] = true;
		gestore.testOttieniStato().testGetMosseValide()[1] = false;
		gestore.testOttieniStato().testGetMosseValide()[2] = true;
		
		assertTrue(gestore.testControllaMossaValida(gestore.testOttieniStato().getMosseValide(), "0"));
		assertFalse(gestore.testControllaMossaValida(gestore.testOttieniStato().getMosseValide(), "1"));
		assertTrue(gestore.testControllaMossaValida(gestore.testOttieniStato().getMosseValide(), "2"));
	}

	/**
	 * Test del metodo lanciaDado: controlla che dal lancio del dado risultino soltanto 
	 * valori compresi tra 0 e 6
	 */
	@Test
	public void testLanciaDado() {
		GestorePartita gestore = new GestorePartita(null, null, 0);
		for(int i=0; i<=20; i++){
			assertTrue((gestore.testGetLancioDado() >= 0)&&(gestore.testGetLancioDado() <= 6));
			
		}
	}
	
	/**
	 * Test del metodo muoviPecoraNera: controlla che, qualora la pecora nera si muova, i flag
	 * siano modificati coerentemente con lo spostamento
	 */
	@Test
	public void testMuoviPecoraNera() {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		
		assertTrue(stato.getMappa().getRegionePecoraNera().equals(stato.getMappa().getRegione(0)));
		
		boolean pecoraNeraMossa = gestore.testGetMuoviPecoraNera();
		
		int regione = stato.getIdRegionePecoraNera();
		
		assertEquals(!pecoraNeraMossa, stato.getMappa().getRegione(0).getPecoraNera());
		if(pecoraNeraMossa){
			assertTrue(stato.getMappa().getRegione(regione).getPecoraNera());
			assertFalse(stato.getMappa().getRegione(0).getPecoraNera());
		}else{
			assertEquals(stato.getMappa().getRegione(0), stato.getMappa().getRegione(stato.getIdRegionePecoraNera()));
			
		}
		
	}
	
	/**
	 * test del metodo muoviPecora : controlla che, a valle dei controlli di validità,
	 * la pecora sia mossa correttamente, decrementando il contatore delle pecore nella regione di partenza
	 * e incrementando quello nella regione di arrivo.
	 * Nel caso di pecora nera il contatore non si incrementa.
	 * 
	 */
	@Test
	public void testMuoviPecora() {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		Giocatore g = stato.getGiocatore(0);
		g.setPosizione(stato.getMappa().getStrada(17));
		stato.setPastoreInUso(stato.getMappa().getStrada(17));
		stato.getMappa().getRegione(0).setPecoraNera(true);
		
		//Mossa pecora bianca
		gestore.testGetMuoviPecora(9, false);
		assertEquals(0, stato.getMappa().getRegione(9).getContatorePecore());
		assertEquals(2, stato.getMappa().getRegione(8).getContatorePecore());
		
		
		//Mossa pecora nera
		assertEquals(1, stato.getMappa().getRegione(0).getContatorePecore());
		assertTrue(stato.getMappa().getRegione(0).getPecoraNera());
		g.setPosizione(stato.getMappa().getStrada(19));
		stato.setPastoreInUso(stato.getMappa().getStrada(19));
		gestore.testGetMuoviPecora(0, true);
		assertEquals(0, stato.getMappa().getRegione(0).getContatorePecore());
		assertEquals(2, stato.getMappa().getRegione(6).getContatorePecore());
		assertTrue(stato.getMappa().getRegione(6).getPecoraNera());
		
		
	}
	
	
	/**
	 * test del metodo compraCarta: controlla che, a valle dei controlli, il metodo incrementi il costo delle carte del
	 * tipo scelto, incrementi il numero delle carte del tipo acquistato, decrementi il denaro del giocatore
	 * coerentemente con il costo delle carte
	 */
	@Test
	public void testCompraCarta() {
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		stato.setGiocatoreGioca(stato.getGiocatore(0));
		
		int carteInizialiPianura = stato.getGiocatore(0).getNumeroCarteTipoN("pianura");
		int carteInizialiDeserto = stato.getGiocatore(0).getNumeroCarteTipoN("deserto");
		
		//Acquisto di una carta pianura e 3 carte deserto
		
		int denaroSpeso = stato.getCostoCarteTipoNDisponibili("pianura");
		gestore.testGetCompraCarta("pianura");
		denaroSpeso = denaroSpeso + stato.getCostoCarteTipoNDisponibili("deserto");
		gestore.testGetCompraCarta("deserto");
		denaroSpeso = denaroSpeso + stato.getCostoCarteTipoNDisponibili("deserto");
	    gestore.testGetCompraCarta("deserto");
		denaroSpeso = denaroSpeso + stato.getCostoCarteTipoNDisponibili("deserto");
	    gestore.testGetCompraCarta("deserto");
		
		//Controllo che il costo delle carte sia stato incrementato,
		//che le carte del giocatore diano state incrementate
		// e che il denaro del giocatore sia stato decrementato
		assertEquals(1, stato.getCostoCarteTipoNDisponibili("pianura"));
		assertEquals(3, stato.getCostoCarteTipoNDisponibili("deserto"));
		assertEquals(carteInizialiPianura+1 , stato.getGiocatore(0).getNumeroCarteTipoN("pianura"));
		assertEquals(carteInizialiDeserto+3 , stato.getGiocatore(0).getNumeroCarteTipoN("deserto"));
		assertEquals(30-denaroSpeso, stato.getGiocatore(0).getDenaro());
		
	}
	
	
	/**
	 * test del metodo muoviPastore: controlla che, a valle dei controlli il pastore sia spostato nella regione indicata,
	 * che sia tolto il flag del giocatore e posizionato quello del recinto nella strada di partenza,
	 * e che la nuova posizione sia inserita come nuova posizione del giocatore.
	 * Si controlla anche che il senaro sia decrementato in caso di spostamento su strada non adiacente.
	 * 
	 */
	@Test
	public void testMuoviPastore() {
		
		StatoPartita stato = new StatoPartita(2);
		GestorePartita gestore = new GestorePartita(stato, null, 2);
		stato.getGiocatore(0).setInGioco(true);
		stato.setGiocatoreGioca(stato.getGiocatore(0));
		stato.setPastoreInUso(stato.getMappa().getStrada(0));
		stato.getGiocatore(0).setPosizione(stato.getMappa().getStrada(0));
		
		//pastore mosso da 0 a 1: si controlla la nuova posizione, i flag e il denaro.
		gestore.testGetMuoviPastore(1);
		assertTrue(stato.getGiocatore(0).getPosizioni().contains(stato.getMappa().getStrada(1)));
		assertEquals(30, stato.getGiocatore(0).getDenaro());
		assertFalse(stato.getMappa().getStrada(0).getFlagGiocatore());
		assertTrue(stato.getMappa().getStrada(0).getFlagRecinto());
		assertTrue(stato.getMappa().getStrada(1).getFlagGiocatore());
		
		//pastore mosso da 1 a 40: si controlla la nuova posizione, i flag e il denaro.
		gestore.testGetMuoviPastore(40);
		assertTrue(stato.getGiocatore(0).getPosizioni().contains(stato.getMappa().getStrada(40)));
		assertFalse(stato.getGiocatore(0).getPosizioni().contains(stato.getMappa().getStrada(1)));
		assertEquals(29, stato.getGiocatore(0).getDenaro());
		
		
	}
	
	
	private class GestoreComunicazioneStubPosizionamentoPastoriInizialiTest implements GestoreComunicazione {

		@Test
		@Override
		public void notificaGiocatore(int numeroGiocatore, String codice) {
		
		}

		@Test
		@Override
		public void notificaTutti(String codice) {
		
		}

		@Test
		@Override
		public String ottieniParametri(String stream) {
			
			Random random = new Random();
			
			String risposta = Integer.toString(random.nextInt(50));
			
			return risposta;
			
			}
					

	}

	private class GestoreComunicazioneStubParametriMossaTest implements GestoreComunicazione {

		@Test
		@Override
		public void notificaGiocatore(int numeroGiocatore, String codice) {
		
		}

		@Test
		@Override
		public void notificaTutti(String codice) {
		
		}

		@Test
		@Override
		public String ottieniParametri(String stream) {
			
			String messaggio = "2"+":"+"pianura";
			
			return messaggio;
			
			
			}
					

	}
	
	private class GestorePartitaStub extends GestorePartita{
		
		
		public GestorePartitaStub(StatoPartita s, GestoreComunicazione g,int numGiocatori){
			
			super(s, g, numGiocatori);
		}
		
		public void runTurno(){};
		
		
	}

}
