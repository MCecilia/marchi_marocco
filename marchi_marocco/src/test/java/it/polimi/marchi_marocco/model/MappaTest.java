package it.polimi.marchi_marocco.model;

import static org.junit.Assert.*;

import java.util.Set;

import it.polimi.marchi_marocco.model.Mappa;
import it.polimi.marchi_marocco.model.Regione;
import it.polimi.marchi_marocco.model.Strada;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class MappaTest {
	
	
	
	@BeforeClass
	public static void testSetup(){
  
		
	}
	
	@AfterClass
	public static void testCleanup(){
		
	}
	
	@Test
	public void testGetRegioneAdiacente(){
		//Controlla che le regioni adiacenti alla strada passata siano corrette, 
		//ovvero che siano tutte e sole le regioni adiacenti.
		Mappa mappa = new Mappa();
		Strada stradaTest = mappa.getStrada(0);
		Regione regioneTest1 = mappa.getRegione(1);
		Regione regioneTest2 = mappa.getRegione(2);
		Regione regioneTest3 = mappa.getRegione(3);
		assertTrue("La regione 1 è adiacente alla strada 0!", mappa.getRegioneAdiacente(stradaTest).contains(regioneTest1));
		assertTrue("La regione 2 è adiacente alla strada 0!", mappa.getRegioneAdiacente(stradaTest).contains(regioneTest2));
		assertFalse("La regione 3 NON è adiacente alla strada 0!", mappa.getRegioneAdiacente(stradaTest).contains(regioneTest3));
	}
	
	@Test
	public void testGetRegionePecoraNera(){
		//Controllare che SOLO una regione contenga la pecora nera!!!!!!
		Mappa mappa = new Mappa();
		Regione regioneTest1 = mappa.getRegione(1);
		regioneTest1.setPecoraNera(true);
		Regione regioneTest2 = mappa.getRegione(2);
		regioneTest2.setPecoraNera(false);
		assertEquals("La pecora nera è nella regione 1!", regioneTest1, mappa.getRegionePecoraNera());
		assertFalse("La pecora nera non è nella regione 2!", regioneTest2 == mappa.getRegionePecoraNera());
		regioneTest1.setPecoraNera(false);
		regioneTest1 = mappa.getRegione(0);
		regioneTest1.setPecoraNera(true);
		assertEquals("La pecora nera è a Sheepsburg!", regioneTest1, mappa.getRegionePecoraNera());
		
		
		
	}
	
	@Test
	public void testGetStradeAdiacentiAStrada(){
		//Controlla che le trade adiacenti ad una strada siano corrette,
		//e che una strada non risulti mai adiacente a se stessa
		Mappa mappa = new Mappa();
		Strada stradaTest = mappa.getStrada(0);
		assertTrue("Strada 0 adiacente a strada 1!", mappa.getStradeAdiacenti(stradaTest).contains(mappa.getStrada(1)));
		assertTrue("Strada 0 adiacente a strada 2!", mappa.getStradeAdiacenti(stradaTest).contains(mappa.getStrada(2)));
		assertFalse("Strada 0 NON adiacente a se stessa!", mappa.getStradeAdiacenti(stradaTest).contains(stradaTest));
		assertFalse("Strada 0 NON adiacente a strada 3!", mappa.getStradeAdiacenti(stradaTest).contains(mappa.getStrada(3)));
		
		stradaTest = mappa.getStrada(13);
		assertTrue("Strada 13 adiacente a strada 2!", mappa.getStradeAdiacenti(stradaTest).contains(mappa.getStrada(2)));
		assertTrue("Strada 13 adiacente a strada 14!", mappa.getStradeAdiacenti(stradaTest).contains(mappa.getStrada(14)));
		assertTrue("Strada 13 adiacente a strada 3!", mappa.getStradeAdiacenti(stradaTest).contains(mappa.getStrada(3)));
		assertTrue("Strada 13 adiacente a strada 5!", mappa.getStradeAdiacenti(stradaTest).contains(mappa.getStrada(5)));
		assertFalse("Strada 13 NON adiacente a se stessa!", mappa.getStradeAdiacenti(stradaTest).contains(stradaTest));
		assertFalse("Strada 13 NON adiacente a strada 8!", mappa.getStradeAdiacenti(stradaTest).contains(mappa.getStrada(8)));
		
		
		
	}
	
	@Test
	public void testGetStradeAdiacentiARegione(){
		//Controlla che le strade adiacenti ad una regione siano restituite correttamente
		Mappa mappa = new Mappa();
		Regione regioneTest = mappa.getRegione(1);
		assertTrue("regione 1 adiacente a strada 0", mappa.controllaRegioneAdiacenteStrada(mappa.getStrada(0), mappa.getRegione(1)));
		assertTrue("regione 1 adiacente a strada 1", mappa.getStradeAdiacenti(regioneTest).contains(mappa.getStrada(1)));
		assertTrue("regione 1 adiacente a strada 16", mappa.getStradeAdiacenti(regioneTest).contains(mappa.getStrada(16)));
		assertFalse("regione 1 NON adiacente a strada 17", mappa.controllaRegioneAdiacenteStrada(mappa.getStrada(17), mappa.getRegione(1)));
		
		regioneTest = mappa.getRegione(0);
		assertTrue("regione 0 adiacente a strada 15", mappa.getStradeAdiacenti(regioneTest).contains(mappa.getStrada(15)));
		assertTrue("regione 0 adiacente a strada 18", mappa.getStradeAdiacenti(regioneTest).contains(mappa.getStrada(18)));
		assertTrue("regione 0 adiacente a strada 19", mappa.getStradeAdiacenti(regioneTest).contains(mappa.getStrada(19)));
		assertTrue("regione 0 adiacente a strada 21", mappa.getStradeAdiacenti(regioneTest).contains(mappa.getStrada(21)));
		assertTrue("regione 0 adiacente a strada 22", mappa.getStradeAdiacenti(regioneTest).contains(mappa.getStrada(22)));
		assertTrue("regione 0 adiacente a strada 23", mappa.getStradeAdiacenti(regioneTest).contains(mappa.getStrada(23)));
		assertFalse("regione 0 NON adiacente a strada 30", mappa.getStradeAdiacenti(regioneTest).contains(mappa.getStrada(30)));
		
	}
	
	@Test
	public void testControllaIdRegione(){
		
		for(int i=0; i<=18; i++){
			assertTrue(Mappa.controllaIdRegione(i));
		}
		
		assertFalse(Mappa.controllaIdRegione(20));
		
		
		
	}
	
	@Test
	public void testControllaIdStrada(){
		
		for(int i=0; i<=41; i++){
			assertTrue(Mappa.controllaIdStrada(Integer.toString(i)));
		}
		
		assertFalse(Mappa.controllaIdStrada(Integer.toString(50)));
		
		
	}
	
	@Test
	public void testControllaTipiRegione(){
		
		assertTrue(Mappa.controllaTipiRegione("pianura"));
		assertTrue(Mappa.controllaTipiRegione("foresta"));
		assertTrue(Mappa.controllaTipiRegione("deserto"));
		assertTrue(Mappa.controllaTipiRegione("campo"));
		assertTrue(Mappa.controllaTipiRegione("palude"));
		assertTrue(Mappa.controllaTipiRegione("montagna"));
		
		assertFalse(Mappa.controllaTipiRegione("oceano"));
		
		
	}


}
