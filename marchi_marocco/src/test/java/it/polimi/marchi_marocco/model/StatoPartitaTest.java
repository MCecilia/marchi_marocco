package it.polimi.marchi_marocco.model;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class StatoPartitaTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	/**
	 * testa il costruttore di StatoPartita, controllando che i valori siano settati correttamente.
	 */
	@Test
	public void testStatoPartita() {
		int numGiocatori = 4;
		StatoPartita stato1 = new StatoPartita(numGiocatori);
		
		assertEquals(20, stato1.getGiocatore(0).getDenaro());
		assertEquals(20, stato1.getGiocatore(1).getDenaro());
		assertEquals(20, stato1.getRecinti());
		assertEquals(0, stato1.getIdRegionePecoraNera());
		for (int i = 0; i<=2; i++) {
			assertTrue(stato1.getMosseValide()[i]);
			
		}
		assertFalse(stato1.getPastoreMosso());
		
		numGiocatori = 4;
		StatoPartita stato2 = new StatoPartita(numGiocatori);
		assertEquals(20, stato2.getGiocatore(0).getDenaro());
		assertEquals(20, stato2.getGiocatore(1).getDenaro());
		assertEquals(20, stato2.getGiocatore(2).getDenaro());
		assertEquals(20, stato2.getGiocatore(3).getDenaro());
			
		
	}

	/**
	 * testa il metodo getCostoCarteDisponibili, controllando che a inizio partita le carte abbiano costo 0 e che
	 * chiamando setCarteDisponibili, il costo del tipo carta selezionato sia incrementato.
	 */
	@Test
	public void testGetCostoCarteTipoNDisponibili() {
		
		int numGiocatori = 2;
		StatoPartita stato1 = new StatoPartita(numGiocatori);
		
		assertEquals(0, stato1.getCostoCarteTipoNDisponibili("foresta"));
		assertEquals(0, stato1.getCostoCarteTipoNDisponibili("pianura"));
		assertEquals(0, stato1.getCostoCarteTipoNDisponibili("campo"));
		assertEquals(0, stato1.getCostoCarteTipoNDisponibili("montagna"));
		assertEquals(0, stato1.getCostoCarteTipoNDisponibili("deserto"));
		assertEquals(0, stato1.getCostoCarteTipoNDisponibili("palude"));
		
		stato1.setCarteDisponibili("foresta");
		assertEquals(1, stato1.getCostoCarteTipoNDisponibili("foresta"));
		
	}
	
	/**
	 * Testa il metodo calcolaVincitori
	 */
	
	@Test
	public void testCalcolaVincitori() {
		int numGiocatori = 4;
		StatoPartita stato1 = new StatoPartita(numGiocatori);
		
		for (int i=0; i<=5; i++){
			stato1.getGiocatore(0).getCarteTerreno()[i] = 0;
			stato1.getGiocatore(1).getCarteTerreno()[i] = 0;
			stato1.getGiocatore(2).getCarteTerreno()[i] = 0;
			stato1.getGiocatore(3).getCarteTerreno()[i] = 0;
		}
		
		stato1.getGiocatore(0).setCartaTipoN("foresta") ;
		stato1.getGiocatore(3).setCartaTipoN("foresta") ;
		stato1.getGiocatore(3).setCartaTipoN("pianura") ;
		
		assertEquals(stato1.calcolaVincitore()[0], stato1.getGiocatore(3).getNomeGiocatore());
		assertEquals(1, stato1.calcolaVincitore().length);
		
		
	}
	
	/**
	 * Testa il metodo assegnaCarteIniziali, controllando che, sia in caso di 2 che 4 giocatori, 
	 * si assegnata una sola carta per giocatore e che ne sia assegnata soltanto una per ogni tipo.
	 */
	@Test
	public void testAssegnaCarteIniziali() {
		
		//Caso 2 giocatori
		StatoPartita stato1 = new StatoPartita(2);
		//Carte assegnate tutte di tipo diverso
		for( int i= 0; i<=5; i++){
			if (stato1.getGiocatore(0).getCarteTerreno()[i] == 1) {
				assertTrue(stato1.getGiocatore(1).getCarteTerreno()[i] != 1);
			}
		}
		
		//Una sola carta per giocatore
		int carteTotali = 0;
		
		for( int i= 0; i<=5; i++){
			carteTotali = carteTotali + stato1.getGiocatore(0).getCarteTerreno()[i];
		}
		assertEquals(1, carteTotali);
		
        carteTotali = 0;
		
		for( int i= 0; i<=5; i++){
			carteTotali = carteTotali + stato1.getGiocatore(1).getCarteTerreno()[i];
		}
		assertEquals(1, carteTotali);
		
		//Caso 4 giocatori
		StatoPartita stato2 = new StatoPartita(4);
		//Carte assegnate tutte di tipo diverso
		for( int i= 0; i<=5; i++){
			if (stato2.getGiocatore(0).getCarteTerreno()[i] == 1) {
				assertTrue(stato2.getGiocatore(1).getCarteTerreno()[i] != 1);
				assertTrue(stato2.getGiocatore(2).getCarteTerreno()[i] != 1);
				assertTrue(stato2.getGiocatore(3).getCarteTerreno()[i] != 1);
			}
		}
		
		for( int i= 0; i<=5; i++){
			if (stato2.getGiocatore(1).getCarteTerreno()[i] == 1) {
				
				assertTrue(stato2.getGiocatore(2).getCarteTerreno()[i] != 1);
				assertTrue(stato2.getGiocatore(3).getCarteTerreno()[i] != 1);
			}
		}
		
		for( int i= 0; i<=5; i++){
			if (stato2.getGiocatore(2).getCarteTerreno()[i] == 1) {
				
				assertTrue(stato2.getGiocatore(3).getCarteTerreno()[i] != 1);
			}
		}
		
		
		//Una sola carta per giocatore
		//Giocatore 0
		carteTotali = 0;
		for( int i= 0; i<=5; i++){
			carteTotali = carteTotali + stato2.getGiocatore(0).getCarteTerreno()[i];
		}
		assertEquals(1, carteTotali);
		
		//Giocatore 1
        carteTotali = 0;
		for( int i= 0; i<=5; i++){
			carteTotali = carteTotali + stato2.getGiocatore(1).getCarteTerreno()[i];
		}
		assertEquals(1, carteTotali);
		
		//Giocatore 2
		carteTotali = 0;	
		for( int i= 0; i<=5; i++){
				carteTotali = carteTotali + stato2.getGiocatore(2).getCarteTerreno()[i];
			}
		assertEquals(1, carteTotali);
		
		//Giocatore 3
		carteTotali = 0;
		for( int i= 0; i<=5; i++){
				carteTotali = carteTotali + stato2.getGiocatore(3).getCarteTerreno()[i];
			}
		assertEquals(1, carteTotali);
	}
	
	
	/**
	 * testa il metodo contaPecoreTipoRegione
	 */
	@Test
	public void testContaPecoreTipoRegione() {
		
		StatoPartita stato = new StatoPartita(2);
		int pecoreTotali = 0;
        pecoreTotali = pecoreTotali + stato.testGetContaPecoreTipoRegione("foresta");
        pecoreTotali = pecoreTotali + stato.testGetContaPecoreTipoRegione("pianura");
        pecoreTotali = pecoreTotali + stato.testGetContaPecoreTipoRegione("campo");
        pecoreTotali = pecoreTotali + stato.testGetContaPecoreTipoRegione("montagna");
        pecoreTotali = pecoreTotali + stato.testGetContaPecoreTipoRegione("deserto");
        pecoreTotali = pecoreTotali + stato.testGetContaPecoreTipoRegione("palude");
        pecoreTotali = pecoreTotali + stato.testGetContaPecoreTipoRegione("sheepsburg");
        
        assertEquals(21, pecoreTotali);
    }
	
	/**
	 * controlla che i setters e i getters della classe StatoPartita funzionino correttamente.
	 */
	public void testSettersGettersStatoPartita(){
		StatoPartita stato = new StatoPartita(4);
		
		//Set e get pastoreMosso
		stato.setPastoreMosso(true);
		assertTrue(stato.getPastoreMosso());
		
		//set e get idRegionePecoranera
		stato.setIdRegionePecoraNera(0);
		assertEquals(0, stato.getIdRegionePecoraNera());
		
		//set e get pastoreInUso
		stato.setPastoreInUso(stato.getMappa().getStrada(5));
		assertEquals(stato.getMappa().getStrada(5), stato.getPastoreInUso());
		assertFalse(stato.getPastoreInUso().equals(stato.getMappa().getStrada(8)));
		
		//set e get di giocatoreGioca
		stato.setGiocatoreGioca(stato.getGiocatore(0));
		assertEquals(stato.getGiocatore(0), stato.getGiocatoreGioca());
		assertFalse(stato.getGiocatoreGioca().equals(stato.getGiocatore(1)));
		
		//set e get numeroMosse
		stato.setNumeroMosse(5);
		assertEquals(5, stato.getNumeroMosse());
		assertFalse(6 == stato.getNumeroMosse());
		
		
		
		
		
		
	}
		
		
		
	
	
}
