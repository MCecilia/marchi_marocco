package it.polimi.marchi_marocco.model;
import static org.junit.Assert.*;
import it.polimi.marchi_marocco.model.Giocatore;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


public class GiocatoreTest {
	//Testa i metodi della classe Giocatore
	
	@BeforeClass
	public static void testSetup(){
		
	}

	@AfterClass
	public static void testCleanup(){
		
	}
	
	@Test
	public void testDecrementaDenaro(){
		//Testa che il metodo decrementaDenaro ritorni il risultato corretto
		Giocatore giocatoreTester = new Giocatore(5, "mario");
		giocatoreTester.setDenaro(30);
		giocatoreTester.decrementaDenaro(3);
		assertEquals("30-3 = 27!", 27, giocatoreTester.getDenaro());
	}
		
	@Test
	public void testGetNumeroCarteTipoN(){
		//Testa che le carte siano aggiunte correttamente e che se ne legga correttamente il numero
		Giocatore giocatoreTester = new Giocatore(5, "mario");
		giocatoreTester.setCartaTipoN("pianura");
		assertEquals("Errore lettura numero carte!", 1, giocatoreTester.getNumeroCarteTipoN("pianura"));
	}
	
	
}
