package it.polimi.marchi_marocco.model;
import static org.junit.Assert.*;
import it.polimi.marchi_marocco.model.MapTipoRegione;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;


public class MapTipoRegioneTest {
	
	@BeforeClass
	public static void testSetup(){
		
	}

	@AfterClass
	public static void testCleanup(){
		
	}
	
	
	@Test
	public void testGetValoreTipoRegione() {
		MapTipoRegione tester = new MapTipoRegione();
		assertEquals("foresta ha valore 0!", 0, tester.getValoreTipoRegione("foresta"));
		assertEquals("pianura ha valore 1!", 1, tester.getValoreTipoRegione("pianura"));
		assertEquals("campo ha valore 2!", 2, tester.getValoreTipoRegione("campo"));
		assertEquals("montagna ha valore 3!", 3, tester.getValoreTipoRegione("montagna"));
		assertEquals("deserto ha valore 4!", 4, tester.getValoreTipoRegione("deserto"));
		assertEquals("palude ha valore 5!", 5, tester.getValoreTipoRegione("palude"));
		assertEquals("Sheepsburg ha valore 6!", 6, tester.getValoreTipoRegione("sheepsburg"));
	}

}
