package it.polimi.marchi_marocco.view;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLayeredPane;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.UnsupportedLookAndFeelException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.border.LineBorder;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import javax.swing.border.TitledBorder;
import javax.swing.text.DefaultCaret;
import javax.swing.UIManager;

import java.awt.Font;


public class Mappa implements Observer {
    
	private JPanel contentPane;
	static List<Strada> strade = new ArrayList<Strada>();
	Bottone muoviPastore;
	Bottone muoviPecora;
	Bottone compraCarta;
	Bottone[] carteTerreno = new Bottone[6];
	PecoraBianca[] pecoreBianche = new PecoraBianca[19];
	Point[] pecoreNere = new Point[19];
	static JLayeredPane panel;
	
	//Array dei giocatori a seconda del numero di partecipanti
	Pedina[] pastoriGiocatore1 = new Pedina[2];
	Pedina[] pastoriGiocatore2 = new Pedina[2];
	Pedina[] pastori= new Pedina[4];
	String denaro = "0";
	String idGiocatore;
	String nomeGiocatore;
	
	
	//Label e valore delle carte giocatore
	JTextField lblNumcarte0;
	JTextField lblNumcarte1;
	JTextField lblNumcarte2;
	JTextField lblNumcarte3;
	JTextField lblNumcarte4;
	JTextField lblNumcarte5;
	String numCarteTipo0 = "0";
	String numCarteTipo1 = "0";
	String numCarteTipo2 = "0";
	String numCarteTipo3 = "0";
	String numCarteTipo4 = "0";
	String numCarteTipo5 = "0";
	
	//Valore e Label delle carteTerreno
	JLabel costoForesta;
	JLabel costoPianura;
	JLabel costoMontagna;
	JLabel costoPalude;
	JLabel costoDeserto;
	JLabel costoCampo;
	String valorePianura ="0";
	String valoreForesta ="0";
	String valoreCampo ="0";
	String valoreMontagna ="0";
	String valorePalude ="0";
	String valoreDeserto ="0";
	//Label che rappresenta il denaro giocatore
	JLabel denaroGiocatore;
	//Label che rappresenta il numero di recinti
	JLabel recinti;
	private String numeroRecinti = "20";
	PecoraNera nera;
	JTextArea textArea;
	JLabel giocatore;
	
	//Valore della mossa scelta
	String codiceMossa;
	
	ImageIcon nomeGiocatore1 = new ImageIcon(getClass().getResource("Giocatore1.png"));
	ImageIcon nomeGiocatore2 = new ImageIcon(getClass().getResource("Giocatore2.png"));
	ImageIcon nomeGiocatore3 = new ImageIcon(getClass().getResource("Giocatore3.png"));
	ImageIcon nomeGiocatore4 = new ImageIcon(getClass().getResource("Giocatore4.png"));

	
	/**
	 * Create the frame.
	 * @param  
	 * @throws IOException 
	 */
	public Mappa() throws IOException {
		
		//imposto il look and feel Nimbus
		try {
		UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException e1) {
		Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore creazione Frame principale", e1);
		} catch(InstantiationException e2){
		Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore creazione Frame principale", e2);
		}catch( IllegalAccessException e3) {
		Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore creazione Frame principale", e3);
		}catch(UnsupportedLookAndFeelException e4) {
			Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore creazione Frame principale", e4);
		}
		
		//Caricamento immagini
		ImageIcon terreno0 = new ImageIcon(getClass().getResource("/foresta1.png"));
		ImageIcon terreno0a = new ImageIcon(getClass().getResource("/foresta.png"));
		ImageIcon pianuraDis = new ImageIcon(getClass().getResource("/pianuraScura1.png"));
		ImageIcon pecoraBianca = new ImageIcon(getClass().getResource("/pecoraBianca.png"));
		ImageIcon pecoraNera = new ImageIcon(getClass().getResource("/pecoraNera.png"));
		ImageIcon iconPastore = new ImageIcon(getClass().getResource("/pastore1.png"));
		ImageIcon iconPecora = new ImageIcon(getClass().getResource("/pecoraIcon1.png"));
		ImageIcon iconCarta = new ImageIcon(getClass().getResource("/cartaTerreno1.png"));
		ImageIcon iconCarta1 = new ImageIcon(getClass().getResource("/cartaTerreno.png"));
		ImageIcon campoDis = new ImageIcon(getClass().getResource("/campoScuro1.png"));
		ImageIcon desertoDis = new ImageIcon(getClass().getResource("/desertoScuro1.png"));
		ImageIcon forestaDis = new ImageIcon(getClass().getResource("/forestaScura1.png"));
		ImageIcon paludeDis = new ImageIcon(getClass().getResource("/paludeScuro1.png"));
		ImageIcon montagnaDis = new ImageIcon(getClass().getResource("/montagnaScuro1.png"));
		ImageIcon terreno1 = new ImageIcon(getClass().getResource("/pianura1.png"));
		ImageIcon terreno4 = new ImageIcon(getClass().getResource("/deserto1.png"));
		ImageIcon terreno2 = new ImageIcon(getClass().getResource("/campo1.png"));
		ImageIcon terreno5 = new ImageIcon(getClass().getResource("/palude1.png"));
		ImageIcon terreno3 = new ImageIcon(getClass().getResource("/montagna1.png"));
		ImageIcon soldi = new ImageIcon(getClass().getResource("/coinsIcon.png"));
		ImageIcon recinto = new ImageIcon(getClass().getResource("/RecintoIcon.png"));
		ImageIcon pastoreDis = new ImageIcon(getClass().getResource("/pastoreScuro1.png"));
		ImageIcon pecoraDis = new ImageIcon(getClass().getResource("/pecoraIconScura1.png"));
		ImageIcon cartaDis = new ImageIcon(getClass().getResource("/cartaTerrenoScura1.png"));
				
		JFrame frame = new JFrame();
	    frame.setIconImage(iconCarta1.getImage());
	    frame.setResizable(false);
		frame.setTitle("SHEEPLAND");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(10, 10, 1053, 643);
		contentPane = new JPanel();
		contentPane.setForeground(new Color(0, 0, 0));
		contentPane.setBackground(new Color(31, 142, 247));
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);
		
		//JPanel della mappa
		panel = new JLayeredPane();
		panel.setBackground(new Color(31, 142, 247));
		panel.setBounds(0, 0, 436, 599);
		contentPane.add(panel);
		panel.setLayout(null); 
		
		//Istanzazione dei JButton[]
		//Strade	
		for(int i = 0; i< 42; i++) {
			
			Strada s = new Strada(i);
			strade.add(i, s);
			s.setEnabled(false);
			s.getList().addObserver(this);
			panel.add(s, Integer.valueOf(3));
		}
		
		//Creazione pecore bianche
		for(int j = 0; j <= 18; j++) {
			
			pecoreBianche[j] = new PecoraBianca(pecoraBianca, j);
			pecoreBianche[j].getList().addObserver(this);
	
		}
		
		//Creazione pecore nere, creo solo la prima a sheepsbourg
			
			nera = new PecoraNera(pecoraNera, 0);
			nera.getList().addObserver(this);
			nera.setLocation(210, 245);
			panel.add(nera, Integer.valueOf(4));
			nera.setVisible(true);
			
		//Posizionamento pecore bianche
		
		
		pecoreBianche[0].setBounds(220, 271, 37, 31);
		pecoreBianche[0].setText("0");
		pecoreBianche[0].repaint();
		
		pecoreBianche[1].setBounds(54, 123, 34, 31);
		
		pecoreBianche[2].setBounds(139, 111, 34, 31);
		
		pecoreBianche[3].setBounds(268, 51, 34, 31);
	
		pecoreBianche[4].setBounds(338, 96, 37, 29);

		pecoreBianche[5].setBounds(361, 169, 34, 31);
	
		pecoreBianche[6].setBounds(297, 206, 34, 24);

		pecoreBianche[7].setBounds(235, 166, 31, 24);
	
		pecoreBianche[8].setBounds(151, 231, 34, 24);
	
		pecoreBianche[9].setBounds(88, 231, 30, 31);
	
		pecoreBianche[10].setBounds(347, 279, 34, 31);
	
		pecoreBianche[11].setBounds(278, 294, 34, 24);
	
		pecoreBianche[12].setBounds(155, 318, 30, 24);
	
		pecoreBianche[13].setBounds(84, 355, 30, 31);
	
		pecoreBianche[14].setBounds(210, 382, 30, 24);
		
		pecoreBianche[15].setBounds(338, 340, 37, 31);
		
		pecoreBianche[16].setBounds(282, 415, 30, 31);
	
		pecoreBianche[17].setBounds(213, 459, 27, 31);
		
		pecoreBianche[18].setBounds(151, 423, 37, 31);
		
		for(int j = 0; j <= 18; j++){
			
			panel.add(pecoreBianche[j], Integer.valueOf(2));
			pecoreBianche[j].setVisible(true);
			
		}
		
		//JLabel della mappa
		JLabel lblNewLabel = new JLabel(new ImageIcon(Mappa.class.getResource("/it/polimi/marchi_marocco/view/mappatry.jpeg")), Integer.valueOf(0));
		lblNewLabel.setBounds(6, 6, 424, 588);
		panel.add(lblNewLabel, Integer.valueOf(0));
		
		//Punti delle pecore nere
		pecoreNere[0] = new Point(210,245);
		
		pecoreNere[1] = new Point(65, 166);
		 
		pecoreNere[2] = new Point(180, 111);
		
		pecoreNere[3] = new Point(249, 94);
		
		pecoreNere[4] = new Point(295, 123);
		
		pecoreNere[5] = new Point(374, 140);
		
		pecoreNere[6] = new Point(282, 231);
		
		pecoreNere[7] = new Point(223, 195);
		
		pecoreNere[8] = new Point(161, 195);
		
		pecoreNere[9] = new Point(84, 279);
		
		pecoreNere[10]= new Point(361, 238);
		
		pecoreNere[11] = new Point(282, 325);
		
		pecoreNere[12] = new Point(165, 354);
		
		pecoreNere[13] = new Point(84, 394);
		
		pecoreNere[14] = new Point(223, 340);
		
		pecoreNere[15] = new Point(338, 375);
		
		pecoreNere[16] = new Point(276, 447);
		
		pecoreNere[17] = new Point(196, 492);
		
		pecoreNere[18] = new Point(119, 459);
		
		//Posizionamento strade
		strade.get(0).setBounds(108, 142, 16, 18);
		
		strade.get(1).setBounds(131, 183, 16, 17);
		
		strade.get(2).setBounds(173, 171, 16, 18);
		
		strade.get(3).setBounds(216, 90, 16, 18);
	
		strade.get(4).setBounds(297, 96, 16, 18);
		
		strade.get(5).setBounds(249, 136, 16, 18);

		strade.get(6).setBounds(347, 142, 21, 18);
		
		strade.get(7).setBounds(302, 182, 16, 18);

		strade.get(8).setBounds(280, 160, 16, 18);
	
		strade.get(9).setBounds(336, 196, 21, 18);
		
		strade.get(10).setBounds(374, 207, 16, 18);
	
		strade.get(11).setBounds(329, 242, 16, 18);
		
		strade.get(12).setBounds(266, 202, 16, 18);
		
		strade.get(13).setBounds(216, 148, 16, 18);
		
		strade.get(14).setBounds(203, 196, 16, 18);
	
		strade.get(15).setBounds(220, 219, 21, 18);
	
		strade.get(16).setBounds(71, 207, 16, 18);
	
		strade.get(17).setBounds(131, 235, 16, 18);
	
		strade.get(18).setBounds(186, 242, 16, 18);
	
		strade.get(19).setBounds(254, 242, 16, 18);
	
		strade.get(20).setBounds(285, 271, 16, 18);
	
		strade.get(21).setBounds(259, 287, 16, 18);
		
		strade.get(22).setBounds(225, 309, 16, 18);
	
		strade.get(23).setBounds(191, 287, 16, 18);
	
		strade.get(24).setBounds(158, 266, 16, 18);
	
		strade.get(25).setBounds(131, 293, 21, 18);
	
		strade.get(26).setBounds(100, 320, 16, 18);
	
		strade.get(27).setBounds(135, 351, 21, 18);
		
		strade.get(28).setBounds(162, 386, 16, 18);
		
		strade.get(29).setBounds(196, 345, 16, 18);
	
		strade.get(30).setBounds(255, 334, 21, 18);
	
		strade.get(31).setBounds(326, 282, 21, 18);
	
		strade.get(32).setBounds(314, 343, 16, 18);
	
		strade.get(33).setBounds(365, 320, 16, 18);
	
		strade.get(34).setBounds(280, 375, 16, 18);
	
		strade.get(35).setBounds(321, 419, 16, 18);
	
		strade.get(36).setBounds(258, 398, 19, 17);
		
		strade.get(37).setBounds(222, 425, 21, 18);
		
		strade.get(38).setBounds(253, 469, 21, 18);
		
		strade.get(39).setBounds(195, 406, 16, 18);
		
		strade.get(40).setBounds(161, 492, 16, 17);
		
		strade.get(41).setBounds(108, 443, 16, 18);
		
		//Creazione JPanel con GridBagLayout
		JPanel panelCarteTerreno = new JPanel();
		panelCarteTerreno.setFont(new Font("Comic Sans MS", Font.PLAIN, 11));
		panelCarteTerreno.setBackground(new Color(51, 153, 255));
		panelCarteTerreno.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "Carte Terreno", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelCarteTerreno.setBounds(448, 117, 436, 323);
		contentPane.add(panelCarteTerreno);
		panelCarteTerreno.setLayout(null);
		
		Bottone buttonForesta = new Bottone(terreno0, forestaDis, "foresta");
		buttonForesta.setBounds(10, 28, 129, 123);
		buttonForesta.setToolTipText("Foresta");
		panelCarteTerreno.add(buttonForesta);
		buttonForesta.getList().addObserver(this);
		carteTerreno[0] = buttonForesta;
		
		Bottone buttonMontagna = new Bottone(terreno3, montagnaDis, "montagna");
		buttonMontagna.setBounds(10, 174, 129, 123);
		buttonMontagna.setToolTipText("Montagna");
		panelCarteTerreno.add(buttonMontagna);
		buttonMontagna.getList().addObserver(this);
		carteTerreno[3] = buttonMontagna;
		
		Bottone buttonPalude = new Bottone(terreno5, paludeDis, "palude");
		buttonPalude.setBounds(297, 174, 129, 123);
		buttonPalude.setToolTipText("Palude");
		panelCarteTerreno.add(buttonPalude);
		buttonPalude.getList().addObserver(this);
		carteTerreno[5] = buttonPalude;
		
		Bottone buttonPianura = new Bottone(terreno1, pianuraDis, "pianura");
		buttonPianura.setBounds(154, 28, 129, 123);
		buttonPianura.setToolTipText("Pianura");
		panelCarteTerreno.add(buttonPianura);
		buttonPianura.getList().addObserver(this);
		carteTerreno[1] = buttonPianura;
		
		Bottone buttonDeserto = new Bottone(terreno4, desertoDis, "deserto");
		buttonDeserto.setBounds(154, 174, 129, 123);
		buttonDeserto.setToolTipText("Deserto");
		panelCarteTerreno.add(buttonDeserto);
		buttonDeserto.getList().addObserver(this);
		carteTerreno[4] = buttonDeserto;
		
		Bottone buttonCampo = new Bottone(terreno2, campoDis, "campo");
		buttonCampo.setBounds(297, 28, 129,123);
		buttonCampo.setToolTipText("Campo");
		buttonCampo.getList().addObserver(this);
		panelCarteTerreno.add(buttonCampo);
		carteTerreno[2] = buttonCampo;
		
		costoForesta = new JLabel("Costo : " + valoreForesta);
		costoForesta.setBounds(43, 150, 60, 22);
		panelCarteTerreno.add(costoForesta);
		costoForesta.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		
		costoPianura = new JLabel("Costo: " + valorePianura);
		costoPianura.setBounds(188, 150, 54, 22);
		panelCarteTerreno.add(costoPianura);
		costoPianura.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
	
		costoCampo = new JLabel("Costo: " + valoreCampo);
		costoCampo.setBounds(331, 150, 54, 22);
		panelCarteTerreno.add(costoCampo);
		costoCampo.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		
		costoMontagna = new JLabel("Costo: "+ valoreMontagna);
		costoMontagna.setBounds(49, 295, 54, 22);
		panelCarteTerreno.add(costoMontagna);
		costoMontagna.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		
		costoDeserto = new JLabel("Costo: " + valoreDeserto);
		costoDeserto.setBounds(188, 295, 54, 22);
		panelCarteTerreno.add(costoDeserto);
		costoDeserto.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		
		costoPalude = new JLabel("Costo: " + valorePalude);
		costoPalude.setBounds(331, 295, 54, 22);
		panelCarteTerreno.add(costoPalude);
		costoPalude.setFont(new Font("Comic Sans MS", Font.PLAIN, 13));
		
		JPanel panelMosse = new JPanel();
		panelMosse.setBackground(new Color(51, 153, 255));
		panelMosse.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0), 1, true), "Mosse", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelMosse.setBounds(491, 454, 370, 145);
		contentPane.add(panelMosse);
		panelMosse.setLayout(null);
		
		muoviPastore = new Bottone(iconPastore, pastoreDis, "0");
		muoviPastore.setBounds(10,16,114,109);
		muoviPastore.getList().addObserver(this);
		panelMosse.setLayout(null);
		panelMosse.add(muoviPastore);
		panelMosse.add(muoviPastore);
		
		muoviPecora = new Bottone(iconPecora, pecoraDis, "1");
		muoviPecora.setLocation(119, 16);
		muoviPecora.setSize(133, 109);
		muoviPecora.getList().addObserver(this);
		panelMosse.add(muoviPecora);
		panelMosse.add(muoviPecora);
		
		compraCarta = new Bottone(iconCarta, cartaDis, "2");
		compraCarta.setBounds(237, 18, 133, 107);
		compraCarta.getList().addObserver(this);
		panelMosse.add(compraCarta);
		panelMosse.add(compraCarta);
		
		JPanel panelInfoGiocatore = new JPanel();
		panelInfoGiocatore.setBorder(new TitledBorder(new LineBorder(new Color(0, 0, 0)), "Le tue info", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panelInfoGiocatore.setBackground(new Color(51, 153, 255));
		panelInfoGiocatore.setBounds(894, 6, 127, 593);
		contentPane.add(panelInfoGiocatore);
		panelInfoGiocatore.setLayout(null);
		
		lblNumcarte0 = new JTextField(numCarteTipo0);
		lblNumcarte0.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblNumcarte0.setForeground(new Color(255, 255, 255));
		lblNumcarte0.setBackground(new Color(0, 102, 51));
		lblNumcarte0.setBounds(84, 99, 31, 25);
		lblNumcarte0.setEditable(false);
		lblNumcarte0.setOpaque(false);
		panelInfoGiocatore.add(lblNumcarte0);
		
		lblNumcarte1 = new JTextField(numCarteTipo1);
		lblNumcarte1.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblNumcarte1.setForeground(new Color(255, 255, 255));
		lblNumcarte1.setBackground(new Color(0, 153, 51));
		lblNumcarte1.setBounds(84, 135, 31, 25);
		lblNumcarte1.setEditable(false);
		lblNumcarte1.setOpaque(false);
		panelInfoGiocatore.add(lblNumcarte1);
		
		lblNumcarte2 = new JTextField(numCarteTipo2);
		lblNumcarte2.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblNumcarte2.setForeground(new Color(255, 255, 255));
		lblNumcarte2.setBackground(new Color(204, 153, 0));
		lblNumcarte2.setBounds(84, 175, 31, 25);
		lblNumcarte2.setEditable(false);
		lblNumcarte2.setOpaque(false);
		panelInfoGiocatore.add(lblNumcarte2);
		
		lblNumcarte3 = new JTextField(numCarteTipo3);
		lblNumcarte3.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblNumcarte3.setForeground(new Color(255, 255, 255));
		lblNumcarte3.setBackground(new Color(102, 102, 102));
		lblNumcarte3.setBounds(84, 211, 31, 25);
		lblNumcarte3.setEditable(false);
		lblNumcarte3.setOpaque(false);
		panelInfoGiocatore.add(lblNumcarte3);
		
		lblNumcarte4 = new JTextField(numCarteTipo4);
		lblNumcarte4.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblNumcarte4.setForeground(new Color(255, 255, 255));
		lblNumcarte4.setBackground(new Color(204, 204, 153));
		lblNumcarte4.setBounds(84, 251, 31, 25);
		lblNumcarte4.setOpaque(false);
		lblNumcarte4.setEditable(false);
		panelInfoGiocatore.add(lblNumcarte4);
		
		lblNumcarte5 = new JTextField(numCarteTipo5);
		lblNumcarte5.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		lblNumcarte5.setBackground(new Color(102, 204, 255));
		lblNumcarte5.setForeground(new Color(255, 255, 255));
		lblNumcarte5.setBounds(84, 287, 31, 25);
		lblNumcarte5.setEditable(false);
		lblNumcarte5.setOpaque(false);
		panelInfoGiocatore.add(lblNumcarte5);
		
		JLabel carteGiocatore0 = new JLabel(terreno0a);
		carteGiocatore0.setToolTipText("Foresta");
		carteGiocatore0.setBounds(8, 82, 109, 42);
		panelInfoGiocatore.add(carteGiocatore0);
		
		JLabel carteGiocatore3 = new JLabel(terreno1);
		carteGiocatore3.setToolTipText("Pianura");
		carteGiocatore3.setBounds(8, 120, 109, 42);
		panelInfoGiocatore.add(carteGiocatore3);
		
		JLabel carteGiocatore4 = new JLabel(terreno2);
		carteGiocatore4.setToolTipText("Campo");
		carteGiocatore4.setBounds(8, 158, 109, 42);
		panelInfoGiocatore.add(carteGiocatore4);
		
		JLabel carteGiocatore1 = new JLabel(terreno3);
		carteGiocatore1.setToolTipText("Montagna");
		carteGiocatore1.setBounds(8, 195, 109, 42);
		panelInfoGiocatore.add(carteGiocatore1);
		
		JLabel carteGiocatore5 = new JLabel(terreno4);
		carteGiocatore5.setToolTipText("Deserto");
		carteGiocatore5.setBounds(8, 233, 109, 42);
		panelInfoGiocatore.add(carteGiocatore5);
		
		JLabel carteGiocatore2= new JLabel(terreno5);
		carteGiocatore2.setToolTipText("Palude");
		carteGiocatore2.setBounds(8, 270, 109, 42);
		panelInfoGiocatore.add(carteGiocatore2);
		
		JLabel denaroImmagine = new JLabel(soldi);
		denaroImmagine.setBackground(new Color(0, 153, 255));
		denaroImmagine.repaint();
		denaroImmagine.setBounds(0, 474, 128, 80);
		panelInfoGiocatore.add(denaroImmagine);
		denaroGiocatore = new JLabel();
		denaroGiocatore.setForeground(new Color(255, 255, 255));
		denaroGiocatore.setBackground(new Color(0, 153, 255));
		denaroGiocatore.setText(denaro);
		denaroGiocatore.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		denaroGiocatore.setBounds(8, 529, 31, 25);
		panelInfoGiocatore.add(denaroGiocatore);
		denaroGiocatore.repaint();
		
		JLabel nome = new JLabel(nomeGiocatore);
		nome.setBounds(6, 372, 61, 16);
		panelInfoGiocatore.add(nome);
		
		JLabel immagineRecinti = new JLabel(recinto);
		immagineRecinti.setBackground(new Color(0, 153, 255));
		immagineRecinti.setBounds(8, 327, 109, 109);
		immagineRecinti.setVisible(true);
		panelInfoGiocatore.add(immagineRecinti);
		
		recinti = new JLabel();
		recinti.setBackground(new Color(51, 153, 255));
		recinti.setForeground(new Color(255, 255, 255));
		recinti.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		recinti.setBounds(8, 424, 107, 25);
		recinti.setText(numeroRecinti);
		panelInfoGiocatore.add(recinti);
		recinti.repaint();
		
		giocatore = new JLabel();
		giocatore.setBounds(1, 29, 127, 42);
		panelInfoGiocatore.add(giocatore);
		giocatore.setVisible(false);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.getVerticalScrollBar().setPreferredSize(new Dimension(0,0));
		scrollPane.getHorizontalScrollBar().setPreferredSize(new Dimension(0,0));
		scrollPane.getViewport().setBorder(null);
		scrollPane.setBounds(448, 6, 436, 99);
		contentPane.add(scrollPane);
		
		textArea = new JTextArea();
		textArea.setEditable(false);
		DefaultCaret caret = (DefaultCaret)textArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		scrollPane.setViewportView(textArea);

		frame.setVisible(true);
		contentPane.setVisible(true);
	}
	
	@Override
	public void update(Observable elemGraf, Object mess) {
		
			String codice = mess.toString();
			codiceMossa = codice;
			
	}
}

