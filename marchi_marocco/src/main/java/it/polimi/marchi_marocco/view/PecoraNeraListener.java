package it.polimi.marchi_marocco.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

public class PecoraNeraListener extends Observable implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		
		//Passa l'id della regione della pecora nera e passa il flag true della pecora stessa
		PecoraNera pecora = (PecoraNera) e.getSource();
		int idRegionePassare = pecora.getIdRegione();
		String nera = "true";
		String messaggio = "1"+":"+idRegionePassare+":"+nera;
		setChanged();
		notifyObservers(messaggio);
	}
	
	

}
