package it.polimi.marchi_marocco.view;

import it.polimi.marchi_marocco.rete.ServerRMI;
import it.polimi.marchi_marocco.rete.ServerSS;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RMIoSocketServer extends JDialog {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static RMIoSocketServer dialog;
	private final JPanel contentPanel = new JPanel();
	protected int numGiocatori;

	

	/**
	 * Create the dialog.
	 */
	public RMIoSocketServer() {
		setTitle("Sheepland");
		setBounds(100, 100, 451, 141);
		setSize(new Dimension(450, 150));
		setResizable(false);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
			JLabel lblVuoiCheLa = new JLabel("Vuoi che la connessione sia gestita con RMI o tramite Socket?");
			lblVuoiCheLa.setBounds(31, 10, 387, 16);
			contentPanel.add(lblVuoiCheLa);
		
		
			JButton btnNewButton = new JButton("RMI");
			btnNewButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					
					dialog.dispose();
					try {
						ServerRMI server = new ServerRMI(numGiocatori);
						
					} catch (RemoteException e2) {
					
						Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore nella creazione del server", e2);
					}
					
				}
			});
			btnNewButton.setBounds(69, 53, 117, 29);
			contentPanel.add(btnNewButton);
		
		
			JButton btnSocket = new JButton("Socket");
			btnSocket.addActionListener(new ActionListener() {
				
				
				@Override
				public void actionPerformed(ActionEvent e) {
					
					dialog.dispose();
					ServerSS server = new ServerSS(numGiocatori);
					
					IniziaPartita.dialog.dispose();
					dialog.dispose();
				
				}
			});
			btnSocket.setBounds(252, 53, 117, 29);
			contentPanel.add(btnSocket);
		
	}

}
