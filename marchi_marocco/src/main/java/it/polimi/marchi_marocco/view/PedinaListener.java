package it.polimi.marchi_marocco.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

public class PedinaListener extends Observable implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		Pedina bottone = (Pedina) e.getSource();
		String valore = bottone.getValore();
		setChanged();
		notifyObservers(valore);

	}

}
