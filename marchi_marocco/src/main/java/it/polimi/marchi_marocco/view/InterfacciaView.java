package it.polimi.marchi_marocco.view;

import java.util.Observable;

public abstract class InterfacciaView extends Observable {
	
	public InterfacciaView() {
		
	}

	public void muoviPastore() {
	
	}

	public void muoviPecora() {
	
	}

	public void compraCarta() {
		
	}

	public void posizionaPastoreIniziale() {
		
	}

	public void displayInGioco() {
		
	}

	public void displayDenaro(String denaro) {
	
	}

	public void displayFineTurno() {
		
	}

	public void chiediQualePastore() {
	
	}

	public void displayCarteTerreno(String numero1, String numero2,
			String numero3, String numero4, String numero5, String numero6) {
	
	}

	public void displayMosseDisponibili(String[] valide) {
		
	}

	public void scegliMossa() {
	
	}

	public void displayCostoCarte(String string, String string2,
			String string3, String string4, String string5, String string6) {
	
	}

	public void displayRecinti(String numero) {
		
	}

	public void displayErrore() {
	
	}

	public void displayMovimentoPecoraNera(String idRegione) {
		
	}

	public void displayVincitori(String[] vincitori) {
		
	}

	public void displayPosizionamentoPastoreIniziale(String string,
			String string2) {
	}
	

	public void displayPastoreMosso(String idStradaArrivo,
			String idStradaPartenza, String idGicatore) {
		// TODO Auto-generated method stub
		
	}

	public void displayInfoIniziali(String nome, String denaro, String val1,
			String val2, String val3, String val4, String val5, String val6,
			String codici) {
		// TODO Auto-generated method stub
		
	}

	public void displayPecoraMossa(String idRegioneArrivo,
			String idRegionePartenza, String flagNera) {
		// TODO Auto-generated method stub
		
	}

	
}
