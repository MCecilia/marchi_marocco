package it.polimi.marchi_marocco.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ServerOClient extends JDialog {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static ServerOClient dialog;
	static NumeroGiocatori numGiocatori = null;
	static RMIoSocketClient rmiOSocketClient = null;
	private final JPanel contentPanel = new JPanel();
	
	public ServerOClient() {
		setTitle("Sheepland");
		setBounds(100, 100, 451, 141);
		setSize(new Dimension(450, 150));
		setResizable(false);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		final JButton btnNewButton = new JButton("Server");
		btnNewButton.setBounds(110, 56, 82, 29);
		btnNewButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {

				if (numGiocatori == null){
					numGiocatori = new NumeroGiocatori();
					NumeroGiocatori.dialog = numGiocatori;
					numGiocatori.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
				    numGiocatori.setVisible(true);
				    dialog.dispose();
				
				}else {
					numGiocatori.toFront();
					dialog.dispose();
					
				}
			}
			
		});
		contentPanel.setLayout(null);
		contentPanel.add(btnNewButton);
		
		JLabel lblVuoiGiocareCome = new JLabel("Vuoi giocare come Server o come Client?");
		lblVuoiGiocareCome.setBounds(98, 16, 254, 16);
		contentPanel.add(lblVuoiGiocareCome);
		
		final JButton btnNewButton1 = new JButton("Client");
		btnNewButton1.setBounds(258, 56, 81, 29);
		btnNewButton1.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				if (rmiOSocketClient == null){
					rmiOSocketClient = new RMIoSocketClient();
					RMIoSocketClient.dialog = rmiOSocketClient;
					rmiOSocketClient.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
					rmiOSocketClient.setVisible(true);
				    dialog.dispose();
				
				}else {
					rmiOSocketClient.toFront();
					dialog.dispose();
					
				}
				
			}
		});
		contentPanel.add(btnNewButton1);
		
	}

}
