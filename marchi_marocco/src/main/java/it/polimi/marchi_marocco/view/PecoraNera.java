package it.polimi.marchi_marocco.view;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.Timer;

public class PecoraNera extends JButton {
	
	private static final long serialVersionUID = 1L;
	private static final long DURATION_MUL = (long) 1E6;
	private static final int TIMER_DELAY = 10;
	
	private int idRegione;
	//per animazione
	private Point endPosition;
	private Point startPosition;
	private long startingTime;
	private long animationDuration;
	private boolean animating;
	PecoraNeraListener bottoneListener;
	
	public PecoraNera(ImageIcon icon, int idRegione) {
		
		super(icon);
		this.idRegione = idRegione;
		setBorderPainted(false);
		setContentAreaFilled(false);
		setOpaque(false);
		setEnabled(false);
		setSize(34,24);
		setDisabledIcon(icon);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bottoneListener = new PecoraNeraListener();
		this.addActionListener(bottoneListener); 
	
	}
	
	
	/**
	 * Metodo che muove l'oggetto date delle coordinate
	 * @param destination = Point point = new Point(int x, int y), rappresenta la destinazione finale
	 * @param timeMillisec = durata dell'animazione
	 */
	public void moveTo(Point destination, int timeMillisec) {
		startingTime = System.nanoTime();
		animationDuration = (long) (timeMillisec*DURATION_MUL);
		startPosition = getBounds().getLocation();
		this.endPosition = destination;
		animating = true;
		performAnimation();
	}


	/**
	 * Indica se un oggetto e' in fase di animazione oppure no	
	 * @return = true se l'oggetto si anima, false se l'oggetto e' fermo
	 */
	 
	public boolean isAnimating() {
		return animating;
	}
	
	private void performAnimation() {
		
		//We want to write a block of code to call
		//repeatedly by the timer each 10ms
		ActionListener animationTask = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent evt) {
				//get the current time in nanoseconds
				long now = System.nanoTime();
				//progress is a number between 0 and 1 represents the progress of the animation
				//according to the passed time from the start of the animation
				double progress =  now > (startingTime + animationDuration) ?
						1 : (double)(now - startingTime) / (double)animationDuration ;

				if(now < startingTime) {
					progress = 0;
				}

				//Compute the new position using a simple proportion 
				double newX = startPosition.x + (endPosition.x - startPosition.x)*progress;
				double newY = startPosition.y + (endPosition.y - startPosition.y)*progress;


				Point newPosition = new Point(((int)newX-5),((int)newY-5));

				//check whether the animation must end
				if(progress == 1) {
					((Timer)evt.getSource()).stop();
					animating = false;
				}

				//Set the current location
				PecoraNera.this.setLocation(newPosition);
			}
	
		};

		//Set up a timer that fire each 10ms, calling 
		//the method declare in the animationTask
		Timer timer = new Timer(TIMER_DELAY,animationTask);
		timer.start();
	}
	
	
	public int getIdRegione() {
		
		return this.idRegione;
	}
	
	public void setIdRegione(String id) {
		
		this.idRegione = Integer.parseInt(id);
	}
	
	public PecoraNeraListener getList() {
		
		return  bottoneListener;
	}

}
