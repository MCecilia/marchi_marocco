package it.polimi.marchi_marocco.view;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.Timer;


public class Pedina extends JButton {
	
	
	private static final long serialVersionUID = 1L;
	private static final long DURATION_MUL = (long) 1E6;
	private static final int TIMER_DELAY = 10;
	//individua in che strada e' posizionato il pastore
	private Strada posizione;
	//salva l'id del pastore che e' = a quello del giocatore
	private final int idGiocatore;
	private PedinaListener buttonListener;
	//Informazioni per il movimento del pastore
	private Point endPosition;
	private Point startPosition;
	private long startingTime;
	private long animationDuration;
	private boolean animating;
	
	
	ImageIcon icon1 = new ImageIcon(getClass().getResource("Blue1Icon.png"));
	ImageIcon icon2 = new ImageIcon(getClass().getResource("Green2Icon.png"));
	ImageIcon icon3 = new ImageIcon(getClass().getResource("Red3Icon.png"));
	ImageIcon icon4 = new ImageIcon(getClass().getResource("Yellow4Icon.png"));
	

	ImageIcon icon1a = new ImageIcon(getClass().getResource("Blue1Icon.png"));
	ImageIcon icon2a = new ImageIcon(getClass().getResource("Green1Icon.png"));
	ImageIcon icon3a = new ImageIcon(getClass().getResource("Red2Icon.png"));
	ImageIcon icon4a = new ImageIcon(getClass().getResource("Yellow2Icon.png"));
	
	//Costruttore di pedina
	public Pedina(String id, int indice, int numeroGiocatori){
	
		
		this.setSize(30, 30);
		
		int idGioc = Integer.parseInt(id);
	    this.idGiocatore = idGioc;
	    
	    if(numeroGiocatori == 2) {
	    	
	    	switch (indice) {
			
			case 0:
				this.setIcon(icon1a);
				break;
			case 1:
				this.setIcon(icon2a);
				break;
			case 2:
				this.setIcon(icon3a);
				break;	
			case 3:
				this.setIcon(icon4a);
				break;	
				
			default :
				break;
	    	}
	    	
	    } else {
	    
	    	switch (indice) {
		
	    	case 0:
	    		this.setIcon(icon1);
	    		break;
	    	case 1:
	    		this.setIcon(icon2);
	    		break;
	    	case 2:
	    		this.setIcon(icon3);
	    		break;	
	    	case 3:
	    		this.setIcon(icon4);
	    		break;
	    	default :
	    		break;
	    	
		}
		
	    }
		
	    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		setBorderPainted(false);
		setOpaque(false);
		setContentAreaFilled(false);
		this.setEnabled(true);
		this.setVisible(false);
		buttonListener = new PedinaListener();
		addActionListener(buttonListener);
	}// fine costruttore
	
	/**
	 * Metodo che muove l'oggetto date delle coordinate
	 * @param destination = Point point = new Point(int x, int y), rappresenta la destinazione finale
	 * @param timeMillisec = durata dell'animazione
	 */
	public void moveTo(Point destination, int timeMillisec) {
		startingTime = System.nanoTime();
		animationDuration = (long) (timeMillisec*DURATION_MUL);
		startPosition = getBounds().getLocation();
		this.endPosition = destination;
		animating = true;
		performAnimation();
	}


	/**
	 * Indica se un oggetto e' in fase di animazione oppure no	
	 * @return = true se l'oggetto si anima, false se l'oggetto e' fermo
	 */
	 
	public boolean isAnimating() {
		return animating;
	}
	
	private void performAnimation() {
		
		//We want to write a block of code to call
		//repeatedly by the timer each 10ms
		ActionListener animationTask = new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent evt) {
				//get the current time in nanoseconds
				long now = System.nanoTime();
				//progress is a number between 0 and 1 represents the progress of the animation
				//according to the passed time from the start of the animation
				double progress =  now > (startingTime + animationDuration) ?
						1 : (double)(now - startingTime) / (double)animationDuration ;

				if(now < startingTime) {
					progress = 0;
				}

				//Compute the new position using a simple proportion 
				double newX = startPosition.x + (endPosition.x - startPosition.x)*progress;
				double newY = startPosition.y + (endPosition.y - startPosition.y)*progress;
				
				Point newPosition = new Point(((int)newX-5),((int)newY-5));

				//check whether the animation must end
				if(progress == 1) {
					((Timer)evt.getSource()).stop();
					animating = false;
				}

				//Set the current location
				Pedina.this.setLocation(newPosition);
			}
	
		};

		//Set up a timer that fire each 10ms, calling 
		//the method declare in the animationTask
		Timer timer = new Timer(TIMER_DELAY,animationTask);
		timer.start();
	}
		
	//Setta per ogni pastore la nuova posizione in cui si trova
	public void setPosizione(Strada strada) {
		
		this.posizione = strada;
	}
	
	
	public Strada getPosizione() {
		
		return posizione;
	}
	
	public String getValore() {
		
		return Integer.toString(posizione.getId());
	}
	
	public int getIdGiocatore() {
		
		return idGiocatore;
	}
	
	public PedinaListener getList() {
		
		return  buttonListener;
	}

}
