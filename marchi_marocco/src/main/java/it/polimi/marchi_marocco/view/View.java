package it.polimi.marchi_marocco.view;

import java.awt.Point;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class View extends InterfacciaView {

	private Mappa mappa;
	private boolean aspettaInput;
	int idRegionePecoraNera = 0;
	int recintiTot = 20;
	boolean posizionatoPrimo = false;
	int numeroGiocatori;
	
	public View(){
	
		try {
			mappa = new Mappa();
		} catch (IOException e) {	
		
			Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore nella creazione della Schermata di gioco", e);
		}
		
	}
	
	

	/**
	 * Metodo che chiama la funzione che invia i parametri al controller per l'esecuzione della mossa scelta
	 */
	@Override
	

	public void scegliMossa() {
		
		mappa.codiceMossa = null;
		aspettaInput = true;
		
		while(aspettaInput) {
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Problema sleep", e);
			
		}
		
		if(mappa.codiceMossa!= null) {
		switch (mappa.codiceMossa) {
		
		case "0":
			aspettaInput = false;
			mappa.textArea.append("Hai scelto di muovere il pastore!\n");
			this.muoviPastore();
			break;
			
		case "1":
			aspettaInput = false;
			mappa.textArea.append("Hai scelto di muovere una pecora!\n");
			this.muoviPecora();
			break;
			
		case "2":
			aspettaInput = false;
			mappa.textArea.append("Hai scelto di comprare una carta!\n");
			this.compraCarta();
			break;
			
		default: break;
		}
		
		}
		}
	}

	/**
	 * Metodo che ascolta il bottone della strada premuto e notifica il client 
	 * della scelta fatta dal giocatore.
	 */
	@Override
	public void posizionaPastoreIniziale(){
		
		//Resetta tutti i valori ogni volta che viene chiamato il posizionaPastore
		mappa.codiceMossa= null;
		
		//Attivo tutte le strade
		for(Strada s : Mappa.strade) {
			
				s.setEnabled(true);
						
				}
		
		mappa.textArea.append("Scegli una strada (libera!) dove posizionare il tuo pastore.\n");
		aspettaInput = true;
		
		while(aspettaInput) {
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
			
				Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Problema sleep", e);			
				
			}
			
			if(mappa.codiceMossa!= null) {
			
				setChanged();
				notifyObservers(mappa.codiceMossa);
				aspettaInput = false;
			}
		}
	}
	
	

	
	/**
	 * Metodo che permette al giocatore di scegliere la carta terreno da acquistre. Aspetta input dal giocatore poi
	 * formatta la stringa da passare al controller  [codice:tipoTerreno]
	 */
	@Override
	public void compraCarta(){
		
		mappa.codiceMossa = null;
		aspettaInput = true;
		
		mappa.muoviPecora.setEnabled(false);
		mappa.muoviPastore.setEnabled(false);
		
		for (int i=0; i< 6; i++) {
			mappa.carteTerreno[i].setEnabled(true);
		}
		
		mappa.textArea.append("Scegli che tipo di carta comprare!\n");
		
		while(aspettaInput) {
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Problema sleep", e);	
			}
		
		if(mappa.codiceMossa != null) {
			
			for (int i=0; i<= 5; i++){
				mappa.carteTerreno[i].setEnabled(false);
			}
			
		String mess = "2"+":"+mappa.codiceMossa;
		aspettaInput = false;
		setChanged();
		notifyObservers(mess);
		
		}
	}
		
	}
	
	/**
	 * Metodo che recupera i parametri del muovi pastore e li invia al controller  sotto forma di stringa ben formattata
	 * [codice:idStradaDestinazione]
	 */
	@Override
	public void muoviPastore() {
		
		//Disattivo i bottoni delle altre mosse
		mappa.codiceMossa = null;
		mappa.muoviPecora.setEnabled(false);
		mappa.compraCarta.setEnabled(false);
		
		//Attivo tutte le strade
		for(Strada s : Mappa.strade) {
			
			s.setEnabled(true);
					
			}
		
		mappa.textArea.append("Scegli una strada (libera!) dove posizionare il tuo pastore.\n");
		aspettaInput = true;
		
		while(aspettaInput) {
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				
				Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Problema sleep", e);
			}
			
			if(mappa.codiceMossa != null) {
				
				for(Strada s : Mappa.strade) {
					
					s.setEnabled(false);
							
					}
				
				//Invio i parametri corretti al controller
				aspettaInput = false;
				mappa.codiceMossa = "0"+":"+mappa.codiceMossa;
				setChanged();
				notifyObservers(mappa.codiceMossa);
				
			}
		}
		
	}
	
	/**
	 * Metodo che recupera i parametri del muovi pecora e li passa al controller sotto forma di stringa ben formattata
	 * [codice:idRegionePartenza:FlagBoolean]
	 */
	@Override
	public void muoviPecora() {
			
			mappa.codiceMossa = null;
			aspettaInput = true;
			//Disabilito bottoni altre mosse
			mappa.muoviPastore.setEnabled(false);
			mappa.compraCarta.setEnabled(false);
			
			//Abilito tutte le pecore bianche
			for(int i = 0; i<mappa.pecoreBianche.length; i++) {
				
				mappa.pecoreBianche[i].setEnabled(true);
			}
			
			//Abilito le regione della pecora nera
			mappa.nera.setEnabled(true);
			
			mappa.textArea.append("Scegli quale pecora muovere, deve essere una regione adiacente al tuo pastore...\n");
			
			while(aspettaInput) {
				 
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
				
					Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Problema sleep", e);
				}
				
				if(mappa.codiceMossa!= null) {
					
					//Disabilito tutte le pecore bianche e la nera
					for(int i = 0; i<mappa.pecoreBianche.length; i++) {
					
						mappa.pecoreBianche[i].setEnabled(false);
				}
				
				mappa.nera.setEnabled(false);
				
				//Invio la stringa al server
				aspettaInput = false;
				setChanged();
				notifyObservers(mappa.codiceMossa);
				
			}
			
			}
			
		}
	
	@Override
	public void chiediQualePastore() {
		
		mappa.textArea.append("Scegli quale dei tuoi pastori utilizzare per questo turno.\n");
		
		mappa.codiceMossa = null;
		aspettaInput = true;
		
		while(aspettaInput) {
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				
				Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Problema sleep", e);
			}
			
			if(mappa.codiceMossa != null) {

				setChanged();
				notifyObservers(mappa.codiceMossa);
				aspettaInput = false;
			}
		}
	}
	

	
	
	@Override
	public void displayInfoIniziali(String nome, String denaro,
			String val1, String val2, String val3, String val4, String val5, String val6, String numero){
		
		mappa.idGiocatore = nome;
		int giocatore1 = (Integer.parseInt(nome)+1);
		mappa.nomeGiocatore = Integer.toString(giocatore1);
		mappa.denaro = denaro;
		
		mappa.textArea.append("Benvenuto in Sheepland Giocatore"+mappa.nomeGiocatore+"!\n");
		mappa.textArea.append("La partita conta "+numero+" giocatori\n");
		mappa.textArea.append("Il tuo denaro iniziale e': "+mappa.denaro+"\n");
		mappa.textArea.append("La tua carta inizale è stata assegnata!\n\n");
		
		mappa.numCarteTipo0 = val1;
		mappa.numCarteTipo1 = val2;
		mappa.numCarteTipo2 = val3;
		mappa.numCarteTipo3 = val4;
		mappa.numCarteTipo4 = val5;
		mappa.numCarteTipo5 = val6;
		
		mappa.lblNumcarte0.setText(mappa.numCarteTipo0);
		mappa.lblNumcarte1.setText(mappa.numCarteTipo1);
		mappa.lblNumcarte2.setText(mappa.numCarteTipo2);
		mappa.lblNumcarte3.setText(mappa.numCarteTipo3);
		mappa.lblNumcarte4.setText(mappa.numCarteTipo4);
		mappa.lblNumcarte5.setText(mappa.numCarteTipo5);
		
		mappa.denaroGiocatore.setText(denaro);
			
			switch(mappa.nomeGiocatore) {
			
			case "1":
			mappa.giocatore.setIcon(mappa.nomeGiocatore1);
			break;
			
			case "2":
			mappa.giocatore.setIcon(mappa.nomeGiocatore2);
			break;
			
			case "3":
			mappa.giocatore.setIcon(mappa.nomeGiocatore3);
			break;
			
			case "4":
			mappa.giocatore.setIcon(mappa.nomeGiocatore4);
			break;
			
			default : break;
			}
			
			mappa.giocatore.setVisible(true);
			mappa.giocatore.repaint();
		
		numeroGiocatori = Integer.parseInt(numero);
		
		//crea le pedine a seconda delle info arrivategli dal controller
		this.creaPedine(Integer.parseInt(numero));
		
}
	
	/**
	 * Metodo che visualizza effettivamente che un pastore e' stato posizionato sulla plancia di gioco e lo 
	 * visualizza per tutti i giocatori
	 */
	
	public void displayPosizionamentoPastoreIniziale(String stradaArrivo, String idGiocatore) {
		
		int id = Integer.parseInt(idGiocatore);
		int nome = id+1;
		Strada stradaFinale = null;
		mappa.textArea.append("Il pastore del giocatore"+nome+" e' stato posizionato su una strada.\n");
		
		//Giocatori == 2 ha un problema nello switch
		if(numeroGiocatori == 2) {
			
			if(!posizionatoPrimo) {
				
				switch(idGiocatore) {
				
				case "0":
					Pedina pedina0 = mappa.pastoriGiocatore1[0]; 
					for(Strada s: Mappa.strade) {
						if(s.getId() == Integer.parseInt(stradaArrivo)) {
							stradaFinale = s;
						}
					}
					pedina0.setLocation(stradaFinale.getX()-5,stradaFinale.getY()-5);
					pedina0.setPosizione(stradaFinale);
					//Lo posiziona sulla mappa e lo rende visibile
					Mappa.panel.add(pedina0, Integer.valueOf(5));
					pedina0.setVisible(true);
					stradaFinale.setEnabled(false);
					//Rimuovo dall'arrayList la strada su cui si e' posizionato il pastore
					//Il riferimento alla strada rimane salvato nell'oggetto Pedina.posizione
					Mappa.strade.remove(stradaFinale);
					posizionatoPrimo = true;
					break;
				
				case "1":
					Pedina pedina1 = mappa.pastoriGiocatore2[0]; 
					for(Strada s: Mappa.strade) {
						if(s.getId() == Integer.parseInt(stradaArrivo)) {
							stradaFinale = s;
						}
					}
					pedina1.setLocation(stradaFinale.getX()-5,stradaFinale.getY()-5);
					pedina1.setPosizione(stradaFinale);
					//Lo posiziona sulla mappa e lo rende visibile
					Mappa.panel.add(pedina1, Integer.valueOf(5));
					pedina1.setVisible(true);
					//Disabilitiamo la strada su cui e' posizionato il pastore
					stradaFinale.setEnabled(false);
					//Rimuovo dall'arrayList la strada su cui si e' posizionato il pastore
					//Il riferimento alla strada rimane salvato nell'oggetto Pedina.posizione
					Mappa.strade.remove(stradaFinale);
					posizionatoPrimo = true;
					break;
					
				default: break;	
				}
				
			} else {
			
			switch(idGiocatore) {
			case "0":
				Pedina pedina2 = mappa.pastoriGiocatore1[1]; 
				for(Strada s: Mappa.strade) {
					if(s.getId() == Integer.parseInt(stradaArrivo)) {
						stradaFinale = s;
					}
				}
				pedina2.setLocation(stradaFinale.getX()-5,stradaFinale.getY()-5);
				pedina2.setPosizione(stradaFinale);
				//Lo posiziona sulla mappa e lo rende visibile
				Mappa.panel.add(pedina2, Integer.valueOf(5));
				pedina2.setVisible(true);
				//Disabilitiamo la strada su cui e' posizionato il pastore
				stradaFinale.setEnabled(false);
				//Rimuovo dall'arrayList la strada su cui si e' posizionato il pastore
				//Il riferimento alla strada rimane salvato nell'oggetto Pedina.posizione
				Mappa.strade.remove(stradaFinale);
				posizionatoPrimo = false;
				break;
			
			case "1":
				Pedina pedina3 = mappa.pastoriGiocatore2[1]; 
				for(Strada s: Mappa.strade) {
					if(s.getId() == Integer.parseInt(stradaArrivo)) {
						stradaFinale = s;
					}
				}
				pedina3.setLocation(stradaFinale.getX()-5,stradaFinale.getY()-5);
				pedina3.setPosizione(stradaFinale);
				//Lo posiziona sulla mappa e lo rende visibile
				Mappa.panel.add(pedina3, Integer.valueOf(5));
				pedina3.setVisible(true);
				//Disabilitiamo la strada su cui e' posizionato il pastore
				stradaFinale.setEnabled(false);
				//Rimuovo dall'arrayList la strada su cui si e' posizionato il pastore
				//Il riferimento alla strada rimane salvato nell'oggetto Pedina.posizione
				Mappa.strade.remove(stradaFinale);
				posizionatoPrimo = false;
				break;
				
				
			default : break;	
				}
			}
		} else {
			
			//Recupero la pedina giusta nell'array
			Pedina pedina4 = mappa.pastori[Integer.parseInt(idGiocatore)];
			//Posiziona il pastore sulla strada giusta
			for(Strada s: Mappa.strade) {
				if(s.getId() == Integer.parseInt(stradaArrivo)) {
					stradaFinale = s;
				}
			}
			pedina4.setLocation(stradaFinale.getX()-5,stradaFinale.getY()-5);
			pedina4.setPosizione(stradaFinale);
			//Lo posiziona sulla mappa e lo rende visibile
			Mappa.panel.add(pedina4, Integer.valueOf(5));
			pedina4.setVisible(true);
			//Disabilitiamo la strada su cui e' posizionato il pastore
			stradaFinale.setEnabled(false);
			//Rimuovo dall'arrayList la strada su cui si e' posizionato il pastore
			//Il riferimento alla strada rimane salvato nell'oggetto Pedina.posizione
			Mappa.strade.remove(stradaFinale);
			
		}
	}	
	
	/**
	 * Metodo che una volta mosso un pastore, visualizza il movimento sulla view di ciascun giocatore
	 */
	@Override
	public void displayPastoreMosso(String idStradaArrivo, String idStradaPartenza, String idGiocatore) {
		
		mappa.textArea.append("Un pastore e' stato posizionato! \n");
		recintiTot--;
		Pedina pedina = null;
		
		if(numeroGiocatori == 2) {
			
			int id = Integer.parseInt(idGiocatore);
			
			switch(id) {
			
			case 0:
				for(int i = 0; i<numeroGiocatori; i++) {
					
					if(mappa.pastoriGiocatore1[i].getPosizione().getId() == Integer.parseInt(idStradaPartenza)) {
						
						pedina = mappa.pastoriGiocatore1[i];
				
					}
						
				}
				break;
				
			case 1:
				for(int i = 0; i<numeroGiocatori; i++) {
					
					if(mappa.pastoriGiocatore2[i].getPosizione().getId() == Integer.parseInt(idStradaPartenza)) {
						pedina = mappa.pastoriGiocatore2[i];
					}
						
				}
			
				break;
			
			default: break;
			}//FineSwitch	
			
			
		} else {
			
			for(int i = 0; i<numeroGiocatori; i++) {
				
				if(mappa.pastori[i].getPosizione().getId() == Integer.parseInt(idStradaPartenza)) {
					pedina = mappa.pastori[i];

				}
				
			}
		}
		
		//Qua sposto il pastore effettivamente
		//Tolgo le strada iniziale che tolgo dall'arrayList, e' la strada salvata nel pastore selezionato
		Strada stradaDaTogliere = pedina.getPosizione();
		
				//Rimuovo la strada dall'array e ci posiziono sopra un recinto (finale o normale a seconda)
				if(recintiTot < 0) {
				RecintoFinale recinto = new RecintoFinale();
				recinto.setLocation(stradaDaTogliere.getX()-1, stradaDaTogliere.getY()-2);
				recinto.setVisible(true);
				Mappa.panel.add(recinto, Integer.valueOf(5));
				
				} else {
					
					Recinto recinto = new Recinto();
					recinto.setLocation(stradaDaTogliere.getX()-1, stradaDaTogliere.getY()-2);
					recinto.setVisible(true);
					Mappa.panel.add(recinto, Integer.valueOf(4));
				}
				
				//Rimuovo la strada dall'array
				Mappa.strade.remove(stradaDaTogliere);
				
			
			
				//Faccio comparire il pastore nella nuova strada di destinazione
				for(int i = 0; i<Mappa.strade.size(); i++) {
					
					if(Mappa.strade.get(i).getId() == Integer.parseInt(idStradaArrivo)) {
						
						Strada stradaPartenza = Mappa.strade.get(i);
						Point point = new Point(stradaPartenza.getX(), stradaPartenza.getY());
						pedina.moveTo(point, 750); //Qui NULLPOINTEREXCEPTION porca....
						while(pedina.isAnimating()) {
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								
								Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Problema sleep", e);
							}
						}
						pedina.setPosizione(stradaPartenza);
						pedina.setVisible(true);
						break;
				}
			}
				
	
	}
				
	@Override
	public void displayMovimentoPecoraNera(String idRegioneArrivo) {
		
		
		Point destinazione = mappa.pecoreNere[Integer.parseInt(idRegioneArrivo)];
		mappa.nera.setIdRegione(idRegioneArrivo);
		
		if(Integer.parseInt(idRegioneArrivo) != idRegionePecoraNera) {
			
			mappa.nera.moveTo(destinazione, 120);
			while(mappa.nera.isAnimating()) {
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Problema sleep", e);
				}
			}
		
			mappa.textArea.append("La pecora nera si e' mossa!\n");
		} else {
			
			mappa.textArea.append("La pecora nera e' rimasta ferma.\n");
		}
		
		idRegionePecoraNera = Integer.parseInt(idRegioneArrivo);
		
		
	}
	
	@Override
	public void displayPecoraMossa(String idRegioneArrivo, String idRegionePartenza, String flagNera) {
		
		//Se devo muovere una bianca...
		if(flagNera.equals("false")) {
			
			mappa.textArea.append("Una pecora bianca si e' mossa!\n");
			
			int numeroPartenza = Integer.parseInt(mappa.pecoreBianche[Integer.parseInt(idRegionePartenza)].getText());
			numeroPartenza--;
		
			mappa.pecoreBianche[Integer.parseInt(idRegionePartenza)].setText(Integer.toString(numeroPartenza));
			int numeroArrivo = Integer.parseInt(mappa.pecoreBianche[Integer.parseInt(idRegioneArrivo)].getText());
			numeroArrivo++;
		
			mappa.pecoreBianche[Integer.parseInt(idRegioneArrivo)].setText(Integer.toString(numeroArrivo));
			mappa.pecoreBianche[Integer.parseInt(idRegionePartenza)].repaint();
			mappa.pecoreBianche[Integer.parseInt(idRegioneArrivo)].repaint();
		//Se devo muovere una nera....
		} else {
			
			displayMovimentoPecoraNera(idRegioneArrivo);
		}
		
	}
	
	@Override
	public void displayCarteTerreno(String val1, String val2, String  val3,
			String  val4, String  val5,String  val6 ) {
		
		mappa.numCarteTipo0 = val1;
		mappa.numCarteTipo1 = val2;
		mappa.numCarteTipo2 = val3;
		mappa.numCarteTipo3 = val4;
		mappa.numCarteTipo4 = val5;
		mappa.numCarteTipo5 = val6;
		
		mappa.lblNumcarte0.setText(mappa.numCarteTipo0);
		mappa.lblNumcarte1.setText(mappa.numCarteTipo1);
		mappa.lblNumcarte2.setText(mappa.numCarteTipo2);
		mappa.lblNumcarte3.setText(mappa.numCarteTipo3);
		mappa.lblNumcarte4.setText(mappa.numCarteTipo4);
		mappa.lblNumcarte5.setText(mappa.numCarteTipo5);
	}
	
	@Override
	public void displayCostoCarte(String string1, String string2, String string3, 
			String string4, String string5, String string6) {
		
		
		mappa.valoreForesta = string1;
		mappa.valorePianura = string2;
		mappa.valoreCampo =  string3;
		mappa.valoreMontagna = string4;
		mappa.valoreDeserto = string5;
		mappa.valorePalude = string6;
					
		if("5".equals(mappa.valoreForesta)) {
			
			mappa.costoForesta.setText("esaurita!");
			
		} else {
			
			mappa.costoForesta.setText("Costo: "+mappa.valoreForesta);
		}
		
		if("5".equals(mappa.valorePianura)) {
			
			mappa.costoPianura.setText("esaurita!");
			 
		} else {
			
			mappa.costoPianura.setText("Costo: "+mappa.valorePianura);
		}
		
		if("5".equals(mappa.valoreCampo)) {
			
			mappa.costoCampo.setText("esaurita!");
			 
		} else {
			
			mappa.costoCampo.setText("Costo: "+mappa.valoreCampo);
		}
		
		if("5".equals(mappa.valoreMontagna)) {
			
			mappa.costoMontagna.setText("esaurita!");
			 
		} else {
			
			mappa.costoMontagna.setText("Costo: "+mappa.valoreMontagna);
		}
		
		if("5".equals(mappa.valoreDeserto)) {
			
			mappa.costoDeserto.setText("esaurita!");
			 
		} else {
			
			mappa.costoDeserto.setText("Costo: "+mappa.valoreDeserto);
		}
		
		if("5".equals(mappa.valorePalude)) {
			
			mappa.costoPalude.setText("esaurita!");
			 
		} else {
			
			mappa.costoPalude.setText("Costo: "+mappa.valorePalude);
		}
		
		mappa.costoForesta.repaint();
		mappa.costoMontagna.repaint();
		mappa.costoPianura.repaint();
		mappa.costoPalude.repaint();
		mappa.costoDeserto.repaint();
		mappa.costoCampo.repaint();
	}
	
	
	@Override
	public void displayMosseDisponibili(String[] valide) {
		
		mappa.textArea.append("Scegli una mossa!\n");
		
		boolean[] flag = new boolean[3];
		for(int i = 0; i < 3; i++) {
			flag[i] = Boolean.parseBoolean(valide[i]);
		}
		
		mappa.muoviPastore.setEnabled(flag[0]);
		mappa.muoviPecora.setEnabled(flag[1]);
		mappa.compraCarta.setEnabled(flag[2]);
		
	}
	
	@Override
	public void displayInGioco() {
		
		mappa.textArea.append("\nE' il tuo turno!\n");
		
	}
	
	@Override
	public void displayFineTurno() {
		
		mappa.textArea.append("\nIl tuo turno si e' concluso!\n"
				+ "Aspetta che gli altri giocatori abbiano terminato il loro.\n");
		mappa.muoviPastore.setEnabled(false);
		mappa.muoviPecora.setEnabled(false);
		mappa.compraCarta.setEnabled(false);
	}
	
	/**
	 * Metodo che aggiorna la visualizzazione dei recinti
	 */
	@Override
	public void displayRecinti(String numeroRecinti) {
		
		if(recintiTot <= 0) {
			
			mappa.recinti.setText("Fase finale!");
			mappa.recinti.repaint();
		} else {
			mappa.recinti.setText(numeroRecinti);
			mappa.recinti.repaint();
		}
	}
	
	@Override
	public void displayDenaro(String denaroG) {
		
		int valore = Integer.parseInt(denaroG);
		if(valore<=0) {
			mappa.denaroGiocatore.setText("0");
			mappa.denaroGiocatore.repaint();
		} else {
			mappa.denaroGiocatore.setText(denaroG);
			mappa.denaroGiocatore.repaint();
		}
	}
	
	/**
	  * Metodo che crea le pedine dei giocatori una volta che e' noto il numero dei giocatori
	  * @param numeroGiocatori = numero Giocatori partecipanti alla partita
	  */
	public void creaPedine(int numeroGiocatori) {
		
		//Se sono 2 gli id di due pastori saranno uguali ma i colori devono essere tutti differenti
		if(numeroGiocatori == 2) {
			mappa.pastoriGiocatore1[0] = new Pedina("0", 0, numeroGiocatori);
			mappa.pastoriGiocatore1[0].getList().addObserver(mappa);
			mappa.pastoriGiocatore1[1] = new Pedina("0", 1, numeroGiocatori);
			mappa.pastoriGiocatore1[1].getList().addObserver(mappa);
			mappa.pastoriGiocatore2[0] = new Pedina("1", 2, numeroGiocatori);
			mappa.pastoriGiocatore2[0].getList().addObserver(mappa);
			mappa.pastoriGiocatore2[1] = new Pedina("1", 3, numeroGiocatori);
			mappa.pastoriGiocatore2[1].getList().addObserver(mappa);
			
			} else {
			
				//Se sono 4 o 3 pedine tutte diverse.
				for(int i = 0; i<numeroGiocatori; i++) {
					mappa.pastori[i] = new Pedina(Integer.toString(i), i, numeroGiocatori);
					mappa.pastori[i].getList().addObserver(mappa);
				}	
		}
	}
	
	/**
	 * Metodo che informa l'utente se c'e' stato un errore nell'esecuzione della mossa
	 */
	@Override
	public void displayErrore() {
		
		mappa.textArea.append("\nLa mossa non e' valida! Riprova.\n");
		
	}
	
	@Override 
	public void displayVincitori(String[] vincitori) {
		
		String vinc = null;
		
			if(vincitori.length == 1) {
				mappa.textArea.append("\nIl vincitore e': "+vincitori[0]);
			} else {
			for(int i = 0; i<vincitori.length-1; i++) {
				
				vinc = vincitori[i]+"e";
			}
			String finale = vinc+vincitori[vincitori.length-1];
			mappa.textArea.append("\nI vincitori sono: "+finale);
			mappa.textArea.append("La partita e' finita! Grazie di aver partecipato!");
		}
	}



	
}
