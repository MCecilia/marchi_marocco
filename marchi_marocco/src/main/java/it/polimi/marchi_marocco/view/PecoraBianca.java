package it.polimi.marchi_marocco.view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import javax.swing.SwingConstants;

/**
 * Pecore bianche ordinate nell'array secondo l'id della regione
 */
public class PecoraBianca extends JButton {
	
	private static final long serialVersionUID = 1L;
	private int idRegione;
	private PecoraBiancaListener bottoneListener;
	
	public PecoraBianca (ImageIcon icon, int id) {
		
		
		super("1", icon);
		repaint();
		this.idRegione = id;
		Insets m = new Insets(0, 0, 0, 0);
		this.setMargin(m);
		this.setBorder(null);
		setVerticalTextPosition(SwingConstants.CENTER);
		setHorizontalTextPosition(SwingConstants.CENTER);
		setFont(new Font("Segoe Print", Font.BOLD, 16));
		setForeground(Color.red);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setDisabledIcon(icon);
		setOpaque(false);
		setEnabled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		bottoneListener = new PecoraBiancaListener();
		addActionListener(bottoneListener); 
		/**addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					
					//Passa l'iddella Regione in cui si trova la pecora
					PecoraBianca pecora = (PecoraBianca) e.getSource();
					int idRegionePassare = pecora.getIdRegione();
					Mappa.setRegioneScelta(Integer.toString(idRegionePassare));
					System.out.println("idRegione da passare : "+idRegionePassare);
					
			}
		});**/
		
		
	}
	
	public int getIdRegione() {
		
		return this.idRegione;
	}
	
	public PecoraBiancaListener getList() {
		
		return  bottoneListener;
	}
	
}
