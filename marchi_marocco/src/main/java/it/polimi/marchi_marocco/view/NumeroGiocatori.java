package it.polimi.marchi_marocco.view;



import javax.swing.JDialog;
import javax.swing.JLabel;
import java.awt.Dimension;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NumeroGiocatori extends JDialog {
	
	private static final long serialVersionUID = 1L;
	static RMIoSocketServer rmiOSocketServer = null;
	static NumeroGiocatori dialog;
	private final JPanel contentPanel = new JPanel();
	
	/**
	 * Create the dialog.
	 */
	public NumeroGiocatori() {
		setTitle("Sheepland");
		setBounds(100, 100, 451, 141);
		setSize(new Dimension(449, 219));
		setResizable(false);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(null);
		contentPanel.setBounds(0, 0, 0, 0);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		contentPanel.setLayout(null);
		
		JLabel lblScegliIlNumero = new JLabel("Scegli il numero dei giocatori per la partita:");
		lblScegliIlNumero.setBounds(85, 0, 279, 63);
		getContentPane().setLayout(null);
		getContentPane().add(lblScegliIlNumero);
		
		JButton btnNewButton = new JButton("2 giocatori");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (rmiOSocketServer == null){
					rmiOSocketServer = new RMIoSocketServer();
					rmiOSocketServer.numGiocatori = 2;
					RMIoSocketServer.dialog = rmiOSocketServer;
					rmiOSocketServer.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
					rmiOSocketServer.setVisible(true);					
					dialog.dispose();
					
				}else {
					rmiOSocketServer.toFront();
						dialog.dispose();
						
				}
			}
		});
		btnNewButton.setBounds(148, 57, 117, 29);
		getContentPane().add(btnNewButton);
		
		JButton btnNewButton1 = new JButton("3 giocatori");
		btnNewButton1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (rmiOSocketServer == null){
					rmiOSocketServer = new RMIoSocketServer();
					rmiOSocketServer.numGiocatori = 3;
					RMIoSocketServer.dialog = rmiOSocketServer;
					rmiOSocketServer.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
					rmiOSocketServer.setVisible(true);					
					dialog.dispose();
					
				}else {
					rmiOSocketServer.toFront();
						dialog.dispose();
						
				}
			}
		});
		btnNewButton1.setBounds(148, 92, 117, 29);
		getContentPane().add(btnNewButton1);
		
		JButton btnNewButton2 = new JButton("4 giocatori");
		btnNewButton2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				if (rmiOSocketServer == null){
					rmiOSocketServer = new RMIoSocketServer();
					RMIoSocketServer.dialog = rmiOSocketServer;
					rmiOSocketServer.numGiocatori = 4;
					rmiOSocketServer.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
					rmiOSocketServer.setVisible(true);					
					dialog.dispose();
					
				}else {
					rmiOSocketServer.toFront();
						dialog.dispose();
						
				}
			}
		});
		btnNewButton2.setBounds(148, 129, 117, 29);
		getContentPane().add(btnNewButton2);

	}
}
