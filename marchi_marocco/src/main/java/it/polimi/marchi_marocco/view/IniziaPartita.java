package it.polimi.marchi_marocco.view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IniziaPartita extends JDialog {
	
	private static final long serialVersionUID = 1L;
	static IniziaPartita dialog;
	static ServerOClient dialogServerOClient;
	
	private final JPanel contentPanel = new JPanel();

	/**
	 * Create the dialog.
	 * @throws Exception 
	 */
	public IniziaPartita() {
		
		setTitle("Sheepland");
		setBounds(100, 100, 538, 502);
		setResizable(false);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		ImageIcon icon = new ImageIcon(getClass().getResource("SheeplandApertura1.png"));
		JLabel lblNewLabel = new JLabel(icon);
		lblNewLabel.setBounds(6, 6, 531, 405);
		
		contentPanel.add(lblNewLabel);
		{
			JLabel lblIniziareLaPartita = new JLabel("Iniziare la partita?");
			lblIniziareLaPartita.setBounds(28, 423, 126, 35);
			contentPanel.add(lblIniziareLaPartita);
		}
		
		JButton btnNewButton = new JButton("Ok!");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (dialogServerOClient == null){
					dialogServerOClient = new ServerOClient();
					dialogServerOClient.dialog = dialogServerOClient;
					dialogServerOClient.setSize(new Dimension(450, 150));
					dialogServerOClient.setResizable(false);
					
					dialogServerOClient.setVisible(true);
				}else{
					dialogServerOClient.toFront();
				}
				
				
				
				
			}
		});
		btnNewButton.setBounds(305, 427, 117, 29);
		contentPanel.add(btnNewButton);
		
	setVisible(true);	
	
	}
	
	
}
