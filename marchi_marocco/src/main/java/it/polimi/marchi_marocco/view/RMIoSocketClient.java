package it.polimi.marchi_marocco.view;

import it.polimi.marchi_marocco.rete.ClientRMI;
import it.polimi.marchi_marocco.rete.ClientSS;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

public class RMIoSocketClient extends JDialog {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static RMIoSocketClient dialog;
	private final JPanel contentPanel = new JPanel();
	
	

	/**
	 * Create the dialog.
	 */
	public RMIoSocketClient() {
		setTitle("Sheepland");
		setBounds(100, 100, 451, 141);
		setSize(new Dimension(450, 150));
		setResizable(false);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
			JLabel lblVuoiCheLa = new JLabel("Vuoi che la connessione sia gestita con RMI o tramite Socket?");
			lblVuoiCheLa.setBounds(31, 10, 387, 16);
			contentPanel.add(lblVuoiCheLa);
		
		
			JButton btnNewButton = new JButton("RMI");
			
			btnNewButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					
					String name = null;
					while (name==null|| name.length() < 7 || name.length() > 15){
						name = JOptionPane.showInputDialog("Inserisci l'indirizzo IP relativo alla partita a cui vuoi collegarti:");
						if(name == null) {
						System.exit(0);
						}
					}
					dialog.dispose();	
					
					View view = null;
					view = new View();
					
					ClientRMI client = new ClientRMI(name, view);
					Thread thread = new Thread(client);
					thread.start();
				}
				
			});
			btnNewButton.setBounds(69, 53, 117, 29);
			contentPanel.add(btnNewButton);
		
		
			JButton btnSocket = new JButton("Socket");
			
			btnSocket.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
						
					String name = null;
					while (name==null|| name.length() < 7 || name.length() > 15){
						name = JOptionPane.showInputDialog("Inserisci l'indirizzo IP relativo alla partita a cui vuoi collegarti:");
						if(name == null) {
						System.exit(0);
						}
					}
					dialog.dispose();	
					
					View view = null;
					view = new View();
				
					ClientSS client = new ClientSS(name, view);
					Thread thread = new Thread(client);
					thread.start();
				}
			});
			btnSocket.setBounds(252, 53, 117, 29);
			contentPanel.add(btnSocket);
		
	}

}