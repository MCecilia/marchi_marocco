package it.polimi.marchi_marocco.view;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Recinto extends JLabel {
	
	private static final long serialVersionUID = 1L;
	ImageIcon icon1 = new ImageIcon(getClass().getResource("recinto1.png"));
	
	public Recinto() {
		
		super();
		setIcon(icon1);
		setSize(21, 21);
		setOpaque(false);
		setVisible(false);
	
	}

}
