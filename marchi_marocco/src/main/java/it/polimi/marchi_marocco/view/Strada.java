package it.polimi.marchi_marocco.view;

import java.awt.Cursor;

import javax.swing.JButton;

public class Strada extends JButton {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final int idStrada;
	private StradaListener buttonListener;
	
	public Strada(int i){
		super();
		
		idStrada = i;
		setBorderPainted(false);
		setOpaque(false);
		setContentAreaFilled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		buttonListener = new StradaListener();
		addActionListener(buttonListener);
	
	}
	
	//Ritorna l'id del bottone strada
	public int getId() {
		
		return this.idStrada;
	}
	
	public String getValore() {
		
		return Integer.toString(idStrada);
	}
	
	public StradaListener getList() {
		
		return buttonListener;
	}

}
