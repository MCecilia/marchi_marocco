package it.polimi.marchi_marocco.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

public class PecoraBiancaListener  extends Observable implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		//Passa l'iddella Regione in cui si trova la pecora
		PecoraBianca pecora = (PecoraBianca) e.getSource();
		int idRegionePassare = pecora.getIdRegione();
		String messaggio = "1"+":"+idRegionePassare+":"+"false";
		setChanged();
		notifyObservers(messaggio);
	}

	
}


