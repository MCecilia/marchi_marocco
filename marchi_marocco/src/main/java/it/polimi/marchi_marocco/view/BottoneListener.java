package it.polimi.marchi_marocco.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

public class BottoneListener extends Observable implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		Bottone bottone = (Bottone) e.getSource();
		bottone.setEnabled(false);
		String valore = bottone.getValore();
		setChanged();
		notifyObservers(valore);

	}
}
