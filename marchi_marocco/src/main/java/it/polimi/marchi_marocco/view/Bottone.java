package it.polimi.marchi_marocco.view;

import java.awt.Cursor;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Bottone extends JButton {
	
	private static final long serialVersionUID = 1L;
	private String valore;
	private BottoneListener bottoneListener;
	
	public Bottone (ImageIcon icon1, ImageIcon icon2, String valore) {
		
		super();
		this.valore = valore;
		setIcon(icon1);
		setEnabled(false);
		setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		setDisabledIcon(icon2);
		setOpaque(false);
		setContentAreaFilled(false);
		setBorderPainted(false);
		bottoneListener = new BottoneListener();
		addActionListener(bottoneListener);
	}
	
	public String getValore() {
		
		return this.valore;
	}
	
	public BottoneListener getList() {
		
		return bottoneListener;
	}

}
