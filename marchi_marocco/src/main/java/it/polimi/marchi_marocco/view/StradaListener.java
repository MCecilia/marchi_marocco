package it.polimi.marchi_marocco.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;

public class StradaListener extends Observable implements ActionListener {

	@Override
	public void actionPerformed(ActionEvent e) {
	
		Strada bottone = (Strada) e.getSource();
		String valore = bottone.getValore();
		setChanged();
		notifyObservers(valore);
	}

}
