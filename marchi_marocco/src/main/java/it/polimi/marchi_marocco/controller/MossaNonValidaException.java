package it.polimi.marchi_marocco.controller;

public class MossaNonValidaException extends Exception{
	
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

/**
 * Eccezione che viene lanciata una volta che il turno fallisce
 */
	MossaNonValidaException() {
		
		super("Attenzione! Mossa non valida! Riprova.");
	}
	

}
