package it.polimi.marchi_marocco.controller;

/**
 * Classe enum che mappa tutti i codici che vengono utilizzati in rete come protocollo per la comunicazione
 *
 */
public enum ProtocolloDiRete {


	MUOVI_PASTORE(0), MUOVI_PECORA(1), COMPRA_CARTA(2), POSIZIONA_PASTORE(11), SCEGLI_PASTORE(21),NOTIFICA_MOSSE(14),
	IN_GIOCO(40), FINE_TURNO(50), INFO_INIZIALI(77), NUMERO_RECINTI(7),
	SCEGLI_MOSSA(15), MUOVI_NERA(16), NOTIFICA_VINCITORI(1000),
	NOTIFICA_MUOVI_PASTORE(17),NOTIFICA_MUOVI_PECORA(18), NOTIFICA_COMPRA_CARTA(19), NOTIFICA_MUOVI_PASTORE_INIZIALE(85),
	NOTIFICA_CARTE_RIMANENTI(22), NOTIFICA_DENARO(33), ERRORE(666);
	private int codice;
	
	private ProtocolloDiRete(int codice) {
		this.codice = codice;
	}
	
	public int getCodice() {
		return this.codice;
	}

}
