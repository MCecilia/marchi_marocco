package it.polimi.marchi_marocco.controller;

import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Test;
import it.polimi.marchi_marocco.model.*;


/**
 * Classe che si occupa della gestione del turno, della modifica del model e
 * della verifica delle condizioni neccessarie perche' le mosse si verifichino
 */


public class GestorePartita {

	//Classe che gestisce solo il turno giocatore
	//Oggetto con riferimento allo stato
	//Oggetto con riferimento al gestore partita
	private StatoPartita stato;
	private int numeroGiocatori;
	private GestoreComunicazione comunicazione;
	MapTipoRegione m = new MapTipoRegione();
	

	public GestorePartita(StatoPartita s, GestoreComunicazione c, int numeroGiocatori) {

		//Classe che istanzia il controlle e crea la partita
		//
		stato = s;
		comunicazione = c;
		this.numeroGiocatori = numeroGiocatori; 
		
	}


	/**
	 * Funzione che istanzia un turno per i giocatori
	 * viene eseguito un ciclo, affinche ci siano turni disponibili sino alla fine dei
	 * recinti (e poi un ultimo alla fine di tutti)
	 * nello stato partita viene settato il giocatore attualmente in gioco
	 */
	public void runPartita() {
		
		for(int i = 0; i < numeroGiocatori; i++) {
			
			Giocatore g = stato.getGiocatore(i);
			
			//Inizia la fase di notifica della creazione della partita
			//recupero il denaro di ciascun giocatore, la carta iniziale assegnatagli
			//crea lo stream e glielo invia al server
			String codice = Integer.toString(ProtocolloDiRete.INFO_INIZIALI.getCodice());
			int[] carte1 = g.getCarteTerreno();
			String denaro = Integer.toString(g.getDenaro());
			String stream = codice+":"+denaro+":"+i+":"+carte1[0]+":"+carte1[1]+":"+carte1[2]+":"+carte1[3]+":"+carte1[4]+":"+carte1[5]+":"+numeroGiocatori;
			comunicazione.notificaGiocatore(i, stream);
			
			
		}
			
			this.inizioPartita();
	}
				
		
	private void inizioPartita() {
		
	
		//Chiedo a ciascun giocatore di posizionare il pastore iniziale
		//Preparo la stringa da inviare al metodo che mi recupera i parametri
		for(int j = 0; j < numeroGiocatori; j++) {	
			
			int codice5 = ProtocolloDiRete.POSIZIONA_PASTORE.getCodice();
			String stream5 = codice5+":"+j;
			boolean mossaValida = false;
			String messaggio = null;
			
			while(!mossaValida) {
			messaggio = comunicazione.ottieniParametri(stream5);
			if((Mappa.controllaValiditaPosizionePastore(messaggio))) {
				mossaValida = true;
			}
			}
			
			Strada strada = stato.getMappa().getStrada(Integer.parseInt(messaggio));
			stato.getGiocatore(j).setPosizione(strada);
			strada.setFlagGiocatore(true);
			//Notifico a tutti giocatori dove il giocatore in gioco ha posizionato il suo primo pastore iniziale
			int cod = ProtocolloDiRete.NOTIFICA_MUOVI_PASTORE_INIZIALE.getCodice();
			int idGiocatore = stato.getGiocatore(j).getIdGiocatore();
			String mss = cod+":"+messaggio+":"+idGiocatore;
			comunicazione.notificaTutti(mss);
			
			//Se sono 2 giocatori devo chiedere di posizionare un altro pastore sulla plancia di gioco
			if(numeroGiocatori == 2) {
				
				int codice6 = ProtocolloDiRete.POSIZIONA_PASTORE.getCodice();
				String stream6 = codice6+":"+j;
				
				boolean mossaValida2 = false;
				
				while(!mossaValida2) {
				messaggio = comunicazione.ottieniParametri(stream6);
				if((Mappa.controllaValiditaPosizionePastore(messaggio))) {
					mossaValida2 = true;
				}
				
			}
				
				
			Strada strada1 = stato.getMappa().getStrada(Integer.parseInt(messaggio));
			stato.getGiocatore(j).setPosizione(strada1);
			strada1.setFlagGiocatore(true);
			
			int cod1 = ProtocolloDiRete.NOTIFICA_MUOVI_PASTORE_INIZIALE.getCodice();
			int idGiocatore1 = stato.getGiocatore(j).getIdGiocatore();
			String mss1 = cod1+":"+messaggio+":"+idGiocatore1;
			comunicazione.notificaTutti(mss1);
		

			}
		}	
		}
			
		//Finisce il for, inizia la vera partita per tutti i giocatori fino a quando il numero di rencinti e' maggiore di 0
		
		//Istanzia il turno per una partita a 4-3-2 giocatori
	
	public void partitaVera() {
	
		
		while(stato.getRecinti() >= 0) {
			
			for(int i = 0; i < numeroGiocatori; i++) {
				
				
				//Recupero il giocatore giocante salvandolo nello stato partita
				//e metto che lui stesso sta giocando
				Giocatore g = stato.getGiocatore(i);
				stato.setGiocatoreGioca(g);
				g.setInGioco(true);
				stato.setNumeroMosse(0);
				stato.setUltimaMossa(-1);
				stato.setPastoreMosso(false);
				
				//Crea il messaggio al giocatore che e' in gioco
				int codice = ProtocolloDiRete.IN_GIOCO.getCodice();
				String stream = Integer.toString(codice);
				comunicazione.notificaGiocatore(i, stream);
				//Notifica i recinti rimanenti a tutti i giocatori
				int codice5 = ProtocolloDiRete.NUMERO_RECINTI.getCodice();
				int numero = stato.getRecinti();
				String stream5 = codice5+":"+numero;
				comunicazione.notificaTutti(stream5);				
				
				//Se i giocatori sono 2 gli chiedo quale pastore usare
				if(numeroGiocatori == 2) {
					
					int codice1 = ProtocolloDiRete.SCEGLI_PASTORE.getCodice();
					String stream1 = codice1+":"+i;
					boolean valido = false;
					String messaggio = null;
					
					//Gli richiedo il posizionamento fino a quando non mi da un pastore corretto
					while(!valido) {
						
						messaggio = comunicazione.ottieniParametri(stream1);
					
						//Se la strada non e' una di un pastore allora gli richiedo la mossa
						if(Mappa.controllaIdStrada(messaggio)) {
							
							Strada strada = stato.getMappa().getStrada(Integer.parseInt(messaggio));
							
							List<Strada> pastori = g.getPosizioni();
							
							for(Strada s : pastori) {
								if(s == strada){
									stato.setPastoreInUso(strada);
									valido = true;
								}
							}
					
								
					
						}
					}
					
				} else {
					//Altrimenti e' per default la posizione dell'unico pastore nell'arrayGiocatore
					stato.setPastoreInUso(g.getPosizioni().get(0));
				}
				
				this.runTurno();
				g.setInGioco(false);
				//comunico al giocatore che ha finito il turno che ha concluso le sue mosse
				int codice2 = ProtocolloDiRete.FINE_TURNO.getCodice();
				comunicazione.notificaGiocatore(i, Integer.toString(codice2));
			}
		}
		
		
		String[] giocatoriVinc = stato.calcolaVincitore();
		
		int codice = ProtocolloDiRete.NOTIFICA_VINCITORI.getCodice();
		String mess = Integer.toString(codice);
		for(int i = 0; i<giocatoriVinc.length; i++) {
			mess = mess+":"+giocatoriVinc[i];
		}

		comunicazione.notificaTutti(mess);
		//calcolo vincitori
	}
	



	/**
	 * Funzione che controlla che l'azione di muovere una pecora e' effettivamente possibile
	 * @param idRegione = regione di partenza
	 * @param muoviNera = indica se si e' scelto di muovere la pecora nera
	 * @throws Exception = lancia un'eccezione a Mossa() gestita da runTurno() nel caso non
	 * si verifichino le condizioni previste per la mossa della pecora
	 */
	private void controllaMuoviPecora(int idRegione, boolean muoviNera) throws MossaNonValidaException {

		//Recupera l'oggetto Regione di partenza partendo dall'id passatogli dal metodo 
		Regione r = stato.getMappa().getRegione(idRegione);
		//Controlla se il contatore delle pecore della regione di partenza non sia =0 altrimenti viene lanciata
		//un'eccezione
		if(r.getContatorePecore() == 0 && !r.getPecoraNera()) {
			throw new MossaNonValidaException();
		}
		
		//Controlla se il pastore e' effettivamente vicino alla regione in cui vuole spostare la pecora
		Strada posizionePastore = stato.getPastoreInUso();
		if(!stato.getMappa().controllaRegioneAdiacenteStrada(posizionePastore, r)) {
			throw new MossaNonValidaException();
		}
		
		//Controllo se si e' selezionato di spostare la nera che ci sia effettivamente nella regione di partenza
		if(muoviNera && !r.getPecoraNera()) {
			
			throw new MossaNonValidaException();
					
				}
			
		//Muovo effettivamente la pecora
		this.muoviPecora(idRegione, muoviNera);

	}

	/**
	 * Funzione che controlla che possa essere effettuata l'operazione di acquisto di una
	 * carta, se non e' possibile viene lanciata un'accezione al metodo chiamante che
	 * poi verra' gestita dal runTurno();
	 * @param tipo = Stringa del tipo di carta regione si vuole acquistare
	 * @throws Exception = eccezione che viene lanciata se una della condizione non e'verificata
	 * e la mossa non si puo' fare
	 */
	private void controllaCompraCarta(String tipo) throws MossaNonValidaException {

		//Recupera il costo della carta del tipo terreno richiesto
		int costo = stato.getCostoCarteTipoNDisponibili(tipo);
		//Controlla che il costo del denaro sia maggiore di 4 (ovvero le carte sono finite)
		//o che il giocatore abbia troppo poco denaro per l'acquisto
		Regione r1 = stato.getPastoreInUso().getRegione1();
		Regione r2 = stato.getPastoreInUso().getRegione2();
		//Controllo che il tipo che scelgo sia effettivamente di una delle due regioni adiacenti
		if(!(r1.getTipo().equals(tipo)) && !(r2.getTipo().equals(tipo))) { 
			throw new MossaNonValidaException();
		}
		if(costo > 4 ) {	
			throw new MossaNonValidaException();
		}
		
		if(stato.getGiocatoreGioca().getDenaro()<costo) {
			throw new MossaNonValidaException();
		}
		//Effetua veramente l'operazione di acquisto carta
		this.compraCarta(tipo);
	}

	/**
	 * Controlla che il pastore possa essere effettivamente mosso oppure no. Se una delle 
	 * condizioni di validit' non si verifica, metodo lancia un eccezione al metodo
	 * Mossa() che lo invoca, il quale lo rimandera' al turno, chiedendo al giocatore di
	 * effettuare una'altra mossa
	 * @param g = giocatore che effettua la mossa
	 * @param idDestinazione = id della strada di destinazione, verra' recuperato dalla view
	 * mediante un listener
	 * @param idPartenza = id della posizione di partenza del pastore, verra' recuperato dalla
	 * view
	 * @throws Exception = se la mossa non puo' essere effettuata viene lanciata al metodo
	 * Mossa()
	 */
	private void controllaMuoviPastore(int idDestinazione) throws MossaNonValidaException {

		//Recupera la strada di destinazione dalla mappa
		Strada destinazione = stato.getMappa().getStrada(idDestinazione);
		//Recupera l'oggetto strada di partenza dalla mappa
		Strada partenza = stato.getPastoreInUso();
		//Controlla che la strada di destinazione non sia occupata da un pastore
		//o da un recinto, se lo e' lancia un'eccezione
		if(destinazione.getFlagGiocatore() ) {
			throw new MossaNonValidaException();
		}
		if(destinazione.getFlagRecinto()) {
			throw new MossaNonValidaException();
		}
				
		//Controlla se la strada in cui mi devo spostare e' adiacente o meno al pastore
		//Se non e' adiacente controllo denaro giocatore, se il denaro del giocatore e'
		//<0 allora lancio un'eccezione altrimenti decremento denaro di uno
		//e sposto il pastore
		if(!stato.getMappa().controllaStradaAdicente(partenza, destinazione) && stato.getGiocatoreGioca().getDenaro() < 1) {
				throw new MossaNonValidaException();
			}
		
		//Effettuo lo spostamento del pastore richiamendo il metodo che modifica il model	
		this.muoviPastore(idDestinazione);


	}

	
	/**
	 * verifica il tipo di mosse disponibili dall'utente, basandosi sui dati presenti
	 * nello stato partita
	 * @return = array di boolean, ciascuna posizione corrisponde a una mossa, che e' 
	 * abilitata se true e disabilitata se false
	 */
	private boolean[] controllaMosseDisponibili() {

		int ultimaMossa = stato.getUltimaMossa();
		boolean mossoPastore = stato.getPastoreMosso();
		int numeroMosse = stato.getNumeroMosse();
		boolean[] valide = stato.getMosseValide();

		switch(numeroMosse) {
		//controllo a che mossa sono e faccio le dovute considerazioni
		//Ho fatto una mossa, devo fare la seconda dopo aver invocato il metodo
		//Queste sono le mosse valide per il secondo turno
		case 1:
			
			if(!mossoPastore) {
				valide[ultimaMossa] = false;
				return valide;
			} else {
				return valide;
			}
			
		
		case 2:
			  
			if(!mossoPastore) {
				valide[0] = true;
				valide[1] = false;
				valide[2] = false;
				return valide;
			} else {
				if(ultimaMossa != 0) {
				valide[ultimaMossa] = false;
				return valide;
				} else {
				valide[0] = true;
				valide[1]= true;
				valide[2] = true;
				return valide;
			}
			}
			
			
		 default: 
			 valide[0] = true;
			 valide[1] = true;
			 valide[2] = true;
			 return valide;
			 
			  
		}
}


	/**
	 * Controlla se effettivamente la mossa scelta dal giocatore e' valida o meno
	 * @param valide = array di mosse attive
	 * @return = true se e' valida false se non lo e'
	 */
	private boolean controllaMossaValida(boolean[] valide, String codiceMossa) {
		
		int indice = Integer.parseInt(codiceMossa);
		return valide[indice];
		
	}


	/**
	 * Metodo che istanzia il turno del giocatore, con un ciclo while fa in modo che il
	 * giocatore effettui esattamente 3 mosse per poi passare il controlla ad un'altro
	 * giocatore
	 * @param g = giocatore che sta giocando
	 * codici delle mosse (corrispondono agli indici degli array):
	 * 0 muoviPastore
	 * 1 muoviPecora
	 * 2 compraCarta
	 */
	private void runTurno() {
		
		//All'inizio del turno viene mossa la pecora e viene notificato ai giocatori dove di e' mossa
		this.muoviPecoraNera();
		int codice = ProtocolloDiRete.MUOVI_NERA.getCodice();
		int idRegione = stato.getIdRegionePecoraNera();
		String stream  = codice+":"+idRegione;
		comunicazione.notificaTutti(stream);
		
		//Inizia il turno del giocatore fino a che non ha fatto tre mosse continuo a chiederglielo
		while(stato.getNumeroMosse() < 3) {
			
			//Aggiorna le mosse disponibili al giocatore all'inizio di ogni turno
			stato.setMosseValide(controllaMosseDisponibili());
			//Notifico il giocatore delle mosse disponibili che puo' fare a inizio turno
			int codice1 = ProtocolloDiRete.NOTIFICA_MOSSE.getCodice();
			int idGiocatore = stato.getGiocatoreGioca().getIdGiocatore();
			boolean[] valide = stato.getMosseValide();
			String stream1 = codice1+":"+valide[0]+":"+valide[1]+":"+valide[2];
			comunicazione.notificaGiocatore(idGiocatore, stream1);
			
			try{
				//Comincia a chiedere mosse al giocatore, se una mossa non puo' essere fatta parte l'eccezione e non viene eseguita
				//La variabile mossaValida rimane false fino  a quando il giocatore non sceglie una mossa corretta
				boolean mossaValida = false;
				String codiceMossa = null;
				String[] parametri = null;
				
				while (!mossaValida) {
				int codice3 = ProtocolloDiRete.SCEGLI_MOSSA.getCodice();
				String stream3 = codice3+":"+stato.getGiocatoreGioca().getIdGiocatore();
				codiceMossa = comunicazione.ottieniParametri(stream3);
				parametri = this.recuperaParametriMossa(codiceMossa);
				
				
				if(controllaMossaValida(stato.getMosseValide(), parametri[0])) {
					mossaValida = true;
				}
				} 
					
					this.runMossa(codiceMossa);
					//Aggiono il numero di mosse del giocatore sia nel metodo che nello stato
					//aumento il numero di mosse fatte e aggiorno l'ultima mossa fatta dal giocatore
					stato.setNumeroMosse(stato.getNumeroMosse()+1);
					stato.setUltimaMossa(Integer.parseInt(parametri[0]));
					
			}	catch (MossaNonValidaException e) {
				
				
				Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore mossa non valida", e);

				String errore = Integer.toString( ProtocolloDiRete.ERRORE.getCodice());
				int idGiocatore1 = stato.getGiocatoreGioca().getIdGiocatore();
				comunicazione.notificaGiocatore(idGiocatore1, errore);
				//Ritorna nel while se catturo l'eccezione e continuo a chiedere mosse al 
				//giocatore
				//Display l'errore al giocatore
			}
		}
		
	

	}

	/**
	 * Metodo che in base al codice che riceve fa in modo tale che la mossa voluta venga
	 * effettuata dai metodi prestabiliti, una volta ricevuto il codice mossa (dal server
	 * che avrà dialogato con il giocatore e gli avra' indicato in qualche modo la mossa che 
	 * voleva fare
	 * Le stringhe che possono essermi passate dal server sono tre:
	 * [0:idStrada] (MuoviPastore)
	 * [1:idRegionePartenza:PecoraNeraTrue] (MuoviPecora)
	 * [2:tipoRegione] (compraCarta)
	 * @param codice = numero che corrisponde alla mossa scelta del giocatore
	 */
	private void runMossa(String mossaECodici) throws MossaNonValidaException{
		
		String[] codice = this.recuperaParametriMossa(mossaECodici);
		int mossa = Integer.parseInt(codice[0]);
		
		ProtocolloDiRete mossaScelta = null;
		for(ProtocolloDiRete m :ProtocolloDiRete.values()) {
			if(m.getCodice() == mossa) {
			mossaScelta = m;
			break;
			}
		}


		switch(mossaScelta) {
		case MUOVI_PASTORE:
		
			//Scelgo di muovere il pastore, recupero i paramentri che mi servono
			int idDestinazione = Integer.parseInt(codice[1]);
			int idPartenza = stato.getPastoreInUso().getId();
			int idGiocatore = stato.getGiocatoreGioca().getIdGiocatore();
			//invoco il "controlla pastore", lancia una eccezione se non puo' eseguire la mossa
			this.controllaMuoviPastore(idDestinazione);
			
			stato.setPastoreMosso(true);
			//Notifico al giocatore il denare rimanente
			int cod1 = ProtocolloDiRete.NOTIFICA_DENARO.getCodice();
			int denaro = stato.getGiocatoreGioca().getDenaro();
			String stream1 = cod1+":"+denaro;
			comunicazione.notificaGiocatore( stato.getGiocatoreGioca().getIdGiocatore(), stream1);
			int cod21 = ProtocolloDiRete.NUMERO_RECINTI.getCodice();
			int numeroRecinti = stato.getRecinti();
			String stream21 = cod21+":"+numeroRecinti;
			comunicazione.notificaTutti(stream21);
			int cod = ProtocolloDiRete.NOTIFICA_MUOVI_PASTORE.getCodice();
			String mss = cod+":"+idDestinazione+":"+idPartenza+":"+idGiocatore;
			comunicazione.notificaTutti(mss);
			break;
			
			
			
		case MUOVI_PECORA:
	
			//Scelgo di muovere una pecore, chiedo informazioni al server riguardo ai parametri
			int idRegionePartenza = Integer.parseInt(codice[1]); 
			boolean muoviNera = Boolean.parseBoolean(codice[2]);
			
			//Controllo se posso eseguire la mossa
			this.controllaMuoviPecora(idRegionePartenza, muoviNera);
			int cod2 = ProtocolloDiRete.NOTIFICA_MUOVI_PECORA.getCodice();
			Strada street = stato.getPastoreInUso();
			int idRegioneArrivo = 0;
			Regione regionePartenza = stato.getMappa().getRegione(idRegionePartenza);
			List<Regione> regioni = street.getSetRegioni();
			for(Regione r: regioni) {
				if(r != regionePartenza) {
					idRegioneArrivo = r.getId();
				}
			}
			
			String mss2 = cod2+":"+idRegioneArrivo+":"+idRegionePartenza+":"+muoviNera;
			comunicazione.notificaTutti(mss2);
			break;
			
		case COMPRA_CARTA:
		
			//Se devo comprare una carta, chiedo le informazioni al mio server per i parametri 
			//da passargli
			String tipo = codice[1];
			
			//controllo se posso eseguire la mossa
			this.controllaCompraCarta(tipo);
			int cod3 = ProtocolloDiRete.NOTIFICA_COMPRA_CARTA.getCodice();
			int denaro3 = stato.getGiocatoreGioca().getDenaro();
			int[] carte1 = stato.getGiocatoreGioca().getCarteTerreno();
			String mss3 = cod3+":"+denaro3+":"+carte1[0]+":"+carte1[1]+":"+carte1[2]+":"+carte1[3]+":"+carte1[4]+":"+carte1[5];
			comunicazione.notificaGiocatore(stato.getGiocatoreGioca().getIdGiocatore(),mss3);
			int cod4 = ProtocolloDiRete.NOTIFICA_CARTE_RIMANENTI.getCodice();
			int[] carte = stato.getCostoCarteRimanenti();
			String stream4 = cod4+":"+carte[0]+":"+carte[1]+":"+carte[2]+":"+carte[3]+":"+carte[4]+":"+carte[5];
			comunicazione.notificaTutti(stream4);
			break;
			
		default: break;
		} 

	}





	/**
	 * Metodo privato che genera un numero random compreso tra 1 e 6 
	 * @return = numero generato
	 */
	private int lanciaDado() {

		//Funzione usata per lanciare il dado all'inizio del turno
		//per il movimento della pecora nera
		int faccia;
		Random random = new Random();

		faccia = 1 + random.nextInt(6);
		return faccia;

	}
	
	/**
	 * Metodo che muove la pecora nera nella plancia di gioco. Pecora parte da sheepsburg
	 * e si muove per le varie regioni (se puo') all' inzio del turno di ogni giocatore
	 * @return = true se la pecora nera e' stata mossa, false se e' rimasta nella stessa
	 * regione
	 */
	private boolean muoviPecoraNera() {

		//Metodo da richiamare all'inizio di ogni turno per muovere la pecoraNera
		Mappa mappa = stato.getMappa();
		Regione arrivo;
		//Ottengo la regione con la pecora nera al suo interno
		Regione r = mappa.getRegionePecoraNera();
		//Ricevo la lista delle strade vicine alla regione di partenza
		List<Strada> strade = stato.getMappa().getStradeAdiacenti(r);
		//Lancio il dado random
		int n = lanciaDado();
		//Controllo se il numero corrispondene del dado e' persente in una delle caselle vicine	
		//Alla pecora
		for(Strada s : strade) {
			if(s.getValore() == n && !s.getFlagGiocatore() && !s.getFlagRecinto()) {
					List<Regione> regioniAdiacenti = s.getSetRegioni();
					for(Regione reg: regioniAdiacenti) {
						if(reg != r) {
							arrivo = reg;
							r.setPecoraNera(false);
							arrivo.setPecoraNera(true);
							r.diminuisciPecore();
							arrivo.aumentaPecore();
							stato.setIdRegionePecoraNera(arrivo.getId());
							return true;
						}
					}
			}		

		}
	

		return false;

	}


	/**
	 *  * Metodo che muove la pecora scelta dal giocatore (nera o bianca) sde disponibili
	 * viene passato il Giocatore, quale pastore, l'id della regione
	 * di partenza e se viene scelto di muovere la pecora nera(true) o no(false) 
	 * (controllo di validita' non effettuato nel metodo.
	 * @param idRegione = identificativo della regione di partenza
	 * @param muoviNera = indica se deve avvenire lo spostamento della pecora nera(true)
	 */
	private void muoviPecora(int idRegione, boolean muoviNera) {

		//Metodo che permette al giocatore di muovere la pecora nel proprio turno
		//vengono passati l'id della regione di partenza, quale pastore si vuole muovere
		//se si vuole o meno muovere una pecora nera oppure no e il giocatore
		//Recupero la regione dalla quale vuole spostare le pecore
	
		Regione partenza = stato.getMappa().getRegione(idRegione);
		Regione arrivo = null;
		Strada strada = stato.getPastoreInUso();
		List<Regione> regioni = stato.getMappa().getRegioneAdiacente(strada);
		//Determino la regione di arrivo e quella di partenza
		for(Regione r1 : regioni) {
			if(r1 != partenza) {
					arrivo = r1;
			} 
			

		}
		//Se voglio muovere la nera e nella regione di partenza e' presente
		if(muoviNera) {
			partenza.setPecoraNera(false);
			partenza.diminuisciPecore();
			arrivo.setPecoraNera(true);
			arrivo.aumentaPecore();
			stato.setIdRegionePecoraNera(arrivo.getId());
		
		} else {
		//voglio muovere un'altra pecora che non sia la nera e il contatore pecore e' non nullo
		
			partenza.diminuisciPecore();
			arrivo.aumentaPecore();
		}
	}	 


	/**
	 * Metodo che permette al giocatore di aggiungere una carta di un determinato tipo
	 * a quelle a lui disponibili, se possibile (controllo validita' non effettuato in questo
	 * metodo)
	 * @param tipo = Tipo della carta da comprare
	 */
	private  void compraCarta(String tipo) {

		
		//Metodo che permette di acquistare la carta desiderata del tipo voluto
		int costo = stato.getCostoCarteTipoNDisponibili(tipo);

		stato.setCarteDisponibili(tipo);
		stato.getGiocatoreGioca().setCartaTipoN(tipo);
		stato.getGiocatoreGioca().decrementaDenaro(costo);
	}


	/**
	 * Metodo che muove il pastore da una strada a un'altra se e' possibile 
	 * (controllo non avviena in questo metodo)
	 * @param idDestinazione = idRegione della destinazione del pastore
	 */
	private void muoviPastore(int idDestinazione) {

		//Metodo che permette che sposta il pastore nella casella scelta dal giocatore
		//Vengono passati il giocatore, l'identificativo della destinazione e il numero
		//che identifica quale pastore deve essere mosso (il primo indice 0 il secondo 
		// indice 1)
		Strada destinazione = stato.getMappa().getStrada(idDestinazione);
		Strada partenza = stato.getPastoreInUso();

		partenza.setFlagGiocatore(false);
		partenza.setFlagRecinto(true);
		destinazione.setFlagGiocatore(true);
		//cambio la destinazione del giocatore con la nuova posizione
		List<Strada> posizioni = stato.getGiocatoreGioca().getPosizioni();
		for(int j = 0; j< posizioni.size(); j++) {
			if(posizioni.get(j)==partenza) {
				posizioni.remove(posizioni.get(j));
				posizioni.add(destinazione);
			}
		}
		//Controllo se la destinazione è adiacente o meno alla vecchia posizione
		//se si allora decremento di 1 unita' il denaro
		if(!stato.getMappa().controllaStradaAdicente(partenza, destinazione)) {
			stato.getGiocatoreGioca().decrementaDenaro(1);
		}
		stato.decrementaRecinti();
		stato.setPastoreInUso(destinazione);
	}


	/**
	 * Metodo che decodifica la stringa passatami dal server al momento della
	 * della chiamata della mossa
	 */

	private String[] recuperaParametriMossa(String codice) {

		//Divido la stringa in corrispondenza dei ":"
		return codice.split(":");
	}

	@Test
	public StatoPartita testOttieniStato(){
		return this.stato;
	}

    @Test
	public boolean[] testGetControllaMosseDisponibili() {
		
		return controllaMosseDisponibili();
	}

    @Test
	public boolean testControllaMossaValida(boolean[] mosseValide, String codMossa) {
		
		return controllaMossaValida(mosseValide, codMossa);
	}

    @Test
	public int testGetLancioDado() {
		
		return lanciaDado();
	}
    
    @Test
    public void testGetMuoviPecora( int regione, boolean muoviPecoraNera){
    	
    	muoviPecora(regione, muoviPecoraNera);
    }
    
    @Test
    public void testGetCompraCarta(String tipo){
    	
    	compraCarta(tipo);
    }
    
    @Test
    public void testGetMuoviPastore(int destinazione){
    	
    	muoviPastore(destinazione);
    }
    
    @Test
    public boolean testGetMuoviPecoraNera(){
    	
    	return muoviPecoraNera();
    }
    
    @Test
    public void testGetControllaMuoviPecora(int idRegionePartenza, boolean muoviNera) throws MossaNonValidaException{
    	
    	controllaMuoviPecora(idRegionePartenza, muoviNera);
    	
    }
    
    @Test
    public void testGetControllaCompraCarta(String tipo) throws MossaNonValidaException{
    	
    	controllaCompraCarta(tipo);
    	
    }
    
    @Test
    public void testGetControllaMuoviPastore(int stradaDestinazione) throws MossaNonValidaException{
    	
    	controllaMuoviPastore(stradaDestinazione);
    	
    }
    
    @Test
    public void testGetInizioPartita(){
    	
    	inizioPartita();
    }
    
    @Test
    public void testGetRunMossa(String messaggio) throws MossaNonValidaException{
    	
    	runMossa(messaggio);
    }
    
    @Test
    public void testGetRunTurno()throws MossaNonValidaException{
    	
    	runTurno();
    }

}





