package it.polimi.marchi_marocco.controller;


public interface GestoreComunicazione {

	/**
	 * Metodo che viene richiamato dal controller quando deve notificare un determinato giocatore
	 * @param numeroGiocatore = Id del giocatore, che viene usato per indicare lo stream d'uscita sul quale scrivere
	 * @param codice = Stringa formattata in modo opportuno a seconda delle azioni che deve fare il client
	 */
	void notificaGiocatore(int numeroGiocatore, String codice);
	
	/**
	 * Metodo che viene richiamato dal controller per notificare tutti i giocatori riguardo modifiche sullo stato partita
	 * Rivelanti per tutti.
	 * @param codice =  Stringa formattata in modo opportuno a seconda delle azioni che deve fare il client
	 */
	void notificaTutti(String codice);
	
	/**
	 * Metodo richiamato una volta dal controller quando servono dei parametri dall'utente 
	 * @param messaggio = Stringa formattata in modo che [codice:nGiocatore:parametro1:parametro2] ecc...
	 * a seconda del caso
	 * @return = Stringa formattata che contiene le informazioni necesserie al controller per concludere la sua mossa
	 */
	String ottieniParametri(String messaggio);

	
	


	
	
	

}
