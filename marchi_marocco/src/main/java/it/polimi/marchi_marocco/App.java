package it.polimi.marchi_marocco;

import it.polimi.marchi_marocco.view.IniziaPartita;
import it.polimi.marchi_marocco.view.InterfacciaView;

import java.awt.EventQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

	public class App extends InterfacciaView {

		public static void main(String[] args) {

			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						IniziaPartita start = new IniziaPartita();
						start.setVisible(true);
					} catch (Exception e) {
						Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Errore nell'avvio del programma", e);
					}
				}
			});
			
			
		
		}

	}


