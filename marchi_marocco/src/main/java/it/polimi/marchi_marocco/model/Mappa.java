package it.polimi.marchi_marocco.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.Graphs;

import it.polimi.marchi_marocco.model.Strada;

/**
 * Classe che fornisce tutti i metodi per settare e modificare la mappa all'occorrenza,
 * nonchè istanziarla e utilizzando il costruttore
 */

public class Mappa extends Graphs {
	
	//Classe mappa che implementa i metodi per la manipolazione della plancia si gioco
	private Parser parser;
	private final UndirectedGraph <Regione, Strada> mapGraph;
	private final Set<Strada> setStrade;
	private final Set<Regione> setRegioni;
	private static Set<Strada> setStradeStatico;
	private static Set<Regione> setRegioniStatico;
	private static String[] tipi = {"foresta", "pianura", "campo", "montagna", "deserto", "palude"};
	

	/**
	 * Costruttore che istanzia il parser e setta gli attributi della lista delle regioni e 
	 * delle strade e del grafo per le informazioni sulle stesse
	 */
	public Mappa() {
		
		//Costruttore di Mappa
		parser = new Parser();
		this.mapGraph = parser.getGraph();
		setStrade = mapGraph.edgeSet();
		setRegioni = mapGraph.vertexSet();
		setRegioniStatico = setRegioni;
		setStradeStatico = setStrade;
	}
	
	public  List<Regione> getRegioneAdiacente(Strada s) {
		
		//Ritorna l'arrayList delle regioni adiacenti alla strada passata come parametro
		List<Regione> regioni = new ArrayList<Regione>();
		regioni.add(s.getRegione1());
		regioni.add(s.getRegione2());
		return regioni;
	
	}
	
	public Set<Strada> getSetStrade() {
		
		//Ritorna l'intero set delle strade della mappa
		return setStrade;
	}
	
	public UndirectedGraph <Regione, Strada> getGrafo() {
		
		//Metodo che ritorna il riferimento al grafo della mappa
		return mapGraph;
	}
	
	public Strada getStrada(int id) {
		
		//Ritorna l'oggetto strada partendo dal suo ID
		for(Strada s : setStrade) {
			if(id == s.getId()) {
				return s;
			}
		}
	
		        return null;
	
	}
	
	public Regione getRegione(int id) {
		
		//Ritorna l'oggetto regione partendo dal suoi ID
		for(Regione r : setRegioni) {
			if(id == r.getId()) {
				return r;
			}
		}
		        return null;
	
		
	}
	
	public Set<Regione> getSetRegioni() {
		
		//Ritorna la lista di tutte le regioni
		return setRegioni;
	}
	
	public Regione getRegionePecoraNera() {
		
		//Ritorna la regione con la pecora nera al suo interno
		for(Regione r : setRegioni) {
			if(r.getPecoraNera()) {
				return r;
			}
		}
				return null;
	}
	
	/**
	 * Metodo che ritorna la lista delle strade adiacenti a quella data	
	 * @param s = Strada di cui si vogliono sapere le strade adiacenti
	 * @return = lista delle strade adiacenti a quella strada
	 */
	public List<Strada> getStradeAdiacenti(Strada s) {
		
		//Ritorna l'ArrayList delle strade adiacenti alla strada passata come parametro (algoritmo triangolare)
		//Salvo le Regioni (vertici) collegati dalla strada s
	
		//Recupero il riferimento al grafo dallo stato partita
		Regione r1 = s.getRegione1();
		Regione r2 = s.getRegione2();
		//Salvo lista delle regioni vicine a r1 e r2
		List<Regione> listaRegioniVicineR1 = neighborListOf(mapGraph, r1);
		List<Regione> listaRegioniVicineR2 = neighborListOf(mapGraph, r2);
		List<Regione> listaRegioniComuni = new ArrayList<Regione>();
		//con due cicli for ritrovo le regioni vicine a entrambe(r1 e r2)
		for(Regione r3 : listaRegioniVicineR1) {
			for(Regione r4 : listaRegioniVicineR2) {
				if(r3 == r4) {
					listaRegioniComuni.add(r3);
				}
			}
		}	
		
		List<Strada> stradeAdiacenti = new ArrayList<Strada>();
		//Con due for annidati aggiungo all'ArrayList le strade che collegano r1 e r2 alle regioni
		//comuni ad entrambi
		for(Regione r4 : listaRegioniComuni) {
			stradeAdiacenti.add(mapGraph.getEdge(r4, r1));
			stradeAdiacenti.add(mapGraph.getEdge(r4, r2));
		}
		
		//Ritorno l'ArrayList di strade adiacenti alla strada richiesta
		return stradeAdiacenti;
		
	}

	/**
	 * Metodo che ritorna le strade adiacenti a una determinata regione
	 * @param r = Regione di cui si vuole avere info sulle strade
	 * @return = lista delle strade adiacenti a quella regione
	 */
	public List<Strada> getStradeAdiacenti(Regione r) {
	
		//Ritorna le strade adiacenti a una regione passata come parametro
	    UndirectedGraph <Regione, Strada> grafo = this.getGrafo();
		List<Strada> stradeArray = new ArrayList<Strada>();
		Set<Strada> stradeList = grafo.edgesOf(r);
			for(Strada s : stradeList) {
				stradeArray.add(s);
			}
			return stradeArray;	
		}

	/**
	 * Metodo che verifica se una strada data è adiacente a un'altra
	 * @param s = prima strada
	 * @param stradaAdiacente = strada di cui si vuole verificare l'adiacenza
	 * @return = true se la strada e' adiacente alla seconda, false se non lo e'
	 */
	public boolean controllaStradaAdicente(Strada s, Strada stradaAdiacente) {
	
	//Ritorna true se la strada s ha adiacente stradaAdiacente
	List<Strada> strade = this.getStradeAdiacenti(s);
	for(Strada s1 : strade) {
		if(s1.getId() == stradaAdiacente.getId()) { 
			return true;
		}
	}
		    
		   return false;
 }
	
	public boolean controllaRegioneAdiacenteStrada(Strada s, Regione r) {
		
		
		return s.getRegione1().getId() == r.getId() || s.getRegione2().getId() == r.getId();
		
	}
		
	
	/**
	 * Metodo checontrolla che l'identificativo della regione digitato che sia veramente presente
	 * @param id = identificativo digitato dall'utente
	 * @return = true se esiste e false se non esiste
	 */
	public static boolean controllaIdRegione(int id) {
		
		for(Regione r: setRegioniStatico) {
			if(r.getId() == id) {
				return true;
			}
			
		}
		return false;
	}
	
	/**
	 * Metodo che controlla che l'identificativo strada sia veramente esistente 
	 * @param id = id digitato da tastiera
	 * @return = true se esiste veramente, false se non esiste 
	 */
	public static boolean controllaIdStrada(String id)  {
		
		int stradaId = Integer.parseInt(id);
		
		for(Strada s : setStradeStatico) {
			if(s.getId() == stradaId) {
			return true;
			}
	}	
		return false;

	}
	
	/**
	 * Metodo che ritorna true se il tipo della regione è veramente presente come indice nella mappa
	 * @param tipo = tipo della regione digitato
	 * @return = true se e' un vero tipo, false se il tipo non esiste
	 */
	public static boolean controllaTipiRegione(String tipo) {
		
		for(String s: tipi) {
			if(tipo.equals(s)) {
			return true;	
			}
	}
		return false;
	}
	
	/**
	 * Metodo statico che controlla che sulla strada selezionata non ci sia gi un pastore ad occuparla,
	 * Se si ritorna false, se  libera torna true
	 * @param idStrada = identificativo della strada di cui bisogna verificare l'occupazione
	 * @return = false se  occupata, true se  libera
	 */
	public static boolean controllaValiditaPosizionePastore(String idStrada)  {
		
				int idstrada = Integer.parseInt(idStrada);
				
				for(Strada s : setStradeStatico) {
					
					if(s.getId() == idstrada) {
						return(!s.getFlagGiocatore()); 
					}
				}
				
				return false;
	
}

	

}