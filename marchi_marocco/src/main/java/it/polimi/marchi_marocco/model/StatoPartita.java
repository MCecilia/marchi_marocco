package it.polimi.marchi_marocco.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.junit.Test;



/**Classe che contiene tutti i dati sensibili della partita
 * contiene nRecinti, costo delle carte terreno disponibili durante la partita, 
 * un arraylist di gicatori, il gicatore che gioca al momento, la mappa, 
 * le mosse fatte fino ad ora dal giocatore in gioco
 */
public class StatoPartita extends MapTipoRegione {
	
	private int nRecinti = 20;
	private int[] carteTerreno = {0,0,0,0,0,0};
	private List<Giocatore> giocatori = new ArrayList<Giocatore>();
	private Giocatore giocatoreGioca;
	private int ultimaMossa = -1;
	private int numeroMosse = 0;
	private boolean[] mosseValide = {true, true, true};
	private boolean pastoreMosso = false;
	private Strada pastoreInUso;
	private Mappa mappa;
	private int idRegionePecoraNera = 0;
	private MapTipoRegione m;
	
	
	/**
	 * Costruttore dello stato partita
	 * Setta le istanze della mappa, inizializza la pecora nera a sheepsbourg e istanzia
	 * la classe MapTipoRegione per i tipi del terreno e delle carte
	 */
	public StatoPartita(int numero) {
		
		//costruttore dell stato_partita, istanzia lo stato di tutta la partita
		//colloca la pecora nera a sheepsburg
		//Verifico se devo istanziare i giocatori qui
		//Assegno denari a 30 se sono 2 e assegno una carta terreno iniziale a tutti i
		//giocatori
		
		mappa = new Mappa();
		Regione r = mappa.getRegione(0);
		r.setPecoraNera(true);
		m = new MapTipoRegione();
		int j = 1;
		for(int i = 0; i < numero; i++) {
			
			//L'id e' 0 ma il nome e' giocatore 1
			Giocatore giocatore = new Giocatore(i, "Giocatore"+j);
			j++;
			giocatori.add(giocatore);
		if(numero == 2) {
			giocatore.setDenaro(30);
		} else {
			giocatore.setDenaro(20);
		}
		}
		
		this.assegnaCarteIniziali();
			
	}
	
	
	public boolean getPastoreMosso() {
		return pastoreMosso;
	}

	public void setPastoreMosso(boolean pastoreMosso) {
		this.pastoreMosso = pastoreMosso;
	}
	
	public int getIdRegionePecoraNera() {
		return idRegionePecoraNera;
	}

	public void setIdRegionePecoraNera(int idRegionePecoraNera) {
		this.idRegionePecoraNera = idRegionePecoraNera;
	}

	public Strada getPastoreInUso() {
		return pastoreInUso;
	}

	public void setPastoreInUso(Strada pastoreInUso) {
		this.pastoreInUso = pastoreInUso;
	}

	public boolean[] getMosseValide() {
		return mosseValide;
	}

	public void setMosseValide(boolean[] mosseValide) {
		this.mosseValide = mosseValide;
	}

	public void setCarteDisponibili(String tipo) {
		
		//Incrementa il contenuto dell'array alla posizione specificata, il numero
		//contenuto nell'array rappresenta il costo delle carte terreno disp per tipo 
		String ret = tipo;
        int posizione = -1;
        
        posizione = getValoreTipoRegione(ret);
        carteTerreno[posizione]++;
    
    }
	
	public int getCostoCarteTipoNDisponibili(String tipo) {
        //Restituisce il costo del tipo di catrta selezionato 
        //del tipo indicato 
        
        String ret = tipo;
        int posizione = -1;
       
           posizione = getValoreTipoRegione(ret);
           return carteTerreno[posizione];
    	}
	
	
	public int[] getCostoCarteRimanenti() {
		return this.carteTerreno;
	}


	public Giocatore getGiocatore(int id) {
		
		//Ritorna un oggetto giocatore utilizzando il suo ID
		for(Giocatore g : giocatori) {
			if(g.getIdGiocatore() == id) {
			
				return g;
			}
		}
				return null;
	}
	
	public Giocatore getGiocatorePosizioneArray(int indice) {
		
		return giocatori.get(indice);
	}
	
	public void decrementaRecinti() {
		
		//Decrementa i recinti totali disponibili
		nRecinti--;
	}
	
	public void insertGiocatore(Giocatore g) {
		
		//Inserisce un giocatore nell'arrayList
		giocatori.add(g);
	
	}
	
	public int getIndiceGiocatore(Giocatore g) {
		
		//Ritorna l'indice del giocatore nell'array
		return giocatori.indexOf(g);
		
	}
	
	public Mappa getMappa() {
		
		return mappa;
		
	}
	
	public int getRecinti() {
		
		return nRecinti;
	}
	
	public void setGiocatoreGioca(Giocatore g) {
		
		this.giocatoreGioca = g;
	
	}
	
	public Giocatore getGiocatoreGioca() {
		
		return this.giocatoreGioca;
	}

	public int getUltimaMossa() {
		return ultimaMossa;
	}

	public void setUltimaMossa(int ultimaMossa) {
		this.ultimaMossa = ultimaMossa;
	}

	public int getNumeroMosse() {
		return numeroMosse;
	}

	public void setNumeroMosse(int numeroMosse) {
		this.numeroMosse = numeroMosse;
	}
	
	public MapTipoRegione getMapTipoRegione() {
		return m;
	}
	
	/**
	 * Metodo che calcola il punteggio di tutti i giocatori e restituisce il/i vincitori
	 */
	public String[] calcolaVincitore() {
		
		int punteggioMassimo = 0;
		
		int[] punteggi = calcolaPunteggi();
		//Calcolo punteggio massimo		
		for(int i = 0; i < punteggi.length; i++){
			if (punteggi[i] > punteggioMassimo){
				punteggioMassimo = punteggi[i];	
			}	
		}
		
		//Creo e costruisco arraylist vincitori
		List<Giocatore> vincitori = new ArrayList<Giocatore>();
		
		for (int j=0; j<giocatori.size(); j++) {
			
			if (giocatori.get(j).getPunteggioFinale() == punteggioMassimo) {
				vincitori.add(giocatori.get(j));
			}
		}
		
		//Creo l'array di stringhe con i nomi dei vincitori
		String[] vincitoriString = new String[vincitori.size()];
		int i = 0;
		for(Giocatore g : vincitori) {
			vincitoriString[i] = g.getNomeGiocatore();
			i++;
		}
		
		return vincitoriString;
		
	} 
	
	private int[] calcolaPunteggi(){
		int[] punteggi = new int[giocatori.size()]; 
		
		for ( int i=0; i<giocatori.size(); i++){
			
			int numCarteTipo = giocatori.get(i).getNumeroCarteTipoN("foresta");
			int numPecoreTipo = contaPecoreTipoRegione("foresta");
			int puntCorrente = numPecoreTipo*numCarteTipo;
			
			numCarteTipo = giocatori.get(i).getNumeroCarteTipoN("pianura");
			numPecoreTipo = contaPecoreTipoRegione("pianura");
			puntCorrente = puntCorrente + numPecoreTipo*numCarteTipo;
			
			numCarteTipo = giocatori.get(i).getNumeroCarteTipoN("campo");
			numPecoreTipo = contaPecoreTipoRegione("campo");
			puntCorrente = puntCorrente + numPecoreTipo*numCarteTipo;
			
			numCarteTipo = giocatori.get(i).getNumeroCarteTipoN("montagna");
			numPecoreTipo = contaPecoreTipoRegione("montagna");
			puntCorrente = puntCorrente + numPecoreTipo*numCarteTipo;
			
			numCarteTipo = giocatori.get(i).getNumeroCarteTipoN("deserto");
			numPecoreTipo = contaPecoreTipoRegione("deserto");
			puntCorrente = puntCorrente + numPecoreTipo*numCarteTipo;
			
			numCarteTipo = giocatori.get(i).getNumeroCarteTipoN("palude");
			numPecoreTipo = contaPecoreTipoRegione("palude");
			puntCorrente = puntCorrente + numPecoreTipo*numCarteTipo;
			
			puntCorrente = puntCorrente + giocatori.get(i).getDenaro();
			
			giocatori.get(i).setPunteggioFinale(puntCorrente);
			punteggi[i] = giocatori.get(i).getPunteggioFinale();
	
	
		}
		return punteggi;
		
		
	}


/**
	 * Metodo che assegna in modo random le carte terreno iniziali a ciascun giocatore
	 * all'inizio della partita
	 */
	public void assegnaCarteIniziali() { 
        
		Random random = new Random();
		int tipoCarta;
        int[] uscite = {0,0,0,0,0,0};
        for ( Giocatore g : giocatori){
                do{
                	tipoCarta = random.nextInt(6);
                	
                }while (uscite[tipoCarta] == 1);
                
                g.getCarteTerreno()[tipoCarta]++;
                uscite[tipoCarta] = 1;
                
                   
        }
                
}
	/**
	 * Metodo che conta il numero di pecore per ciascun tipo di regione, conta pure
	 * la presenza della pecora nera valente 2
	 * @param tipo = tipo della regione di cui vogliamo sapere il numero delle pecore
	 * @return = numero di percore di ciascun tipo di regione
	 */
	private int contaPecoreTipoRegione(String tipo) {

		//Ritorna il numero totale di pecore presenti sulle regioni di un determinato tipo
		//conta gia' la pecora nera come 2 punti
		Set<Regione> setRegioni = this.getMappa().getSetRegioni();
		int contatore = 0;
		for(Regione r : setRegioni) {
			if(r.getTipo().equals(tipo)) {
				contatore += r.getContatorePecore();
				if(r.getPecoraNera()) {
					contatore += 2;
				}
				}
			}
		return contatore;

	}
	
	@Test
	public int testGetContaPecoreTipoRegione(String tipoRegione){
		return contaPecoreTipoRegione(tipoRegione);
		
		
	}
	
	@Test
	public boolean[] testGetMosseValide(){
		return this.mosseValide;
	}
	
	
	}
	

	


