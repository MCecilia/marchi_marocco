package it.polimi.marchi_marocco.model;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.SimpleGraph;
import org.xml.sax.SAXException;

/**
 * Classe che richiama il Handler e istanzia la mappa con SAX
 */
public class Parser {
	
	 private UndirectedGraph <Regione, Strada> mapGraph = new SimpleGraph <Regione,Strada> (Strada.class);

	 /**
	  * Costruttore del parser
	  */
public Parser() {
    	 
    	 MyHandler handler = new MyHandler();
 		 SAXParserFactory factory = SAXParserFactory.newInstance();
	try {
		
		SAXParser parser = factory.newSAXParser();
		parser.parse(getClass().getResourceAsStream("/Mappa.xml"), handler);
		//Ritorno la Mappa appena costruita 
		mapGraph = handler.getmapGraph();
		
	} catch (ParserConfigurationException e) {  
		Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Errore di parser", e);
	} catch(SAXException e) {
		Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Errore di parser", e);
	} catch(IOException e) {
		Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Errore apertura file", e);
	}
	
  }

/**
 * Metodo che ritorna il grafo appena istanziato
 * @return mapGraph = grafo della mappa della partita
 */
public UndirectedGraph <Regione, Strada> getGraph() {
	//Ritorna il grafo costruito dal file XML
	return mapGraph; 
	}
}



