    
package it.polimi.marchi_marocco.model;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.UndirectedGraph;
import org.jgrapht.graph.SimpleGraph;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
 
 /**
  * Classe che utilizza il parser SAX per leggere il file XML e stanziare il grafo
  *Ogni apertura tag corrisponde un azione dell'utente
  */
public class MyHandler extends DefaultHandler {
 
    //Creo un Grafo al quale aggiugere i nodi e i vertici leggendo iln file xml e usando i costruttori di classe specifici.
    private UndirectedGraph <Regione, Strada> mapGraph = new SimpleGraph<Regione,Strada>(Strada.class);
    private boolean tagTipo = false;
	private boolean tagValore = false;
	private boolean tagr1 = false;
    private boolean tagr2 = false;
    private int idRegione;
    private int idStrada;
    private int valoreStrada;
    private int idr1;
    private int idr2;
    //Salvo le regione che creo in una collezione di oggetti (per fare gli archi del grafo).
    private List<Regione> nodi = new ArrayList<Regione>();
    private Regione r;
    private Regione r1;
    private Regione r2;
    private Strada s;
    
   
    public UndirectedGraph<Regione,Strada> getmapGraph() {
	      
    	return mapGraph;
    }

   @Override
   public void startElement(String uri, String localName, String qName, Attributes attribute)
   		throws SAXException {
	
	//Legge tag regione
	if("regione".equalsIgnoreCase(qName)) { 
   		idRegione = Integer.parseInt(attribute.getValue("id"));
   		}
   	if("tipo".equalsIgnoreCase(qName)) {
   		tagTipo = true;
   	}
   	if("strada".equalsIgnoreCase(qName)) {
   	    idStrada = Integer.parseInt(attribute.getValue("id"));    
   	   }
   	if("valore".equalsIgnoreCase(qName)) {
   		tagValore = true;
   	}
   	if("item1".equalsIgnoreCase(qName)) {
   		tagr1 = true;
   	}
   	if("item2".equalsIgnoreCase(qName)) {
   		tagr2 = true; 
   	}
  }
   	
 
    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
	
    String stringa = new String(ch, start, length);
    stringa = stringa.trim();
    
    if(tagTipo) {
    	//creo la regione passando l'id e il tipo
    	r = new Regione(idRegione, stringa);
    	//aggiugo la regione alla collezione
    	nodi.add(r);  
    }
    
    if(tagValore) {
    	//trasformo il valore letto in un int 
    	valoreStrada = Integer.parseInt(stringa);
    	}
    
    if(tagr1) {
    	//Dopo che ho letto l'id della prima regione adiacente,
    	//scandisco il Set regioni e lo aggiungo al set della nostra Strada
        idr1 = Integer.parseInt(stringa);
        for(int i=0; i<nodi.size(); i++) {
    	  r1 = nodi.get(i);
    	  int x = r1.getId();
    	  if(idr1 == x) {
    	  break;
    	  }
    	}
    }
   
    
   if(tagr2) {
	   //Stesse operazioni precedenti pero' fatte sulla regione 2
	   idr2 = Integer.parseInt(stringa);
   	   for(int i=0; i<nodi.size(); i++) {
   	   r2 = nodi.get(i);
   	   int x = r2.getId();
   	   if(idr2 == x) { 
   	   break;
   	   }
   	 }
   }
 }
   
  
   @Override
   public void endElement(String uri, String localName, String qName)
		throws SAXException {
	
	
	if("regione".equalsIgnoreCase(qName)) {
		//Aggiunge un nodo al grafo mettendo la regione appena costruita
		mapGraph.addVertex(r);
	}
	
	if("tipo".equalsIgnoreCase(qName)) {
	    tagTipo = false;
	}
	
	if("valore".equalsIgnoreCase(qName)) {
		tagValore = false;
	}
	
	if("item1".equalsIgnoreCase(qName)) {
		tagr1 = false;
	}
	
	if("item2".equalsIgnoreCase(qName)) {
		tagr2 = false;
	}
	
    if("strada".equalsIgnoreCase(qName)) {
    	
    	//creo la strada mediante il suo costruttore
    	//creo un arco che collega le 2 regioni ritrovate dal set e metto a falso il tag Strada
    	s = new Strada(idStrada, valoreStrada, r1, r2);
    	mapGraph.addEdge(r1, r2, s);	
	}
	
   }
}

 
 
   