package it.polimi.marchi_marocco.model;


import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.io.*;

/**
 *Classe che fornisce i tipi delle regioni e le lega con una chiave univoca mediante
 *la classe properties. I tipi sono caricati da file XML
 */
public class MapTipoRegione {
	//Classe MapTipiRegione crea un'associazione univoca tra chiavi (TipiRegione) e valori(posizione nell'array).

	private static Properties prop;
	
	/**
	 * Costruttore della classe MapTipoRegione che apre il file XML e crea l'oggetto basandosi
	 * sui dati ricevuti da file XML
	 */
	public MapTipoRegione() {
		
		//Classe Properties che estende Hashtable.
		prop = new Properties();
		
		try{
			//FileInputStream xmlInStream = new FileInputStream(getClass().getResourceAsStream("/MapTipo.xml"));
			//Leggo file XML.
			prop.loadFromXML(getClass().getResourceAsStream("/MapTipo.xml"));
		} catch(IOException e) {
			//Gestisce eccezioni se avvengono all'apertura dello Stream da File.
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Errore apertura file", e);

	   }

	}
	
	/**
	 * Metodo che 
	 * @param valore = tipo terreno 
	 * @return l'intero a cui corrisponde la stringa che è passata
	 */
	public synchronized int getValoreTipoRegione(String valore) {
		
		//Dalla Stringa tipo ritorna il valore della posizione nell'array.
			return Integer.parseInt(prop.getProperty(valore));
	}
	   
	
}

