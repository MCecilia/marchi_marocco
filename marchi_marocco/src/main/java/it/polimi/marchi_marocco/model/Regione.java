package it.polimi.marchi_marocco.model;

/**
 *Classe che fornisce l'oggetto con tutte le informazioni rigurdanti la regione 
 */
public class Regione {
	
	private final int id;
	private final String tipo;
	private boolean pecoraNera = false;
	private boolean pecoraMossa = false;
	private int contatorePecore = 1;
	
	/**
	 * Costruttore della regione
	 * @param id = identificativo della regione
	 * @param tipo = tipo terreno della regione
	 */
	public Regione(int id, String tipo) {
		//costruttore del della regione, recupera l'ID e il tipo dal file XML da parsare
		this.id = id;
		this.tipo = tipo;
	}
	
	public int getId() {
		return id;
	}
	
	public String getTipo() {
		
		return tipo;
	}
	
	public boolean getPecoraNera() {
		return pecoraNera;
	}
	
	public boolean getPecoraMossa() {
		return pecoraMossa;
	}
	
	public int getContatorePecore() {
		return contatorePecore;
	}
	
	public void aumentaPecore() {
		contatorePecore++;
	}
	
	public void diminuisciPecore() {
		contatorePecore--;
	}
	
	public void setPecoraNera(boolean b) {
		pecoraNera = b;
	}
	
	public void setPecoraMossa(boolean b) {
		pecoraMossa = b;
	}

	
}
