package it.polimi.marchi_marocco.model;

import java.util.ArrayList;
import java.util.List;

import it.polimi.marchi_marocco.model.Regione;

/**
 * Classe che fornisce tutti gli attributi della strada: identificativo
 * valore sulla plancia di gioco, lista delle regioni adiacenti
 * flag del pastore e del recinto
 */
public class Strada {
	
	private final int id;
	private final int valore;
	private final List<Regione> regioniAdiacenti = new ArrayList<Regione>();
	private boolean flagRecinto = false;
	private boolean flagGiocatore = false;
	
	/**
	 * Costruttore della strada
	 * @param id = identificativo univoco della regione
	 * @param valore = numero della strada sulla plancia di Gioco
	 * @param r = prima regione adiacente alla strada
	 * @param r1 = seconda regione adiacente strada
	 */
	public Strada(int id, int valore, Regione r, Regione r1) {
		//costruttore della strada, vengono passati id, valore casella, e le regioni che collega
		this.id = id;
		this.valore = valore;
		regioniAdiacenti.add(r);
		regioniAdiacenti.add(r1);
	}
   
	public Regione getRegione1() {
		return regioniAdiacenti.get(0);
	}
	
	public Regione getRegione2() {
		return regioniAdiacenti.get(1);
	}
	
	public int getId() {
		return id;
	}
	
	public int getValore() {
		return valore;
	}
	
	public void setFlagRecinto(boolean b) {
		flagRecinto = b;
	}
	
	public void setFlagGiocatore(boolean b) {
		flagGiocatore = b;
	}
	
	public boolean getFlagRecinto() {
		return flagRecinto;
	}
	
	public List<Regione> getSetRegioni() {
		
		return this.regioniAdiacenti;
	}
	
	public boolean getFlagGiocatore() {
		return flagGiocatore;
	}
	
}

