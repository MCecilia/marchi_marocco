package it.polimi.marchi_marocco.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe che provvede a fornire tutti gli attributin di giocatore
 * tra cui id, denaro, carte terreno possedute, posizioni
 */

public class Giocatore extends MapTipoRegione {
	    //La classe identifica un giocatore con il suo ID
	    //Tiene memoria della posizione del /suoi pasto
	    //delle sue carte terreno e della sua interfaccia
	    
	    private final int idGiocatore;
	    private final String nomeGiocatore;
	    private int numeroMossa;
	    private int denaro = 0;
	    private int[] carteTerreno = {0,0,0,0,0,0};
	    private List<Strada>posizioni = new ArrayList<Strada>();
	    private boolean inGioco = false;
	    private int punteggioFinale;
	    private boolean setSecondoPastore = false;
	    
	    public Giocatore(int id, String nome) {
	    	
	        //Inizializza l'ID secondo quanto passato dal controller e il nomeGiocatore
	         idGiocatore = id;
	         nomeGiocatore = nome;
	         
	    }
	   
	    public int getIdGiocatore() {
	    	
	        //Restituisce l'ID del giocatore
	        return idGiocatore;
	    }
	    
	    public String getNomeGiocatore() {
			return nomeGiocatore;
		}

		public int getDenaro() {
	        
	    	//Restituisce il denaro che il giocatore possiede
	        return denaro;
	    }
	    
	    public void setDenaro(int valore) {
	        //Modifica l'attributo denaro al valore stabilito dal controller
	        denaro = valore;
	    }
	    
	    public void decrementaDenaro(int costo) {
	    	
	    	//Decrementa del costo il denaro del Giocatore
	    	//REQUIRES: denaro >= costo
	            denaro-= costo;
	    }

	    
	    public boolean getSetSecondoPastore() {
			return setSecondoPastore;
		}

		public void setSetSecondoPastore(boolean setPrimoPastore) {
			this.setSecondoPastore = setPrimoPastore;
		}

		public int getNumeroMossa() {
			return numeroMossa;
		}

		public void setNumeroMossa(int numeroMossa) {
			this.numeroMossa = numeroMossa;
		}

		/**
	     * 
	     * @param fornisce il tipo del terreno
	     * @return ritorna il numero delle carte terreno di tipo selezionato
	     */
	    public int getNumeroCarteTipoN(String tipo)  {
	        //Restituisce il numero di carte in possesso del giocatore 
	        //del tipo indicato 
	        
	        String ret = tipo;
	        int pos = -1;
	       
	           pos = getValoreTipoRegione(ret);
	    	   return carteTerreno[pos];
	    	}
	       
	    /**
	     * @param fornisce il tipo del terreno che deve essere modificato
	     * e modifica l'array delle carte nella posizione specifica
	     */
	    public void setCartaTipoN(String tipo) {
	        //Aggiunge una carta di un determinato tipo a quelle in possesso del giocatore
	        //Si considera il denaro gia' decrementato, se necessario
	        
	        String ret = tipo;
	        int pos = -1;
	        
	        pos = getValoreTipoRegione(ret);
	        carteTerreno[pos]++;
	    
	    }
	    
	    public void setPosizione(Strada strada) {
	    	
	    	posizioni.add(strada);
	    }
	    
	    public List<Strada> getPosizioni() {
	    	
	    	return posizioni;
	    }
	    
	    public int[] getCarteTerreno(){
            return this.carteTerreno;
        }  

		public boolean getInGioco() {
			
			return inGioco;
		}

		public void setInGioco(boolean inGioco) {
			this.inGioco = inGioco;
		}
		
		public int getTipoCartaIniziale() {
			for(int i = 0; i < carteTerreno.length; i++) {
				if(carteTerreno[i] == 1) {
					return i;
				}
				
			}
			return -1;
		}
		
		public int getPunteggioFinale(){

			return this.punteggioFinale;

		}

		public void setPunteggioFinale ( int punteggio){

			punteggioFinale = punteggio;

			

		}
	    
	    
	}