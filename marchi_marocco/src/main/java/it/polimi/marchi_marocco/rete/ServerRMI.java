package it.polimi.marchi_marocco.rete;

import it.polimi.marchi_marocco.controller.GestorePartita;
import it.polimi.marchi_marocco.model.StatoPartita;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class ServerRMI extends UnicastRemoteObject implements IServerRMI {

	
	private static final long serialVersionUID = 1L;
	private static int idClient = 0;
	private int port = 4444;
	Registry registry;
	String address;
	private int numeroGiocatori = 0;
	private static int numeroConnessi = 0;
	boolean connesso = false;
	boolean gestoreCreato = false;
	InetAddress inetAddress;
	//Arrai delle interfacce utente che dovrà usare il server per comunucare
	private List<IGestoreClientRMI> clienti = new ArrayList<IGestoreClientRMI>();


public ServerRMI(int numeroGiocatori) throws RemoteException{

	this.numeroGiocatori = numeroGiocatori; 
	this.runServer();
}


public void  runServer() {

	
	//Creo Registry per i client RMI del server
	//(Mette a disposizione il metodo per la connessione

	try{
		registry = LocateRegistry.createRegistry(port);
		registry.rebind("rmiServer", this);
		inetAddress = InetAddress.getLocalHost();
		
	} catch(RemoteException e) {
		
		Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Remote Exception", e);
	} catch (UnknownHostException e) {
		
		Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "UnknownHost Exception", e);
	}
	
	
	JOptionPane.showMessageDialog(new JFrame(), "L'indirizzo IP del server è: "+ inetAddress.toString());

	//Crea il gestore di comunicazione lato server passando il registry e numero dei giocatori
	//Deve essere utilizzato dal GEstoreClient per la comunicazione
	
	
	//Aspetta fino a quando tutti i client si sono connessi
	while(numeroConnessi < numeroGiocatori) {
		
		while(!connesso) {
			
			try {

				Thread.sleep(100);

			} catch (InterruptedException e) {
				
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Errore sleep", e);
			}
			
		}
		
		if(connesso) {
		idClient++;
		numeroConnessi++;
		}
		
		connesso = false;

	
	}

		//Una volta che si sono connessi tutti creo la partita e il controller che la avvia
		//Passo al gestore il registry e l'array list di client
		StatoPartita nuovaPartita = new StatoPartita(numeroGiocatori);
		GestoreComunicazioneRMI gestore = null;
		try {
			gestore = new GestoreComunicazioneRMI(registry, clienti);
		} catch (RemoteException e1) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Remote Exception", e1);
		}
		
		//Bind del gestore di comunicazione nel registry cosi' che i client possano utilizzarne il riferimento
		//setta il true il falg della creazione del gestore
		try {
			registry.rebind("comunicazioneServer", gestore);
			gestoreCreato = true;
		
		} catch (RemoteException e) {
		
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Remote Exception", e);
		}
		GestorePartita controller = new GestorePartita(nuovaPartita, gestore, numeroGiocatori);
		controller.runPartita();
		controller.partitaVera();
		
		
}



//Metodo che viene usato dal client per connettersi al server
@Override
public boolean connettitiAlServer() throws RemoteException {
	
		if(numeroConnessi == numeroGiocatori) {
			return false;
			
		} else {
			
			return true;
		}
	
	}

//Metodo remoto
@Override
public int getId() throws RemoteException {
	
	return idClient;
}

//Metodo remoto
@Override 
public void notificaConnessione() throws RemoteException {
	
	this.connesso = true;
	
	
}


//Metodo Remoto
@Override 
public void addClient(IGestoreClientRMI g) throws RemoteException {
	
	clienti.add(g);
}

//Metodo Remoto
@Override
public boolean verificaCreazioneGestore() throws RemoteException{
	
	return gestoreCreato;
}


}


