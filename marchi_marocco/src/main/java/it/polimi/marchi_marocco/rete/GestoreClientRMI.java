package it.polimi.marchi_marocco.rete;

import it.polimi.marchi_marocco.controller.ProtocolloDiRete;
import it.polimi.marchi_marocco.view.View;

import java.io.Serializable;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;


public class GestoreClientRMI extends UnicastRemoteObject implements IGestoreClientRMI,  Observer, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;
	private IGestoreComunicazioneRMI comunicazioneRMI;
	private View view;
	int identificativo;
	IServerRMI rmiServer = null;
	Registry registry;
	String send;

	/**
	 * Costruttore del gestore del client che viene istanziato dal client stesso una volta connesso al server
	 * @param registry 
	 * @param registry = oggetto remoto passato al gestore client per la comunicazione con il server
	 * @param id = Identificativo del client
	 */
	public GestoreClientRMI( Registry registry, int id) throws RemoteException {

		//Oggetto remoto passato (GestoreComunicazione)
		this.registry = registry;
		this.identificativo = id;

	}
	
	public void aggiungiMappa(View mappa) {
		
		this.view = mappa;
		view.addObserver(this);
	}
	
	
	//Remoto. Chiamato dal GestoreComunicazioneRMI
	@Override
	public void notifica(String codice) throws RemoteException {
		
		if(rmiServer==null) {
			
		try {
			rmiServer = (IServerRMI)(registry.lookup("rmiServer"));
		} catch (NotBoundException e) {
			Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore connessione RMI", e);
		}
		
		while(!rmiServer.verificaCreazioneGestore()) {
			//Aspetto creazione gestore comunicazione
		}
			
		try {
			comunicazioneRMI = (IGestoreComunicazioneRMI)(registry.lookup("comunicazioneServer"));
		} catch (NotBoundException e) {
			Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore connessione RMI", e);
		}

		}
		
		String[] codici = this.recuperaParametriMossa(codice);
		ProtocolloDiRete protocollo = this.calcolaProtocollo(codici[0]);

		switch(protocollo) {
		case  INFO_INIZIALI:
			String denaro = codici[1];
			String id = codici[2];
			view.displayInfoIniziali(id, denaro, codici[3], codici[4], codici[5], codici[6], codici[7], codici[8], codici[9]);
			break;

		case NUMERO_RECINTI:
			String  numero= codici[1];
			view.displayRecinti(numero);
			break;

		case POSIZIONA_PASTORE:
			view.posizionaPastoreIniziale();
			break;

		case  SCEGLI_PASTORE:
			view.chiediQualePastore();
			break;

		case IN_GIOCO:
			view.displayInGioco();
			break;

		case FINE_TURNO:
			view.displayFineTurno();
			break;

		case MUOVI_NERA:
			String posizione = codici[1];
			view.displayMovimentoPecoraNera(posizione);
			break;

		case SCEGLI_MOSSA:
			view.scegliMossa();
			break;
			
		case NOTIFICA_MOSSE:
			String[] disponibili = new String[3];
			disponibili[0] = codici[1];
			disponibili[1] = codici[2];
			disponibili[2] = codici[3];
			view.displayMosseDisponibili(disponibili);
			break;
			
		case NOTIFICA_MUOVI_PASTORE_INIZIALE:
			view.displayPosizionamentoPastoreIniziale(codici[1], codici[2]);
			break;

		case NOTIFICA_DENARO:
			view.displayDenaro(codici[1]);
			break;

		case NOTIFICA_CARTE_RIMANENTI:
			view.displayCostoCarte(codici[1], codici[2], codici[3], codici[4], codici[5], codici[6]);
			break;

		case NOTIFICA_MUOVI_PASTORE:
			view.displayPastoreMosso(codici[1], codici[2], codici[3]);
			break;

		case NOTIFICA_MUOVI_PECORA:
			view.displayPecoraMossa(codici[1], codici[2], codici[3]);
			break;

		case NOTIFICA_COMPRA_CARTA:
			view.displayDenaro(codici[1]);
			view.displayCarteTerreno(codici[2],codici[3],codici[4],codici[5],codici[6],codici[7]);
			break;
			
		case  NOTIFICA_VINCITORI:
			String[] vincitori = new String[codici.length-1];
			int j=0;
			for(int i = 1; i<codici.length; i++) {
				
				vincitori[j] = codici[i];
				j++;		
			}
			view.displayVincitori(vincitori);
			break;

		case ERRORE:
			view.displayErrore();
			break;

		default: break;
		}

	}


		/**
		 * Divide la stringa in una array di String			
		 * @param codice = La stringa da dividere
		 * @return = L'array di String in cui ho le stringhe divise
		 */
		private String[] recuperaParametriMossa(String codice) {

				//Divido la stringa in corrispondenza dei ":"
					return codice.split(":");
		}

		/**
 		* Metodo che calcola il codice partendo dalla stringa ricevuta dal server
 		* @param codice = Stringa ricevuta dal server, con la codifica delle azioni da fare
 		* @return = tipo ProtocolloDiRete che seve per la codifica dell'azione da fare
 		*/
		private ProtocolloDiRete calcolaProtocollo(String codice) {

			ProtocolloDiRete protocollo = null;
			for(ProtocolloDiRete r : ProtocolloDiRete.values()) {
					if(r.getCodice() == Integer.parseInt(codice)) {
							return r;
					}
			}
				return protocollo;
		}

		//Observer , richiama il metodo del gestore di comunicazioneRMI
		@Override
		public void update(Observable view, Object arg) {
			
			String messaggio = arg.toString();
			try{
			comunicazioneRMI.inviaRisposta(messaggio);
			} catch (RemoteException e) {
				
				Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Errore connessione RMI", e);
			}
			
		}

}
