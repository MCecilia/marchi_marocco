package it.polimi.marchi_marocco.rete;

import it.polimi.marchi_marocco.view.View;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Classe che istanzia un client e permette al giocatore di iniziare una partita
 *
 */

public class ClientSS implements Runnable{
		
		private int port = 2224;
		private String ip;
		private static PrintWriter output;
		private static Scanner input;
		private static View mappa;

	    
	   public ClientSS(String ip, View mappa2) {
	   
	        this.ip=ip;
	        ClientSS.mappa = mappa2;
	   }
	   
	   
	   @Override
		public void run() {
			
	        try { 
	        	
	    		Socket client = new Socket (ip,port);
	        	
	    		output = new PrintWriter (client.getOutputStream(),true);
	    		input = new Scanner (client.getInputStream());
	         
	    			
	 } catch (IOException e) {
		 Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Controlla che l' ip immesso sia corretto", e);
	    			
	  }
	        
	        GestoreClientSS gestore = new GestoreClientSS(output , input, mappa);
	        

	   }
	   

	  


	   public void closeConnection() throws IOException {
		   
		   input.close();
		   output.close();
	   }




	
}
