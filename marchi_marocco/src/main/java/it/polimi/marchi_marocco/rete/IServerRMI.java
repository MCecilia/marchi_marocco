package it.polimi.marchi_marocco.rete;


import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IServerRMI extends Remote {

	/**
	 * Metodo usato dai client per connettersi al server
	 * @return = true se il client si e' efficacemente connesso, false se i posti disponibili sono esauriti
	 * @throws RemoteException
	 */
	boolean connettitiAlServer() throws RemoteException;
	
	/**
	 * Metodo che assegna un identificativo a ciascun Client
	 * @return = intero che rappresenta l'id del client
	 * @throws RemoteException
	 */
	int getId() throws RemoteException;

	/**
	 * Aggiunge una interfaccia client remota all'array list dei client nel server
	 * @param g = Interfaccia Remota Client che viene aggiunta all'array
	 * @throws RemoteException
	 */
	void addClient(IGestoreClientRMI g) throws RemoteException;
	
	/**
	 * Metodo che verifica se il gestore di comunicazione e' stato gia' creato oppure no
	 * @return = true se il gestore e' stato creato, false se non e' ancora stato creato
	 */
	boolean verificaCreazioneGestore()  throws RemoteException;
	
	/**
	 * Viene invocato dal client per indicare al server che e' connesso e in attesa
	 * @throws RemoteException
	 */
	void notificaConnessione() throws RemoteException;
	
	
	
	
	
	
	
	
	

}
