package it.polimi.marchi_marocco.rete;

import java.io.PrintWriter;
import java.util.Observable;
import java.util.Observer;
import java.util.Scanner;

import it.polimi.marchi_marocco.controller.ProtocolloDiRete;
import it.polimi.marchi_marocco.view.*;

/**
 * Classe che utilizza interfaccia il client con il server e la view, recevendo messaggi dalla view e mandandoli al server
 * @author Federica
 *
 */
public class GestoreClientSS implements Observer {
	
	private  PrintWriter output;
	private  Scanner input;
	private String codice;
	private boolean partitaInCorso = false;
	private View view;
	
	/**
	 * Costruttore che istanzia il GestoreClient Server Socket
	 * @param output = l'output che viene usato per scrivere sul server
	 * @param input = l'input che viene usato 
	 */
	public GestoreClientSS(PrintWriter output, Scanner input, View mappa) {
		
		
		this.output = output;
		this.input = input;
		view = mappa;
		view.addObserver(this);
		partitaInCorso = true;
		this.runClient();
		
	}
	
	public void runClient() {
		
		while(partitaInCorso) {
			
			//Se c'è una stringa sull'output mi metto in ascolto e recupero il codice che mi serve
			//Associo il codice che è nel prima posizione nell'Array di stringhe (gia splittato) all'enumerazione
			//delle varie mosse e dei casi che possono accadere
			while(!input.hasNextLine()) {
				//Aspetto valori di ritorno
			}
				codice = input.nextLine();
				String[] codici = this.recuperaParametriMossa(codice);
				ProtocolloDiRete protocollo = this.calcolaProtocollo(codici[0]);
			
				
			switch(protocollo) {
			//[idGiocatore:denaro:tipo1:tipo2:tipo3:tipo4:tipo5:tipo6:numeroGiocatori]
			case  INFO_INIZIALI:
				String denaro = codici[1];
				String id = codici[2];
				view.displayInfoIniziali(id, denaro, codici[3], codici[4], codici[5], codici[6], codici[7], codici[8], codici[9]);
				break;
				
			//[numeroRecinti]	
			case NUMERO_RECINTI:
				String  numero= codici[1];
				view.displayRecinti(numero);
				break;
			
			case POSIZIONA_PASTORE:
				view.posizionaPastoreIniziale();
				break;
				
			//[idStradaArrivo:idGicatore]
			case NOTIFICA_MUOVI_PASTORE_INIZIALE:
				view.displayPosizionamentoPastoreIniziale(codici[1], codici[2]);
				break;
				
			case  SCEGLI_PASTORE:
				view.chiediQualePastore();
				break;
				
			case IN_GIOCO:
				view.displayInGioco();
				break;
				
			case FINE_TURNO:
				view.displayFineTurno();
				break;
			
			//[idRegione]
			case MUOVI_NERA:
				String posizione = codici[1];
				view.displayMovimentoPecoraNera(posizione);
				break;
			
			//[boolean0:boolean1:boolean2] 
			case NOTIFICA_MOSSE:
				String[] disponibili = new String[3];
				disponibili[0] = codici[1];
				disponibili[1] = codici[2];
				disponibili[2] = codici[3];
				view.displayMosseDisponibili(disponibili);
				break;
				
			case SCEGLI_MOSSA:
				view.scegliMossa();
				break;
			
			
			//[valoreDenaro]
			case NOTIFICA_DENARO:
				view.displayDenaro(codici[1]);
				break;
				
			//[tipo1:tipo2:tipo2:tipo4:tipo5:tipo6]
			case NOTIFICA_CARTE_RIMANENTI:
				view.displayCostoCarte(codici[1], codici[2], codici[3], codici[4], codici[5], codici[6]);
				break;
				
			//[idStradaArrivo:idStradaPartenza:idGiocatore]
			case NOTIFICA_MUOVI_PASTORE:
				view.displayPastoreMosso(codici[1], codici[2], codici[3]);
				break;
				
			//[RegioneArrivo:regionePartenza:BooleanNera]
			case NOTIFICA_MUOVI_PECORA:
				view.displayPecoraMossa(codici[1], codici[2], codici[3]);
				break;
			
			case NOTIFICA_COMPRA_CARTA:
				view.displayDenaro(codici[1]);
				view.displayCarteTerreno(codici[2],codici[3],codici[4],codici[5],codici[6],codici[7]);
				break;
				
			case  NOTIFICA_VINCITORI:
				String[] vincitori = new String[codici.length-1];
				int j = 0;
				for(int i = 1; i<codici.length; i++) {
					vincitori[j] = codici[i];
					j++;		
				}
				view.displayVincitori(vincitori);
				break;
				
			case ERRORE:
				view.displayErrore();
				break;
				
			default: break;
			}
				
			}
		}
		
				
	/**
	 * Divide la stringa in una array di String			
	 * @param codice = La stringa da dividere
	 * @return = L'array di String in cui ho le stringhe divise
	 */
	private String[] recuperaParametriMossa(String codice) {
					
		//Divido la stringa in corrispondenza dei ":"
		return codice.split(":");
			   }

	/**
	 * Metodo che calcola il codice partendo dalla stringa ricevuta dal server
	 * @param codice = Stringa ricevuta dal server, con la codifica delle azioni da fare
	 * @return = tipo ProtocolloDiRete che seve per la codifica dell'azione da fare
	 */
	private ProtocolloDiRete calcolaProtocollo(String codice) {
		
		ProtocolloDiRete protocollo = null;
		for(ProtocolloDiRete r : ProtocolloDiRete.values()) {
			if(r.getCodice() == Integer.parseInt(codice)) {
			return r;
			}
		}
		return protocollo;
	}

	@Override
	public void update(Observable view, Object arg) {
		
		
		String messaggio = arg.toString();
		output.println(messaggio);
		output.flush();
		
	}
		
		
		
		 
	}
				
			
			
	
