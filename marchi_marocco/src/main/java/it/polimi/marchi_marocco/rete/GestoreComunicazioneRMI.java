package it.polimi.marchi_marocco.rete;

import it.polimi.marchi_marocco.controller.GestoreComunicazione;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GestoreComunicazioneRMI extends UnicastRemoteObject implements GestoreComunicazione, IGestoreComunicazioneRMI, Serializable{
	
	
	private static final long serialVersionUID = 1L;
	private List<IGestoreClientRMI> clienti = new ArrayList<IGestoreClientRMI>();
	private Registry registry;
	private String risposta;
	private boolean rispostaArrivata = false;
	
	
	/**
	 * Costruttore del GestoreComunicazioneRMI che viene invocato dal server una volta che sa il numero dei giocatori
	 * partecipanti
	 * @param registry = Registri della connessione RMI
	 * @param clienti2 = numero dei giocatori partecioenti alla partita
	 */
	public GestoreComunicazioneRMI(Registry registry, List<IGestoreClientRMI> clienti) throws RemoteException {

		this.registry = registry;
		this.clienti = clienti;
	}

	
	//GestoreComunicazione usato dal controller
	@Override
	public void notificaGiocatore(int numeroGiocatore, String codice) {
		
		try{
		clienti.get(numeroGiocatore).notifica(codice);
		
		}catch (RemoteException e) {
			
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Remote Exception", e);
		}
		
	}
	
	//GestoreComunicazione usato dal controller
	@Override
	public void notificaTutti(String codice) {
		
		for(int i =0;i<clienti.size();i++){
			try {
				clienti.get(i).notifica(codice);
			} catch (RemoteException e) {
				
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Remote Exception", e);
			}
		}

		
	}
	
	//GestoreComunicazione usato dal controller
	@Override
	public String ottieniParametri(String messaggio) {

		String[] mess = this.recuperaParametriMossa(messaggio);
		int numeroGiocatore = Integer.parseInt(mess[1]);
		
		try{
		
			clienti.get(numeroGiocatore).notifica(messaggio);
		
		
		} catch (RemoteException e) {
			
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Remote Exception", e);
		}
		
		//Aspetta la risposta dal client 
		while(!rispostaArrivata) {
			
			//Aspetto la risposta dal client
		}
			
			//Setta il flag a false e invia la risposta al controller che ha richiamato il metodo
			rispostaArrivata = false;
			return this.risposta;
			
		}
		
	
	//Metodo remoto usato dal GestoreClientRMI per inviare info al GestoreComunicazioneServer
	@Override
	public void inviaRisposta(String messaggio) throws RemoteException {
		
		this.rispostaArrivata = true;
		this.risposta = messaggio;
		
		
	}
	

	/**
	 * Divide la stringa in una array di String			
	 * @param codice = La stringa da dividere
	 * @return = L'array di String in cui ho le stringhe divise
	 */
	private String[] recuperaParametriMossa(String codice) {

			//Divido la stringa in corrispondenza dei ":"
				return codice.split(":");
	
	}
	
	
	

}