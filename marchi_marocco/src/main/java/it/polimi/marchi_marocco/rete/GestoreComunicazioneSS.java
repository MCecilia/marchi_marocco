package it.polimi.marchi_marocco.rete;

import it.polimi.marchi_marocco.controller.GestoreComunicazione;

import java.io.PrintWriter;
import java.util.Scanner;

/**
 * Classe che permette al server di interfacciarsi con i diversi client connessi e lui stesso.
 * Invia i dati necessari al controller e gestisce la comunicazione
 *
 */
public class GestoreComunicazioneSS implements GestoreComunicazione {
	
	private Scanner[] inputs;
    private PrintWriter[] outputs;
    private int numeroGiocatori; 
    
	
    /**
     * Costruttore del gestore di comunicazione di rete lato Server
     * @param inputs = Per leggere gli input dai client
     * @param outputs = per scrivere sui client e inviargli messaggi
     * */
   public GestoreComunicazioneSS(Scanner[] inputs, PrintWriter[] outputs) {
    	
    	this.inputs = inputs;
    	this.outputs = outputs;
    	this.numeroGiocatori = inputs.length;
    }
	
    	
	@Override	
    public void notificaGiocatore(int numeroGiocatore, String codice) {
			
			//Recupero il codice mossa che corrisponde alla prima stringa presente in quella arrivata dal controller
			//Al gestore di comunicazione
					outputs[numeroGiocatore].println(codice);
					outputs[numeroGiocatore].flush();
					
			
			}
		
	@Override
	public void notificaTutti(String codice) {
			
			
			//Recupero il codice mossa che corrisponde alla prima stringa presente in quella arrivata dal controller
			//Al gestore di comunicazione

				for(int i = 0; i < numeroGiocatori; i++) {
					outputs[i].println(codice);
					outputs[i].flush();
				}
			
		}
		
		
		@Override
		public String ottieniParametri(String messaggio) {
			
			
			String[] mess = this.recuperaParametriMossa(messaggio);
			int numeroGiocatore = Integer.parseInt(mess[1]);
			
			outputs[numeroGiocatore].println(messaggio);
			outputs[numeroGiocatore].flush();
			
			
			while(!inputs[numeroGiocatore].hasNextLine()) {
				//Aspetto i parametri di ritorno
			}
			String risposta = inputs[numeroGiocatore].nextLine();
			
			
			return risposta;
			
			}
	
			
			/**
			 * Divide la stringa in una array di String			
			 * @param codice = La stringa da dividere
			 * @return = L'array di String in cui ho le stringhe divise
			 */
			private String[] recuperaParametriMossa(String codice) {
							
				//Divido la stringa in corrispondenza dei ":"
				return codice.split(":");
			 }


		
					
		}
			
			
		


	

	
    
    
	
	

	
	


