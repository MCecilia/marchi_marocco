package it.polimi.marchi_marocco.rete;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import it.polimi.marchi_marocco.controller.GestorePartita;
import it.polimi.marchi_marocco.model.StatoPartita;

/**
 * Classe che istanzia il server per la comunicazione con gli utenti. Accetta un numero di connessioni pari al
 * numero di utenti passato al costruttore. Alla fine istanzia lo stato partita, il controller e fa partire la partita vera a 
 * propria
 *
 */
public class ServerSS {
	
	/**
	 * This handles all of the clients in the room.
	 * It accepts new connections and adds them to the room.
	 * It is a runnable thread and when the start method is called, it will accept clients.
	 */

		private static final int SERVER_PORT = 2224;
		private List<Socket> sockets = new ArrayList<Socket>();
		private ServerSocket serverSocket;
		private InetAddress hostAddress;
		private Socket socket;
        private Scanner[] inputs;
        private PrintWriter[] outputs;
   		private int numeroGiocatori;
		
		/**
		 * Creates a new room for clients to connect to.
		 */
		public ServerSS(int numero) {
			//Attempt to get the host address
			try {
				hostAddress = InetAddress.getLocalHost();
				this.numeroGiocatori = numero;
				outputs = new PrintWriter[numeroGiocatori];
				inputs = new Scanner[numeroGiocatori];
			
			} catch(UnknownHostException e){
				
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Non riesco a trovare l'hostAddress", e);
			}
			
			String address = hostAddress.getHostAddress();
			JOptionPane.showMessageDialog(new JFrame(), "L'indirizzo IP del server è: "+ address);
			// Attempt to create server socket
			try {
				serverSocket = new ServerSocket(SERVER_PORT);
			
			} catch(IOException e) {
				
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Non riesco ad istanziare il server", e);
			}
		
				// Announce the starting of the process
			// Enter the main loop
			boolean accettazioneInCorso = true;
			int i=0;
			
		while (accettazioneInCorso) {	
			
			try {
				
				socket = serverSocket.accept();
				// Client has connected
				sockets.add(socket);
				// Add user to list
				inputs[i] = new Scanner (socket.getInputStream());
				outputs[i] = new PrintWriter (socket.getOutputStream());
				//Devo chiedere il nome agli utenti e salvarlo in un array
				if(i == (numeroGiocatori-1)) {
					accettazioneInCorso = false;
				}
				i++;
				
			} catch(IOException e) {
				
				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Non riesco a raggiungere il client", e);
			}
				
		}
		
		GestoreComunicazioneSS gestore = new GestoreComunicazioneSS(inputs, outputs);
		StatoPartita stato = new StatoPartita(sockets.size());
		GestorePartita controller = new GestorePartita(stato, gestore, numeroGiocatori);
		controller.runPartita();
		controller.partitaVera();
		try {
			this.closeConnection();
		} catch (IOException e) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Errore sleep", e);
		}
		
		try {
			this.closeConnection();
		} catch (IOException e) {
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Errore Input/Output", e);
		}//Bisogna gestire la chiusura delle connessioni una volta finita la partita

			}
		
			
		
		 /**
		  * Metodo che chiude tutte le connessioni e e disconnette il server     
		  * @throws IOException
		  */
		 public void closeConnection() throws IOException {
		      // chiudo gli stream e il socket
		      for(Scanner s : inputs) {
		    	  s.close();
		      }
		      for(PrintWriter w: outputs) {
		    	  w.close();
		      }
		      //Chiude tutti i socket nella connessione
		      for(Socket s : sockets) {
		      s.close();
		      }
		 }

		
	}


