package it.polimi.marchi_marocco.rete;



import it.polimi.marchi_marocco.view.View;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

	  
public class ClientRMI implements Runnable {

	
	    IServerRMI rmiServer;
	    Registry registry;
	    String ip;
	    int serverPort = 4444;
	    int identificativoClient;
	    private View mappa;
	/**
	  * Costruttore del Client che viene richiamato nel momento della sua creazione 
	  * @param ip = Ip in cui si deve connettere il client
	  */
	public ClientRMI (String ip, View mappa2) {

	   this.ip = ip;
	   this.mappa = mappa2;
	    
	}
	    
	@Override
	public void run() {
	    		

	    try{
	    	
	    	//Trova nel registri l'oggetto rmiServer che utilizzera' per la connessione
	    	registry=LocateRegistry.getRegistry (ip, serverPort);
	    	rmiServer=(IServerRMI)(registry.lookup("rmiServer"));
			
	    	//Metodo che permette al client di connettersi al server se c'è ancora posto oppure no
	    		if(rmiServer.connettitiAlServer()) {
						
	    					//Operazioni da compiere dopo che il client si e' connesso:
	    					identificativoClient = rmiServer.getId();
	    					rmiServer.notificaConnessione();
	    					
	    					
	    					//Aggiunge il rifetimento remoto al registry per questo determinato client sul server, cosi' da
	    					//Permettere le comunicazioni
	    					//In piu' vengono aggiunti nell'array list i riferimenti delle interfacce di comunicazione dei client
	    					GestoreClientRMI client = new GestoreClientRMI( registry, identificativoClient);
	    					rmiServer.addClient(client);
	    					
	    					client.aggiungiMappa(mappa);
	    					
	    		}
	    			} catch (RemoteException e) {
	    				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Remote Exception", e);
	    			
	    			} catch (NotBoundException e) {
						
	    				Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Not Bound Exception", e);
					}
	}
}
	
