package it.polimi.marchi_marocco.rete;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IGestoreClientRMI  extends Remote {

	/**
	 * Metodo che viene usato dal gestore comunicazioneRMI per inviare notifiche ai vari client
	 * @param codice = Stringa ben formata di caratetri sacondo il protcollo di rete
	 * @throws RemoteException
	 */
	void notifica(String codice) throws RemoteException; 
	
	

}
