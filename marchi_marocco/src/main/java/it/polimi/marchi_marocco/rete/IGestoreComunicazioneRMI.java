package it.polimi.marchi_marocco.rete;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IGestoreComunicazioneRMI extends Remote {

	/**
	 * Metodo che viene messo a disposizione del clien e viene richiamato quando devono essere ritornate
	 * Le stringhe di risposta al server
	 * @param messaggio = String formattata opportunatamente rinviata al client
	 * @throws RemoteException
	 */
	void inviaRisposta(String messaggio) throws RemoteException;

}
